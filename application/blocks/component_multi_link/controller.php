<?php namespace Application\Block\ComponentMultiLink;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Concrete\Core\Editor\LinkAbstractor;
use Database;
use Permissions;
use File;
use Page;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('cpMultiLinkLoop' => array());
    protected $btExportFileColumns = array();
    protected $btExportPageColumns = array();
    protected $btExportTables = array('btComponentMultiLink', 'btComponentMultiLinkCpMultiLinkLoopEntries');
    protected $btTable = 'btComponentMultiLink';
    protected $btInterfaceWidth = 800;
    protected $btInterfaceHeight = 600;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("リンクリスト");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->cpMultiLinkTitle;
        $content[] = $this->cpMultiLinText;
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $this->set('cpMultiLinText', LinkAbstractor::translateFrom($this->cpMultiLinText));
        $cpMultiLinkLayout_options = array(
            '' => "-- " . t("None") . " --",
            '1' => "縦",
            '2' => "水平"
        );
        $this->set("cpMultiLinkLayout_options", $cpMultiLinkLayout_options);
        $cpMultiLinkLoop = array();
        $cpMultiLinkLoop_items = $db->fetchAll('SELECT * FROM btComponentMultiLinkCpMultiLinkLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpMultiLinkLoop_items as $cpMultiLinkLoop_item_k => &$cpMultiLinkLoop_item_v) {
            $cpMultiLinkLoop_item_v["cpMultiLinkLink_Object"] = null;
			$cpMultiLinkLoop_item_v["cpMultiLinkLink_Title"] = trim($cpMultiLinkLoop_item_v["cpMultiLinkLink_Title"]);
			if (isset($cpMultiLinkLoop_item_v["cpMultiLinkLink"]) && trim($cpMultiLinkLoop_item_v["cpMultiLinkLink"]) != '') {
				switch ($cpMultiLinkLoop_item_v["cpMultiLinkLink"]) {
					case 'page':
						if ($cpMultiLinkLoop_item_v["cpMultiLinkLink_Page"] > 0 && ($cpMultiLinkLoop_item_v["cpMultiLinkLink_Page_c"] = Page::getByID($cpMultiLinkLoop_item_v["cpMultiLinkLink_Page"])) && !$cpMultiLinkLoop_item_v["cpMultiLinkLink_Page_c"]->error && !$cpMultiLinkLoop_item_v["cpMultiLinkLink_Page_c"]->isInTrash()) {
							$cpMultiLinkLoop_item_v["cpMultiLinkLink_Object"] = $cpMultiLinkLoop_item_v["cpMultiLinkLink_Page_c"];
							$cpMultiLinkLoop_item_v["cpMultiLinkLink_URL"] = $cpMultiLinkLoop_item_v["cpMultiLinkLink_Page_c"]->getCollectionLink();
							if ($cpMultiLinkLoop_item_v["cpMultiLinkLink_Title"] == '') {
								$cpMultiLinkLoop_item_v["cpMultiLinkLink_Title"] = $cpMultiLinkLoop_item_v["cpMultiLinkLink_Page_c"]->getCollectionName();
							}
						}
						break;
				    case 'file':
						$cpMultiLinkLoop_item_v["cpMultiLinkLink_File_id"] = (int)$cpMultiLinkLoop_item_v["cpMultiLinkLink_File"];
						if ($cpMultiLinkLoop_item_v["cpMultiLinkLink_File_id"] > 0 && ($cpMultiLinkLoop_item_v["cpMultiLinkLink_File_object"] = File::getByID($cpMultiLinkLoop_item_v["cpMultiLinkLink_File_id"])) && is_object($cpMultiLinkLoop_item_v["cpMultiLinkLink_File_object"])) {
							$fp = new Permissions($cpMultiLinkLoop_item_v["cpMultiLinkLink_File_object"]);
							if ($fp->canViewFile()) {
								$cpMultiLinkLoop_item_v["cpMultiLinkLink_Object"] = $cpMultiLinkLoop_item_v["cpMultiLinkLink_File_object"];
								$cpMultiLinkLoop_item_v["cpMultiLinkLink_URL"] = $cpMultiLinkLoop_item_v["cpMultiLinkLink_File_object"]->getRelativePath();
								if ($cpMultiLinkLoop_item_v["cpMultiLinkLink_Title"] == '') {
									$cpMultiLinkLoop_item_v["cpMultiLinkLink_Title"] = $cpMultiLinkLoop_item_v["cpMultiLinkLink_File_object"]->getTitle();
								}
							}
						}
						break;
				    case 'url':
						if ($cpMultiLinkLoop_item_v["cpMultiLinkLink_Title"] == '') {
							$cpMultiLinkLoop_item_v["cpMultiLinkLink_Title"] = $cpMultiLinkLoop_item_v["cpMultiLinkLink_URL"];
						}
						break;
				    case 'relative_url':
						if ($cpMultiLinkLoop_item_v["cpMultiLinkLink_Title"] == '') {
							$cpMultiLinkLoop_item_v["cpMultiLinkLink_Title"] = $cpMultiLinkLoop_item_v["cpMultiLinkLink_Relative_URL"];
						}
						$cpMultiLinkLoop_item_v["cpMultiLinkLink_URL"] = $cpMultiLinkLoop_item_v["cpMultiLinkLink_Relative_URL"];
						break;
				    case 'image':
						if ($cpMultiLinkLoop_item_v["cpMultiLinkLink_Image"] > 0 && ($cpMultiLinkLoop_item_v["cpMultiLinkLink_Image_object"] = File::getByID($cpMultiLinkLoop_item_v["cpMultiLinkLink_Image"])) && is_object($cpMultiLinkLoop_item_v["cpMultiLinkLink_Image_object"])) {
							$cpMultiLinkLoop_item_v["cpMultiLinkLink_URL"] = $cpMultiLinkLoop_item_v["cpMultiLinkLink_Image_object"]->getURL();
							$cpMultiLinkLoop_item_v["cpMultiLinkLink_Object"] = $cpMultiLinkLoop_item_v["cpMultiLinkLink_Image_object"];
							if ($cpMultiLinkLoop_item_v["cpMultiLinkLink_Title"] == '') {
								$cpMultiLinkLoop_item_v["cpMultiLinkLink_Title"] = $cpMultiLinkLoop_item_v["cpMultiLinkLink_Image_object"]->getTitle();
							}
						}
						break;
				}
			}
        }
        $this->set('cpMultiLinkLoop_items', $cpMultiLinkLoop_items);
        $this->set('cpMultiLinkLoop', $cpMultiLinkLoop);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btComponentMultiLinkCpMultiLinkLoopEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $cpMultiLinkLoop_items = $db->fetchAll('SELECT * FROM btComponentMultiLinkCpMultiLinkLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpMultiLinkLoop_items as $cpMultiLinkLoop_item) {
            unset($cpMultiLinkLoop_item['id']);
            $cpMultiLinkLoop_item['bID'] = $newBID;
            $db->insert('btComponentMultiLinkCpMultiLinkLoopEntries', $cpMultiLinkLoop_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $cpMultiLinkLoop = $this->get('cpMultiLinkLoop');
        $this->set('cpMultiLinkLoop_items', array());
        $this->set('cpMultiLinkLoop', $cpMultiLinkLoop);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        
        $this->set('cpMultiLinText', LinkAbstractor::translateFromEditMode($this->cpMultiLinText));
        $cpMultiLinkLoop = $this->get('cpMultiLinkLoop');
        $cpMultiLinkLoop_items = $db->fetchAll('SELECT * FROM btComponentMultiLinkCpMultiLinkLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $this->set('cpMultiLinkLoop', $cpMultiLinkLoop);
        $this->set('cpMultiLinkLoop_items', $cpMultiLinkLoop_items);
    }

    protected function addEdit()
    {
        $this->set("cpMultiLinkLayout_options", array(
                '' => "-- " . t("None") . " --",
                '1' => "縦",
                '2' => "水平"
            )
        );
        $cpMultiLinkLoop = array();
        $this->set("cpMultiLinkLink_Options", $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
), true));
        $this->set('cpMultiLinkLoop', $cpMultiLinkLoop);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/component_multi_link/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/component_multi_link/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/component_multi_link/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('redactor');
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $db = Database::connection();
        $args['cpMultiLinText'] = LinkAbstractor::translateTo($args['cpMultiLinText']);
        $rows = $db->fetchAll('SELECT * FROM btComponentMultiLinkCpMultiLinkLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $cpMultiLinkLoop_items = isset($args['cpMultiLinkLoop']) && is_array($args['cpMultiLinkLoop']) ? $args['cpMultiLinkLoop'] : array();
        $queries = array();
        if (!empty($cpMultiLinkLoop_items)) {
            $i = 0;
            foreach ($cpMultiLinkLoop_items as $cpMultiLinkLoop_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($cpMultiLinkLoop_item['cpMultiLinkLink']) && trim($cpMultiLinkLoop_item['cpMultiLinkLink']) != '') {
					$data['cpMultiLinkLink_Title'] = $cpMultiLinkLoop_item['cpMultiLinkLink_Title'];
					$data['cpMultiLinkLink'] = $cpMultiLinkLoop_item['cpMultiLinkLink'];
					switch ($cpMultiLinkLoop_item['cpMultiLinkLink']) {
						case 'page':
							$data['cpMultiLinkLink_Page'] = $cpMultiLinkLoop_item['cpMultiLinkLink_Page'];
							$data['cpMultiLinkLink_File'] = '0';
							$data['cpMultiLinkLink_URL'] = '';
							$data['cpMultiLinkLink_Relative_URL'] = '';
							$data['cpMultiLinkLink_Image'] = '0';
							break;
                        case 'file':
							$data['cpMultiLinkLink_File'] = $cpMultiLinkLoop_item['cpMultiLinkLink_File'];
							$data['cpMultiLinkLink_Page'] = '0';
							$data['cpMultiLinkLink_URL'] = '';
							$data['cpMultiLinkLink_Relative_URL'] = '';
							$data['cpMultiLinkLink_Image'] = '0';
							break;
                        case 'url':
							$data['cpMultiLinkLink_URL'] = $cpMultiLinkLoop_item['cpMultiLinkLink_URL'];
							$data['cpMultiLinkLink_Page'] = '0';
							$data['cpMultiLinkLink_File'] = '0';
							$data['cpMultiLinkLink_Relative_URL'] = '';
							$data['cpMultiLinkLink_Image'] = '0';
							break;
                        case 'relative_url':
							$data['cpMultiLinkLink_Relative_URL'] = $cpMultiLinkLoop_item['cpMultiLinkLink_Relative_URL'];
							$data['cpMultiLinkLink_Page'] = '0';
							$data['cpMultiLinkLink_File'] = '0';
							$data['cpMultiLinkLink_URL'] = '';
							$data['cpMultiLinkLink_Image'] = '0';
							break;
                        case 'image':
							$data['cpMultiLinkLink_Image'] = $cpMultiLinkLoop_item['cpMultiLinkLink_Image'];
							$data['cpMultiLinkLink_Page'] = '0';
							$data['cpMultiLinkLink_File'] = '0';
							$data['cpMultiLinkLink_URL'] = '';
							$data['cpMultiLinkLink_Relative_URL'] = '';
							break;
                        default:
							$data['cpMultiLinkLink'] = '';
							$data['cpMultiLinkLink_Page'] = '0';
							$data['cpMultiLinkLink_File'] = '0';
							$data['cpMultiLinkLink_URL'] = '';
							$data['cpMultiLinkLink_Relative_URL'] = '';
							$data['cpMultiLinkLink_Image'] = '0';
							break;	
					}
				}
				else {
					$data['cpMultiLinkLink'] = '';
					$data['cpMultiLinkLink_Title'] = '';
					$data['cpMultiLinkLink_Page'] = '0';
					$data['cpMultiLinkLink_File'] = '0';
					$data['cpMultiLinkLink_URL'] = '';
					$data['cpMultiLinkLink_Relative_URL'] = '';
					$data['cpMultiLinkLink_Image'] = '0';
				}
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btComponentMultiLinkCpMultiLinkLoopEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btComponentMultiLinkCpMultiLinkLoopEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btComponentMultiLinkCpMultiLinkLoopEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("cpMultiLinkTitle", $this->btFieldsRequired) && trim($args["cpMultiLinkTitle"]) == "") {
            $e->add(t("The %s field is required.", t("タイトル")));
        }
        if (in_array("cpMultiLinText", $this->btFieldsRequired) && (trim($args["cpMultiLinText"]) == "")) {
            $e->add(t("The %s field is required.", t("テキスト")));
        }
        if ((in_array("cpMultiLinkLayout", $this->btFieldsRequired) && (!isset($args["cpMultiLinkLayout"]) || trim($args["cpMultiLinkLayout"]) == "")) || (isset($args["cpMultiLinkLayout"]) && trim($args["cpMultiLinkLayout"]) != "" && !in_array($args["cpMultiLinkLayout"], array("1", "2")))) {
            $e->add(t("The %s field has an invalid value.", t("レイアウト")));
        }
        $cpMultiLinkLoopEntriesMin = 0;
        $cpMultiLinkLoopEntriesMax = 0;
        $cpMultiLinkLoopEntriesErrors = 0;
        $cpMultiLinkLoop = array();
        if (isset($args['cpMultiLinkLoop']) && is_array($args['cpMultiLinkLoop']) && !empty($args['cpMultiLinkLoop'])) {
            if ($cpMultiLinkLoopEntriesMin >= 1 && count($args['cpMultiLinkLoop']) < $cpMultiLinkLoopEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("リンク"), $cpMultiLinkLoopEntriesMin, count($args['cpMultiLinkLoop'])));
                $cpMultiLinkLoopEntriesErrors++;
            }
            if ($cpMultiLinkLoopEntriesMax >= 1 && count($args['cpMultiLinkLoop']) > $cpMultiLinkLoopEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("リンク"), $cpMultiLinkLoopEntriesMax, count($args['cpMultiLinkLoop'])));
                $cpMultiLinkLoopEntriesErrors++;
            }
            if ($cpMultiLinkLoopEntriesErrors == 0) {
                foreach ($args['cpMultiLinkLoop'] as $cpMultiLinkLoop_k => $cpMultiLinkLoop_v) {
                    if (is_array($cpMultiLinkLoop_v)) {
                        if ((in_array("cpMultiLinkLink", $this->btFieldsRequired['cpMultiLinkLoop']) && (!isset($cpMultiLinkLoop_v['cpMultiLinkLink']) || trim($cpMultiLinkLoop_v['cpMultiLinkLink']) == "")) || (isset($cpMultiLinkLoop_v['cpMultiLinkLink']) && trim($cpMultiLinkLoop_v['cpMultiLinkLink']) != "" && !array_key_exists($cpMultiLinkLoop_v['cpMultiLinkLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
))))) {
							$e->add(t("The %s field has an invalid value.", t("リンク")));
						} elseif (array_key_exists($cpMultiLinkLoop_v['cpMultiLinkLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
)))) {
							switch ($cpMultiLinkLoop_v['cpMultiLinkLink']) {
								case 'page':
									if (!isset($cpMultiLinkLoop_v['cpMultiLinkLink_Page']) || trim($cpMultiLinkLoop_v['cpMultiLinkLink_Page']) == "" || $cpMultiLinkLoop_v['cpMultiLinkLink_Page'] == "0" || (($page = Page::getByID($cpMultiLinkLoop_v['cpMultiLinkLink_Page'])) && $page->error !== false)) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Page"), t("リンク"), t("リンク"), $cpMultiLinkLoop_k));
									}
									break;
				                case 'file':
									if (!isset($cpMultiLinkLoop_v['cpMultiLinkLink_File']) || trim($cpMultiLinkLoop_v['cpMultiLinkLink_File']) == "" || !is_object(File::getByID($cpMultiLinkLoop_v['cpMultiLinkLink_File']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("File"), t("リンク"), t("リンク"), $cpMultiLinkLoop_k));
									}
									break;
				                case 'url':
									if (!isset($cpMultiLinkLoop_v['cpMultiLinkLink_URL']) || trim($cpMultiLinkLoop_v['cpMultiLinkLink_URL']) == "" || !filter_var($cpMultiLinkLoop_v['cpMultiLinkLink_URL'], FILTER_VALIDATE_URL)) {
										$e->add(t("The %s field for '%s' does not have a valid URL (%s, row #%s).", t("URL"), t("リンク"), t("リンク"), $cpMultiLinkLoop_k));
									}
									break;
				                case 'relative_url':
									if (!isset($cpMultiLinkLoop_v['cpMultiLinkLink_Relative_URL']) || trim($cpMultiLinkLoop_v['cpMultiLinkLink_Relative_URL']) == "") {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Relative URL"), t("リンク"), t("リンク"), $cpMultiLinkLoop_k));
									}
									break;
				                case 'image':
									if (!isset($cpMultiLinkLoop_v['cpMultiLinkLink_Image']) || trim($cpMultiLinkLoop_v['cpMultiLinkLink_Image']) == "" || !is_object(File::getByID($cpMultiLinkLoop_v['cpMultiLinkLink_Image']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Image"), t("リンク"), t("リンク"), $cpMultiLinkLoop_k));
									}
									break;	
							}
						}
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('リンク'), $cpMultiLinkLoop_k));
                    }
                }
            }
        } else {
            if ($cpMultiLinkLoopEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("リンク"), $cpMultiLinkLoopEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-component_multi_link', 'blocks/component_multi_link/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-component_multi_link');
        $this->edit();
    }

        protected function getSmartLinkTypeOptions($include = array(), $checkNone = false)
	{
		$options = array(
			''             => sprintf("-- %s--", t("None")),
			'page'         => t("Page"),
			'url'          => t("External URL"),
			'relative_url' => t("Relative URL"),
			'file'         => t("File"),
			'image'        => t("Image")
		);
		if ($checkNone) {
            $include = array_merge(array(''), $include);
        }
		$return = array();
		foreach($include as $v){
		    if(isset($options[$v])){
		        $return[$v] = $options[$v];
		    }
		}
		return $return;
	}
}