<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div class="c-section">
    <?php if (isset($cpMultiLinkTitle) && trim($cpMultiLinkTitle) != "") { ?>
        <h2 class="c-section_head"><?php echo h($cpMultiLinkTitle); ?></h2>
    <?php } ?>
    <?php if (isset($cpMultiLinText) && trim($cpMultiLinText) != "") { ?>
        <div class="c-section_text"><?php echo $cpMultiLinText; ?></div>
    <?php } ?>
    <?php if (trim($cpMultiLinkLayout) != "") { ?>
        
    <?php switch($cpMultiLinkLayout) {
    case "1":
        $type = 'c-section_links c-section_links--type_long';
        break;
    case "2":
        $type = 'c-section_links c-section_links--type_land';
        break;
    default:
        $type = 'c-section_links c-section_links--type_long';
        break;
    } ?>
    <?php } ?>

    <?php if (!empty($cpMultiLinkLoop_items)) { ?>
        <ul class="<?= $type;?>">
            <?php foreach ($cpMultiLinkLoop_items as $cpMultiLinkLoop_item_key => $cpMultiLinkLoop_item) { ?>
            <?php 
            if (trim($cpMultiLinkLoop_item["cpMultiLinkLink_URL"]) != "") { ?>
                <?php
            	$cpMultiLinkLoop_itemcpMultiLinkLink_Attributes = array();
            	$cpMultiLinkLoop_itemcpMultiLinkLink_Attributes['href'] = $cpMultiLinkLoop_item["cpMultiLinkLink_URL"];
            	$cpMultiLinkLoop_item["cpMultiLinkLink_AttributesHtml"] = join(' ', array_map(function ($key) use ($cpMultiLinkLoop_itemcpMultiLinkLink_Attributes) {
            		return $key . '="' . $cpMultiLinkLoop_itemcpMultiLinkLink_Attributes[$key] . '"';
            	}, array_keys($cpMultiLinkLoop_itemcpMultiLinkLink_Attributes)));
            	echo sprintf('<li class="c-section_link"><a %s class="e-anc">%s</a></li>', $cpMultiLinkLoop_item["cpMultiLinkLink_AttributesHtml"], $cpMultiLinkLoop_item["cpMultiLinkLink_Title"]); ?><?php
            } ?>
            <?php } ?>
        </ul>
    <?php } ?>
</div>