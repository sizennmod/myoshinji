<?php defined('C5_EXECUTE') or die("Access Denied.");
$navigationTypeText = ($navigationType == 0) ? 'arrows' : 'pages';
$c = Page::getCurrentPage();
if ($c->isEditMode()) {
    ?>
    <div class="ccm-edit-mode-disabled-item" style="<?php echo isset($width) ? "width: $width;" : '' ?><?php echo isset($height) ? "height: $height;" : '' ?>">
        <i style="font-size:40px; margin-bottom:20px; display:block;" class="fa fa-picture-o" aria-hidden="true"></i>
        <div style="padding: 40px 0px 40px 0px"><?php echo t('Image Slider disabled in edit mode.')?>
			<div style="margin-top: 15px; font-size:9px;">
				<i class="fa fa-circle" aria-hidden="true"></i>
				<?php if (count($rows) > 0) { ?>
					<?php foreach (array_slice($rows, 1) as $row) { ?>
						<i class="fa fa-circle-thin" aria-hidden="true"></i>
						<?php }
					}
				?>
			</div>
        </div>
    </div>
<?php
} else {
    ?>
    <?php if (count($rows) > 0) {?>
       <div class="c-slider c-gallery_slider js-slider js-popup">
            <?php foreach ($rows as $row) {?>
                <?php $f = File::getByID($row['fID'])?>
                <figure class="c-slider_item js-popup_item">
                    <a href="<?php echo $f->getURL();?>" data-effect="mfp-zoom-in" class="e-box">
                        <img src="<?php echo $f->getURL();?>" alt="" class="e-img">
                        
                        <div class="ccm-image-slider-text">
                            <?php if ($row['title']) {?>
                            	<h2 class="ccm-image-slider-title"><?php echo $row['title'] ?></h2>
                            <?php } ?>
                            <?php echo $row['description'] ?>
                        </div>
                    </a>
                </figure>
            <?php }?>
        </div>
    <?php } else {?>
        <div class="ccm-image-slider-placeholder">
            <p><?php echo t('No Slides Entered.');?></p>
        </div>
    <?php }?>
<?php
} ?>
