<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
            <?php
	$core_editor = Core::make('editor');
	if (method_exists($core_editor, 'getEditorInitJSFunction')) {
		/* @var $core_editor \Concrete\Core\Editor\CkeditorEditor */
		?>
		<script type="text/javascript">var blockDesignerEditor = <?php echo $core_editor->getEditorInitJSFunction(); ?>;</script>
	<?php
	} else {
	/* @var $core_editor \Concrete\Core\Editor\RedactorEditor */
	?>
		<script type="text/javascript">
			var blockDesignerEditor = function (identifier) {$(identifier).redactor(<?php echo json_encode(array('plugins' => array("concrete5lightbox", "undoredo", "specialcharacters", "table", "concrete5magic"), 'minHeight' => 300,'concrete5' => array('filemanager' => $core_editor->allowFileManager(), 'sitemap' => $core_editor->allowSitemap()))); ?>).on('remove', function () {$(this).redactor('core.destroy');});};
		</script>
		<?php
	} ?><?php $repeatable_container_id = 'btComponentTextMulti-cpTextColumns-container-' . $identifier_getString; ?>
    <div id="<?php echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $cpTextColumns_items,
                        'order' => array_keys($cpTextColumns_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php echo t('記事データ') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextTile]" class="control-label"><?php echo t("タイトル"); ?></label>
    <?php echo isset($btFieldsRequired['cpTextColumns']) && in_array('cpTextTile', $btFieldsRequired['cpTextColumns']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextTile]" id="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextTile]" class="form-control" rows="5">{{ cpTextTile }}</textarea>
</div>            <div class="form-group">
    <label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextWyg]" class="control-label"><?php echo t("本文"); ?></label>
    <?php echo isset($btFieldsRequired['cpTextColumns']) && in_array('cpTextWyg', $btFieldsRequired['cpTextColumns']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextWyg]" id="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextWyg]" class="ft-cpTextColumns-cpTextWyg">{{ cpTextWyg }}</textarea>
</div>            <div class="form-group">
    <label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImg]" class="control-label"><?php echo t("画像"); ?></label>
    <?php echo isset($btFieldsRequired['cpTextColumns']) && in_array('cpTextImg', $btFieldsRequired['cpTextColumns']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-file-selector-input-name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImg]" class="ccm-file-selector ft-image-cpTextImg-file-selector" data-file-selector-f-id="{{ cpTextImg }}"></div>
</div>            <?php $cpTextImgLink_ContainerID = 'btComponentTextMulti-cpTextImgLink-container-' . $identifier_getString; ?>
<div class="ft-smart-link" id="<?php echo $cpTextImgLink_ContainerID; ?>">
	<div class="form-group">
		<label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLink]" class="control-label"><?php echo t("画像リンク"); ?></label>
	    <?php echo isset($btFieldsRequired['cpTextColumns']) && in_array('cpTextImgLink', $btFieldsRequired['cpTextColumns']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
	    <?php $cpTextColumnsCpTextImgLink_options = $cpTextImgLink_Options; ?>
                    <select name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLink]" id="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLink]" class="form-control ft-smart-link-type">{{#select cpTextImgLink}}<?php foreach ($cpTextColumnsCpTextImgLink_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
	</div>
	
	<div class="form-group">
		<div class="ft-smart-link-options hidden" style="padding-left: 10px;">
			<div class="form-group">
				<label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLink_Title]" class="control-label"><?php echo t("Title"); ?></label>
			    <input name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLink_Title]" id="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLink_Title]" class="form-control" type="text" value="{{ cpTextImgLink_Title }}" />		
			</div>
			
			<div class="form-group hidden" data-link-type="page">
			<label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLink_Page]" class="control-label"><?php echo t("Page"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-page-selector="{{token}}" data-input-name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLink_Page]" data-cID="{{cpTextImgLink_Page}}"></div>
		</div>

		<div class="form-group hidden" data-link-type="url">
			<label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLink_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLink_URL]" id="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLink_URL]" class="form-control" type="text" value="{{ cpTextImgLink_URL }}" />
		</div>
		</div>
	</div>
</div>
            <div class="form-group">
    <label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImagesAlt]" class="control-label"><?php echo t("画像alt"); ?></label>
    <?php echo isset($btFieldsRequired['cpTextColumns']) && in_array('cpTextImagesAlt', $btFieldsRequired['cpTextColumns']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImagesAlt]" id="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImagesAlt]" class="form-control" type="text" value="{{ cpTextImagesAlt }}" maxlength="255" />
</div>            <div class="form-group">
    <label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgCapTitle]" class="control-label"><?php echo t("キャプションタイトル"); ?></label>
    <?php echo isset($btFieldsRequired['cpTextColumns']) && in_array('cpTextImgCapTitle', $btFieldsRequired['cpTextColumns']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgCapTitle]" id="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgCapTitle]" class="form-control" rows="5">{{ cpTextImgCapTitle }}</textarea>
</div>            <div class="form-group">
    <label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgCapText]" class="control-label"><?php echo t("キャプションテキスト"); ?></label>
    <?php echo isset($btFieldsRequired['cpTextColumns']) && in_array('cpTextImgCapText', $btFieldsRequired['cpTextColumns']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgCapText]" id="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgCapText]" class="form-control" rows="5">{{ cpTextImgCapText }}</textarea>
</div>            <?php $cpTextImgLinkBtn_ContainerID = 'btComponentTextMulti-cpTextImgLinkBtn-container-' . $identifier_getString; ?>
<div class="ft-smart-link" id="<?php echo $cpTextImgLinkBtn_ContainerID; ?>">
	<div class="form-group">
		<label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn]" class="control-label"><?php echo t("リンクボタン"); ?></label>
	    <?php echo isset($btFieldsRequired['cpTextColumns']) && in_array('cpTextImgLinkBtn', $btFieldsRequired['cpTextColumns']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
	    <?php $cpTextColumnsCpTextImgLinkBtn_options = $cpTextImgLinkBtn_Options; ?>
                    <select name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn]" id="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn]" class="form-control ft-smart-link-type">{{#select cpTextImgLinkBtn}}<?php foreach ($cpTextColumnsCpTextImgLinkBtn_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
	</div>
	
	<div class="form-group">
		<div class="ft-smart-link-options hidden" style="padding-left: 10px;">
			<div class="form-group">
				<label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_Title]" class="control-label"><?php echo t("Title"); ?></label>
			    <input name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_Title]" id="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_Title]" class="form-control" type="text" value="{{ cpTextImgLinkBtn_Title }}" />		
			</div>
			
			<div class="form-group hidden" data-link-type="page">
			<label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_Page]" class="control-label"><?php echo t("Page"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-page-selector="{{token}}" data-input-name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_Page]" data-cID="{{cpTextImgLinkBtn_Page}}"></div>
		</div>

		<div class="form-group hidden" data-link-type="url">
			<label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_URL]" id="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_URL]" class="form-control" type="text" value="{{ cpTextImgLinkBtn_URL }}" />
		</div>

		<div class="form-group hidden" data-link-type="relative_url">
			<label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_Relative_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_Relative_URL]" id="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_Relative_URL]" class="form-control" type="text" value="{{ cpTextImgLinkBtn_Relative_URL }}" />
		</div>

		<div class="form-group hidden" data-link-type="file">
		    <label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_File]" class="control-label"><?php echo t("File"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_File]" class="ccm-file-selector" data-file-selector-f-id="{{ cpTextImgLinkBtn_File }}"></div>	
		</div>

		<div class="form-group hidden" data-link-type="image">
			<label for="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_Image]" class="control-label"><?php echo t("Image"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('cpTextColumns'); ?>[{{id}}][cpTextImgLinkBtn_Image]" class="ccm-file-selector" data-file-selector-f-id="{{ cpTextImgLinkBtn_Image }}"></div>
		</div>
		</div>
	</div>
</div>
</div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btComponentTextMulti.cpTextColumns.edit.open', {id: '<?php echo $repeatable_container_id; ?>'});
    $.each($('#<?php echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>