<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div class="c-section">
    <?php if (!empty($cpTextColumns_items)) { ?>
    <?php 
    if(count($cpTextColumns_items) == 2){
        $wrap = 'c-section_columns c-section_columns--column_2';
    }elseif(count($cpTextColumns_items) == 3){
        $wrap = 'c-section_columns c-section_columns--column_3';
    }
    ?>
   
    <div class="<?= $wrap;?>">
        <?php foreach ($cpTextColumns_items as $cpTextColumns_item_key => $cpTextColumns_item) { ?>
        <div class="c-section_column">
            <?php if (isset($cpTextColumns_item["cpTextTile"]) && trim($cpTextColumns_item["cpTextTile"]) != "") { ?>
                <h2 class="c-section_head"><?php echo h($cpTextColumns_item["cpTextTile"]); ?></h2>
            <?php } ?>

            <?php if (isset($cpTextColumns_item["cpTextWyg"]) && trim($cpTextColumns_item["cpTextWyg"]) != "") { ?>
                <div class="c-section_text">
                    <?php echo $cpTextColumns_item["cpTextWyg"]; ?>
                </div>
            <?php } ?>


            <div class="c-section_figure">
                <?php if(is_object($cpTextColumns_item["cpTextImg"])){?>
                <div class="e-img">
                <?php if (trim($cpTextColumns_item["cpTextImgLink_URL"])) echo '<a href="'.$cpTextColumns_item["cpTextImgLink_URL"].'">';?>
                    <img src="<?php echo $cpTextColumns_item["cpTextImg"]->getURL(); ?>" alt="<?php echo h($cpTextColumns_item["cpTextImagesAlt"]); ?>"/>
                <?php if (trim($cpTextColumns_item["cpTextImgLink_URL"])) echo '</a>';?>
                </div>
                <?php } ?>

                <?php if ((isset($cpTextColumns_item["cpTextImgCapTitle"]) && trim($cpTextColumns_item["cpTextImgCapTitle"]) != "") || (isset($cpTextColumns_item["cpTextImgCapText"]) && trim($cpTextColumns_item["cpTextImgCapText"]))) { ?>
                    <div class="e-caption">
                        <?php if (isset($cpTextColumns_item["cpTextImgCapTitle"]) && trim($cpTextColumns_item["cpTextImgCapTitle"]) != "") { ?>
                            <div class="caption-title">
                                <p><?php echo h($cpTextColumns_item["cpTextImgCapTitle"]); ?></p>
                            </div>
                        <?php } ?>
                        <?php if (isset($cpTextColumns_item["cpTextImgCapText"]) && trim($cpTextColumns_item["cpTextImgCapText"]) != "") { ?>
                            <div class="caption-text">
                                <p><?php echo h($cpTextColumns_item["cpTextImgCapText"]); ?></p>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>

            <?php if (trim($cpTextColumns_item["cpTextImgLinkBtn_URL"]) != "") { ?>
                <div class="c-linkBtn c-section_button">
                    <p class="c-linkBtn_item">
                    <?php
                    $cpTextColumns_itemcpTextImgLinkBtn_Attributes = array();
                    $cpTextColumns_itemcpTextImgLinkBtn_Attributes['href'] = $cpTextColumns_item["cpTextImgLinkBtn_URL"];
                    $cpTextColumns_item["cpTextImgLinkBtn_AttributesHtml"] = join(' ', array_map(function ($key) use ($cpTextColumns_itemcpTextImgLinkBtn_Attributes) {
                        return $key . '="' . $cpTextColumns_itemcpTextImgLinkBtn_Attributes[$key] . '"';
                    }, array_keys($cpTextColumns_itemcpTextImgLinkBtn_Attributes)));
                    echo sprintf('<a %s class="e-box"><span class="e-box_in"><strong class="e-name">%s</strong></span></a>', $cpTextColumns_item["cpTextImgLinkBtn_AttributesHtml"], $cpTextColumns_item["cpTextImgLinkBtn_Title"]); ?>
                    </p>
                </div>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
    <?php } ?>
</div>