<?php namespace Application\Block\ComponentCard;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Database;
use Concrete\Core\Editor\LinkAbstractor;
use File;
use Page;
use Permissions;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('cpCardLoop' => array());
    protected $btExportFileColumns = array('cpCardImg');
    protected $btExportPageColumns = array();
    protected $btExportTables = array('btComponentCard', 'btComponentCardCpCardLoopEntries');
    protected $btTable = 'btComponentCard';
    protected $btInterfaceWidth = 800;
    protected $btInterfaceHeight = 600;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("カード");
    }

    public function getSearchableContent()
    {
        $content = array();
        $db = Database::connection();
        $cpCardLoop_items = $db->fetchAll('SELECT * FROM btComponentCardCpCardLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpCardLoop_items as $cpCardLoop_item_k => $cpCardLoop_item_v) {
            if (isset($cpCardLoop_item_v["cpCardTitle"]) && trim($cpCardLoop_item_v["cpCardTitle"]) != "") {
                $content[] = $cpCardLoop_item_v["cpCardTitle"];
            }
            if (isset($cpCardLoop_item_v["cpCardTitle_1"]) && trim($cpCardLoop_item_v["cpCardTitle_1"]) != "") {
                $content[] = $cpCardLoop_item_v["cpCardTitle_1"];
            }
        }
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $cpCardLoop = array();
        $cpCardLoop_items = $db->fetchAll('SELECT * FROM btComponentCardCpCardLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpCardLoop_items as $cpCardLoop_item_k => &$cpCardLoop_item_v) {
            $cpCardLoop_item_v["cpCardTitle_1"] = isset($cpCardLoop_item_v["cpCardTitle_1"]) ? LinkAbstractor::translateFrom($cpCardLoop_item_v["cpCardTitle_1"]) : null;
            if (isset($cpCardLoop_item_v['cpCardImg']) && trim($cpCardLoop_item_v['cpCardImg']) != "" && ($f = File::getByID($cpCardLoop_item_v['cpCardImg'])) && is_object($f)) {
                $cpCardLoop_item_v['cpCardImg'] = $f;
            } else {
                $cpCardLoop_item_v['cpCardImg'] = false;
            }
            $cpCardLoop_item_v["cpCardLink_Object"] = null;
			$cpCardLoop_item_v["cpCardLink_Title"] = trim($cpCardLoop_item_v["cpCardLink_Title"]);
			if (isset($cpCardLoop_item_v["cpCardLink"]) && trim($cpCardLoop_item_v["cpCardLink"]) != '') {
				switch ($cpCardLoop_item_v["cpCardLink"]) {
					case 'page':
						if ($cpCardLoop_item_v["cpCardLink_Page"] > 0 && ($cpCardLoop_item_v["cpCardLink_Page_c"] = Page::getByID($cpCardLoop_item_v["cpCardLink_Page"])) && !$cpCardLoop_item_v["cpCardLink_Page_c"]->error && !$cpCardLoop_item_v["cpCardLink_Page_c"]->isInTrash()) {
							$cpCardLoop_item_v["cpCardLink_Object"] = $cpCardLoop_item_v["cpCardLink_Page_c"];
							$cpCardLoop_item_v["cpCardLink_URL"] = $cpCardLoop_item_v["cpCardLink_Page_c"]->getCollectionLink();
							if ($cpCardLoop_item_v["cpCardLink_Title"] == '') {
								$cpCardLoop_item_v["cpCardLink_Title"] = $cpCardLoop_item_v["cpCardLink_Page_c"]->getCollectionName();
							}
						}
						break;
				    case 'file':
						$cpCardLoop_item_v["cpCardLink_File_id"] = (int)$cpCardLoop_item_v["cpCardLink_File"];
						if ($cpCardLoop_item_v["cpCardLink_File_id"] > 0 && ($cpCardLoop_item_v["cpCardLink_File_object"] = File::getByID($cpCardLoop_item_v["cpCardLink_File_id"])) && is_object($cpCardLoop_item_v["cpCardLink_File_object"])) {
							$fp = new Permissions($cpCardLoop_item_v["cpCardLink_File_object"]);
							if ($fp->canViewFile()) {
								$cpCardLoop_item_v["cpCardLink_Object"] = $cpCardLoop_item_v["cpCardLink_File_object"];
								$cpCardLoop_item_v["cpCardLink_URL"] = $cpCardLoop_item_v["cpCardLink_File_object"]->getRelativePath();
								if ($cpCardLoop_item_v["cpCardLink_Title"] == '') {
									$cpCardLoop_item_v["cpCardLink_Title"] = $cpCardLoop_item_v["cpCardLink_File_object"]->getTitle();
								}
							}
						}
						break;
				    case 'url':
						if ($cpCardLoop_item_v["cpCardLink_Title"] == '') {
							$cpCardLoop_item_v["cpCardLink_Title"] = $cpCardLoop_item_v["cpCardLink_URL"];
						}
						break;
				    case 'relative_url':
						if ($cpCardLoop_item_v["cpCardLink_Title"] == '') {
							$cpCardLoop_item_v["cpCardLink_Title"] = $cpCardLoop_item_v["cpCardLink_Relative_URL"];
						}
						$cpCardLoop_item_v["cpCardLink_URL"] = $cpCardLoop_item_v["cpCardLink_Relative_URL"];
						break;
				    case 'image':
						if ($cpCardLoop_item_v["cpCardLink_Image"] > 0 && ($cpCardLoop_item_v["cpCardLink_Image_object"] = File::getByID($cpCardLoop_item_v["cpCardLink_Image"])) && is_object($cpCardLoop_item_v["cpCardLink_Image_object"])) {
							$cpCardLoop_item_v["cpCardLink_URL"] = $cpCardLoop_item_v["cpCardLink_Image_object"]->getURL();
							$cpCardLoop_item_v["cpCardLink_Object"] = $cpCardLoop_item_v["cpCardLink_Image_object"];
							if ($cpCardLoop_item_v["cpCardLink_Title"] == '') {
								$cpCardLoop_item_v["cpCardLink_Title"] = $cpCardLoop_item_v["cpCardLink_Image_object"]->getTitle();
							}
						}
						break;
				}
			}
        }
        $this->set('cpCardLoop_items', $cpCardLoop_items);
        $this->set('cpCardLoop', $cpCardLoop);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btComponentCardCpCardLoopEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $cpCardLoop_items = $db->fetchAll('SELECT * FROM btComponentCardCpCardLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpCardLoop_items as $cpCardLoop_item) {
            unset($cpCardLoop_item['id']);
            $cpCardLoop_item['bID'] = $newBID;
            $db->insert('btComponentCardCpCardLoopEntries', $cpCardLoop_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $cpCardLoop = $this->get('cpCardLoop');
        $this->set('cpCardLoop_items', array());
        $this->set('cpCardLoop', $cpCardLoop);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        $cpCardLoop = $this->get('cpCardLoop');
        $cpCardLoop_items = $db->fetchAll('SELECT * FROM btComponentCardCpCardLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        
        foreach ($cpCardLoop_items as &$cpCardLoop_item) {
            $cpCardLoop_item['cpCardTitle_1'] = isset($cpCardLoop_item['cpCardTitle_1']) ? LinkAbstractor::translateFromEditMode($cpCardLoop_item['cpCardTitle_1']) : null;
        }
        foreach ($cpCardLoop_items as &$cpCardLoop_item) {
            if (!File::getByID($cpCardLoop_item['cpCardImg'])) {
                unset($cpCardLoop_item['cpCardImg']);
            }
        }
        $this->set('cpCardLoop', $cpCardLoop);
        $this->set('cpCardLoop_items', $cpCardLoop_items);
    }

    protected function addEdit()
    {
        $cpCardLoop = array();
        $this->set("cpCardLink_Options", $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
), true));
        $this->set('cpCardLoop', $cpCardLoop);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/component_card/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/component_card/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/component_card/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->requireAsset('redactor');
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $db = Database::connection();
        $rows = $db->fetchAll('SELECT * FROM btComponentCardCpCardLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $cpCardLoop_items = isset($args['cpCardLoop']) && is_array($args['cpCardLoop']) ? $args['cpCardLoop'] : array();
        $queries = array();
        if (!empty($cpCardLoop_items)) {
            $i = 0;
            foreach ($cpCardLoop_items as $cpCardLoop_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($cpCardLoop_item['cpCardTitle']) && trim($cpCardLoop_item['cpCardTitle']) != '') {
                    $data['cpCardTitle'] = trim($cpCardLoop_item['cpCardTitle']);
                } else {
                    $data['cpCardTitle'] = null;
                }
                $data['cpCardTitle_1'] = isset($cpCardLoop_item['cpCardTitle_1']) ? LinkAbstractor::translateTo($cpCardLoop_item['cpCardTitle_1']) : null;
                if (isset($cpCardLoop_item['cpCardImg']) && trim($cpCardLoop_item['cpCardImg']) != '') {
                    $data['cpCardImg'] = trim($cpCardLoop_item['cpCardImg']);
                } else {
                    $data['cpCardImg'] = null;
                }
                if (isset($cpCardLoop_item['cpCardLink']) && trim($cpCardLoop_item['cpCardLink']) != '') {
					$data['cpCardLink_Title'] = $cpCardLoop_item['cpCardLink_Title'];
					$data['cpCardLink'] = $cpCardLoop_item['cpCardLink'];
					switch ($cpCardLoop_item['cpCardLink']) {
						case 'page':
							$data['cpCardLink_Page'] = $cpCardLoop_item['cpCardLink_Page'];
							$data['cpCardLink_File'] = '0';
							$data['cpCardLink_URL'] = '';
							$data['cpCardLink_Relative_URL'] = '';
							$data['cpCardLink_Image'] = '0';
							break;
                        case 'file':
							$data['cpCardLink_File'] = $cpCardLoop_item['cpCardLink_File'];
							$data['cpCardLink_Page'] = '0';
							$data['cpCardLink_URL'] = '';
							$data['cpCardLink_Relative_URL'] = '';
							$data['cpCardLink_Image'] = '0';
							break;
                        case 'url':
							$data['cpCardLink_URL'] = $cpCardLoop_item['cpCardLink_URL'];
							$data['cpCardLink_Page'] = '0';
							$data['cpCardLink_File'] = '0';
							$data['cpCardLink_Relative_URL'] = '';
							$data['cpCardLink_Image'] = '0';
							break;
                        case 'relative_url':
							$data['cpCardLink_Relative_URL'] = $cpCardLoop_item['cpCardLink_Relative_URL'];
							$data['cpCardLink_Page'] = '0';
							$data['cpCardLink_File'] = '0';
							$data['cpCardLink_URL'] = '';
							$data['cpCardLink_Image'] = '0';
							break;
                        case 'image':
							$data['cpCardLink_Image'] = $cpCardLoop_item['cpCardLink_Image'];
							$data['cpCardLink_Page'] = '0';
							$data['cpCardLink_File'] = '0';
							$data['cpCardLink_URL'] = '';
							$data['cpCardLink_Relative_URL'] = '';
							break;
                        default:
							$data['cpCardLink'] = '';
							$data['cpCardLink_Page'] = '0';
							$data['cpCardLink_File'] = '0';
							$data['cpCardLink_URL'] = '';
							$data['cpCardLink_Relative_URL'] = '';
							$data['cpCardLink_Image'] = '0';
							break;	
					}
				}
				else {
					$data['cpCardLink'] = '';
					$data['cpCardLink_Title'] = '';
					$data['cpCardLink_Page'] = '0';
					$data['cpCardLink_File'] = '0';
					$data['cpCardLink_URL'] = '';
					$data['cpCardLink_Relative_URL'] = '';
					$data['cpCardLink_Image'] = '0';
				}
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btComponentCardCpCardLoopEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btComponentCardCpCardLoopEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btComponentCardCpCardLoopEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        $cpCardLoopEntriesMin = 0;
        $cpCardLoopEntriesMax = 0;
        $cpCardLoopEntriesErrors = 0;
        $cpCardLoop = array();
        if (isset($args['cpCardLoop']) && is_array($args['cpCardLoop']) && !empty($args['cpCardLoop'])) {
            if ($cpCardLoopEntriesMin >= 1 && count($args['cpCardLoop']) < $cpCardLoopEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("カード"), $cpCardLoopEntriesMin, count($args['cpCardLoop'])));
                $cpCardLoopEntriesErrors++;
            }
            if ($cpCardLoopEntriesMax >= 1 && count($args['cpCardLoop']) > $cpCardLoopEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("カード"), $cpCardLoopEntriesMax, count($args['cpCardLoop'])));
                $cpCardLoopEntriesErrors++;
            }
            if ($cpCardLoopEntriesErrors == 0) {
                foreach ($args['cpCardLoop'] as $cpCardLoop_k => $cpCardLoop_v) {
                    if (is_array($cpCardLoop_v)) {
                        if (in_array("cpCardTitle", $this->btFieldsRequired['cpCardLoop']) && (!isset($cpCardLoop_v['cpCardTitle']) || trim($cpCardLoop_v['cpCardTitle']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("タイトル"), t("カード"), $cpCardLoop_k));
                        }
                        if (in_array("cpCardTitle_1", $this->btFieldsRequired['cpCardLoop']) && (!isset($cpCardLoop_v['cpCardTitle_1']) || trim($cpCardLoop_v['cpCardTitle_1']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("テキスト"), t("カード"), $cpCardLoop_k));
                        }
                        if (in_array("cpCardImg", $this->btFieldsRequired['cpCardLoop']) && (!isset($cpCardLoop_v['cpCardImg']) || trim($cpCardLoop_v['cpCardImg']) == "" || !is_object(File::getByID($cpCardLoop_v['cpCardImg'])))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("画像"), t("カード"), $cpCardLoop_k));
                        }
                        if ((in_array("cpCardLink", $this->btFieldsRequired['cpCardLoop']) && (!isset($cpCardLoop_v['cpCardLink']) || trim($cpCardLoop_v['cpCardLink']) == "")) || (isset($cpCardLoop_v['cpCardLink']) && trim($cpCardLoop_v['cpCardLink']) != "" && !array_key_exists($cpCardLoop_v['cpCardLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
))))) {
							$e->add(t("The %s field has an invalid value.", t("リンク")));
						} elseif (array_key_exists($cpCardLoop_v['cpCardLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
)))) {
							switch ($cpCardLoop_v['cpCardLink']) {
								case 'page':
									if (!isset($cpCardLoop_v['cpCardLink_Page']) || trim($cpCardLoop_v['cpCardLink_Page']) == "" || $cpCardLoop_v['cpCardLink_Page'] == "0" || (($page = Page::getByID($cpCardLoop_v['cpCardLink_Page'])) && $page->error !== false)) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Page"), t("リンク"), t("カード"), $cpCardLoop_k));
									}
									break;
				                case 'file':
									if (!isset($cpCardLoop_v['cpCardLink_File']) || trim($cpCardLoop_v['cpCardLink_File']) == "" || !is_object(File::getByID($cpCardLoop_v['cpCardLink_File']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("File"), t("リンク"), t("カード"), $cpCardLoop_k));
									}
									break;
				                case 'url':
									if (!isset($cpCardLoop_v['cpCardLink_URL']) || trim($cpCardLoop_v['cpCardLink_URL']) == "" || !filter_var($cpCardLoop_v['cpCardLink_URL'], FILTER_VALIDATE_URL)) {
										$e->add(t("The %s field for '%s' does not have a valid URL (%s, row #%s).", t("URL"), t("リンク"), t("カード"), $cpCardLoop_k));
									}
									break;
				                case 'relative_url':
									if (!isset($cpCardLoop_v['cpCardLink_Relative_URL']) || trim($cpCardLoop_v['cpCardLink_Relative_URL']) == "") {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Relative URL"), t("リンク"), t("カード"), $cpCardLoop_k));
									}
									break;
				                case 'image':
									if (!isset($cpCardLoop_v['cpCardLink_Image']) || trim($cpCardLoop_v['cpCardLink_Image']) == "" || !is_object(File::getByID($cpCardLoop_v['cpCardLink_Image']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Image"), t("リンク"), t("カード"), $cpCardLoop_k));
									}
									break;	
							}
						}
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('カード'), $cpCardLoop_k));
                    }
                }
            }
        } else {
            if ($cpCardLoopEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("カード"), $cpCardLoopEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-component_card', 'blocks/component_card/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-component_card');
        $this->edit();
    }

        protected function getSmartLinkTypeOptions($include = array(), $checkNone = false)
	{
		$options = array(
			''             => sprintf("-- %s--", t("None")),
			'page'         => t("Page"),
			'url'          => t("External URL"),
			'relative_url' => t("Relative URL"),
			'file'         => t("File"),
			'image'        => t("Image")
		);
		if ($checkNone) {
            $include = array_merge(array(''), $include);
        }
		$return = array();
		foreach($include as $v){
		    if(isset($options[$v])){
		        $return[$v] = $options[$v];
		    }
		}
		return $return;
	}
}