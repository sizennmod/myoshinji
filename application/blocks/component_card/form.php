<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
            <?php
	$core_editor = Core::make('editor');
	if (method_exists($core_editor, 'getEditorInitJSFunction')) {
		/* @var $core_editor \Concrete\Core\Editor\CkeditorEditor */
		?>
		<script type="text/javascript">var blockDesignerEditor = <?php echo $core_editor->getEditorInitJSFunction(); ?>;</script>
	<?php
	} else {
	/* @var $core_editor \Concrete\Core\Editor\RedactorEditor */
	?>
		<script type="text/javascript">
			var blockDesignerEditor = function (identifier) {$(identifier).redactor(<?php echo json_encode(array('plugins' => array("concrete5lightbox", "undoredo", "specialcharacters", "table", "concrete5magic"), 'minHeight' => 300,'concrete5' => array('filemanager' => $core_editor->allowFileManager(), 'sitemap' => $core_editor->allowSitemap()))); ?>).on('remove', function () {$(this).redactor('core.destroy');});};
		</script>
		<?php
	} ?><?php $repeatable_container_id = 'btComponentCard-cpCardLoop-container-' . $identifier_getString; ?>
    <div id="<?php echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $cpCardLoop_items,
                        'order' => array_keys($cpCardLoop_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php echo t('カード') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardTitle]" class="control-label"><?php echo t("タイトル"); ?></label>
    <?php echo isset($btFieldsRequired['cpCardLoop']) && in_array('cpCardTitle', $btFieldsRequired['cpCardLoop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardTitle]" id="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardTitle]" class="form-control" rows="5">{{ cpCardTitle }}</textarea>
</div>            <div class="form-group">
    <label for="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardTitle_1]" class="control-label"><?php echo t("テキスト"); ?></label>
    <?php echo isset($btFieldsRequired['cpCardLoop']) && in_array('cpCardTitle_1', $btFieldsRequired['cpCardLoop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardTitle_1]" id="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardTitle_1]" class="ft-cpCardLoop-cpCardTitle_1">{{ cpCardTitle_1 }}</textarea>
</div>            <div class="form-group">
    <label for="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardImg]" class="control-label"><?php echo t("画像"); ?></label>
    <?php echo isset($btFieldsRequired['cpCardLoop']) && in_array('cpCardImg', $btFieldsRequired['cpCardLoop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-file-selector-input-name="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardImg]" class="ccm-file-selector ft-image-cpCardImg-file-selector" data-file-selector-f-id="{{ cpCardImg }}"></div>
</div>            <?php $cpCardLink_ContainerID = 'btComponentCard-cpCardLink-container-' . $identifier_getString; ?>
<div class="ft-smart-link" id="<?php echo $cpCardLink_ContainerID; ?>">
	<div class="form-group">
		<label for="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink]" class="control-label"><?php echo t("リンク"); ?></label>
	    <?php echo isset($btFieldsRequired['cpCardLoop']) && in_array('cpCardLink', $btFieldsRequired['cpCardLoop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
	    <?php $cpCardLoopCpCardLink_options = $cpCardLink_Options; ?>
                    <select name="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink]" id="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink]" class="form-control ft-smart-link-type">{{#select cpCardLink}}<?php foreach ($cpCardLoopCpCardLink_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
	</div>
	
	<div class="form-group">
		<div class="ft-smart-link-options hidden" style="padding-left: 10px;">
			<div class="form-group">
				<label for="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_Title]" class="control-label"><?php echo t("Title"); ?></label>
			    <input name="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_Title]" id="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_Title]" class="form-control" type="text" value="{{ cpCardLink_Title }}" />		
			</div>
			
			<div class="form-group hidden" data-link-type="page">
			<label for="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_Page]" class="control-label"><?php echo t("Page"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-page-selector="{{token}}" data-input-name="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_Page]" data-cID="{{cpCardLink_Page}}"></div>
		</div>

		<div class="form-group hidden" data-link-type="url">
			<label for="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_URL]" id="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_URL]" class="form-control" type="text" value="{{ cpCardLink_URL }}" />
		</div>

		<div class="form-group hidden" data-link-type="relative_url">
			<label for="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_Relative_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_Relative_URL]" id="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_Relative_URL]" class="form-control" type="text" value="{{ cpCardLink_Relative_URL }}" />
		</div>

		<div class="form-group hidden" data-link-type="file">
		    <label for="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_File]" class="control-label"><?php echo t("File"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_File]" class="ccm-file-selector" data-file-selector-f-id="{{ cpCardLink_File }}"></div>	
		</div>

		<div class="form-group hidden" data-link-type="image">
			<label for="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_Image]" class="control-label"><?php echo t("Image"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('cpCardLoop'); ?>[{{id}}][cpCardLink_Image]" class="ccm-file-selector" data-file-selector-f-id="{{ cpCardLink_Image }}"></div>
		</div>
		</div>
	</div>
</div>
</div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btComponentCard.cpCardLoop.edit.open', {id: '<?php echo $repeatable_container_id; ?>'});
    $.each($('#<?php echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>