<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if (!empty($cpCardLoop_items)) { ?>
<div class="c-section_columns c-section_columns--column_3">
	<?php foreach ($cpCardLoop_items as $cpCardLoop_item_key => $cpCardLoop_item) { ?>
	<?php
	if (trim($cpCardLoop_item["cpCardLink_URL"]) != "") { ?>
	    <?php
		$cpCardLoop_itemcpCardLink_Attributes = array();
		$cpCardLoop_itemcpCardLink_Attributes['href'] = $cpCardLoop_item["cpCardLink_URL"];
		$cpCardLoop_item["cpCardLink_AttributesHtml"] = join(' ', array_map(function ($key) use ($cpCardLoop_itemcpCardLink_Attributes) {
			return $key . '="' . $cpCardLoop_itemcpCardLink_Attributes[$key] . '"';
		}, array_keys($cpCardLoop_itemcpCardLink_Attributes)));
		//echo sprintf('<a %s>%s</a>', $cpCardLoop_item["cpCardLink_AttributesHtml"], $cpCardLoop_item["cpCardLink_Title"]); ?><?php
	} ?>
	<div class="c-section_column">
		<a <?= $cpCardLoop_item["cpCardLink_AttributesHtml"];?> class="e-box e-box--line_gray">
			<?php if (isset($cpCardLoop_item["cpCardTitle"]) && trim($cpCardLoop_item["cpCardTitle"]) != "") { ?>
				<h2 class="c-section_head2"><?php echo h($cpCardLoop_item["cpCardTitle"]); ?></h2>
			<?php } ?>
			 <?php if (isset($cpCardLoop_item["cpCardTitle_1"]) && trim($cpCardLoop_item["cpCardTitle_1"]) != "") { ?>
	    		<div class="c-section_text"><?php echo $cpCardLoop_item["cpCardTitle_1"]; ?></div>
	    	<?php } ?>
			<?php if ($cpCardLoop_item["cpCardImg"]) { ?>
			<div class="c-section_figure">
				<?php
				$cpCardLoop_item["cpCardImg_tag"] = Core::make('html/image', array($cpCardLoop_item["cpCardImg"]))->getTag();
				$cpCardLoop_item["cpCardImg_tag"]->alt($cpCardLoop_item["cpCardImg"]->getTitle());
				echo $cpCardLoop_item["cpCardImg_tag"];
				?>
			</div>
			<?php } ?>
		</a>
	</div><!--column-->
	<?php } ?>
</div>
<?php } ?>