<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if (isset($ttlSchedule) && trim($ttlSchedule) != "") { ?>
    <?php echo h($ttlSchedule); ?><?php } ?>
<?php if (isset($contentSchedule) && trim($contentSchedule) != "") { ?>
    <?php echo $contentSchedule; ?><?php } ?>
<?php if (!empty($loopSchedule_items)) { ?>
    <?php foreach ($loopSchedule_items as $loopSchedule_item_key => $loopSchedule_item) { ?><?php if (isset($loopSchedule_item["dateSchedule"]) && $loopSchedule_item["dateSchedule"] > 0) { ?>
    <?php echo strftime("%A %d %B %Y",$loopSchedule_item["dateSchedule"]); ?><?php } ?><?php if (isset($loopSchedule_item["textDateSchedule"]) && trim($loopSchedule_item["textDateSchedule"]) != "") { ?>
    <?php echo h($loopSchedule_item["textDateSchedule"]); ?><?php } ?><?php if (isset($loopSchedule_item["textDetailSchedule"]) && trim($loopSchedule_item["textDetailSchedule"]) != "") { ?>
    <?php echo $loopSchedule_item["textDetailSchedule"]; ?><?php } ?><?php } ?><?php } ?>