<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php $tabs = array(
    array('form-basics-' . $identifier_getString, t('Basics'), true),
    array('form-loopSchedule_items-' . $identifier_getString, t('スケジュール'))
);
echo Core::make('helper/concrete/ui')->tabs($tabs); ?>

<div class="ccm-tab-content" id="ccm-tab-content-form-basics-<?php echo $identifier_getString; ?>">
    <div class="form-group">
    <?php echo $form->label('ttlSchedule', t("タイトル")); ?>
    <?php echo isset($btFieldsRequired) && in_array('ttlSchedule', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->text($view->field('ttlSchedule'), $ttlSchedule, array (
  'maxlength' => 255,
)); ?>
</div><div class="form-group">
    <?php echo $form->label('contentSchedule', t("概要文")); ?>
    <?php echo isset($btFieldsRequired) && in_array('contentSchedule', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo Core::make('editor')->outputBlockEditModeEditor($view->field('contentSchedule'), $contentSchedule); ?>
</div>
</div>

<div class="ccm-tab-content" id="ccm-tab-content-form-loopSchedule_items-<?php echo $identifier_getString; ?>">
    <script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
            <?php
	$core_editor = Core::make('editor');
	if (method_exists($core_editor, 'getEditorInitJSFunction')) {
		/* @var $core_editor \Concrete\Core\Editor\CkeditorEditor */
		?>
		<script type="text/javascript">var blockDesignerEditor = <?php echo $core_editor->getEditorInitJSFunction(); ?>;</script>
	<?php
	} else {
	/* @var $core_editor \Concrete\Core\Editor\RedactorEditor */
	?>
		<script type="text/javascript">
			var blockDesignerEditor = function (identifier) {$(identifier).redactor(<?php echo json_encode(array('plugins' => array("concrete5lightbox", "undoredo", "specialcharacters", "table", "concrete5magic"), 'minHeight' => 300,'concrete5' => array('filemanager' => $core_editor->allowFileManager(), 'sitemap' => $core_editor->allowSitemap()))); ?>).on('remove', function () {$(this).redactor('core.destroy');});};
		</script>
		<?php
	} ?><?php $repeatable_container_id = 'btComponentSchedule-loopSchedule-container-' . $identifier_getString; ?>
    <div id="<?php echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $loopSchedule_items,
                        'order' => array_keys($loopSchedule_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php echo t('スケジュール') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php echo $view->field('loopSchedule'); ?>[{{id}}][dateSchedule]" class="control-label"><?php echo t("日付"); ?></label>
    <?php echo isset($btFieldsRequired['loopSchedule']) && in_array('dateSchedule', $btFieldsRequired['loopSchedule']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php echo $view->field('loopSchedule'); ?>[{{id}}][dateSchedule]" id="<?php echo $view->field('loopSchedule'); ?>[{{id}}][dateSchedule]" class="ft-datetime-loopSchedule-dateSchedule" type="text" value="{{formatDate dateSchedule 'MM/DD/YYYY'}}" autocomplete="off" />
</div>            <div class="form-group">
    <label for="<?php echo $view->field('loopSchedule'); ?>[{{id}}][textDateSchedule]" class="control-label"><?php echo t("表示用日付"); ?></label>
    <?php echo isset($btFieldsRequired['loopSchedule']) && in_array('textDateSchedule', $btFieldsRequired['loopSchedule']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php echo $view->field('loopSchedule'); ?>[{{id}}][textDateSchedule]" id="<?php echo $view->field('loopSchedule'); ?>[{{id}}][textDateSchedule]" class="form-control" type="text" value="{{ textDateSchedule }}" maxlength="255" />
</div>            <div class="form-group">
    <label for="<?php echo $view->field('loopSchedule'); ?>[{{id}}][textDetailSchedule]" class="control-label"><?php echo t("テキスト"); ?></label>
    <?php echo isset($btFieldsRequired['loopSchedule']) && in_array('textDetailSchedule', $btFieldsRequired['loopSchedule']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('loopSchedule'); ?>[{{id}}][textDetailSchedule]" id="<?php echo $view->field('loopSchedule'); ?>[{{id}}][textDetailSchedule]" class="ft-loopSchedule-textDetailSchedule">{{ textDetailSchedule }}</textarea>
</div></div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btComponentSchedule.loopSchedule.edit.open', {id: '<?php echo $repeatable_container_id; ?>'});
    $.each($('#<?php echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>
</div>