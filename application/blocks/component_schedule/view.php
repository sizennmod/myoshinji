<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div class="c-section">
	<?php if (isset($ttlSchedule) && trim($ttlSchedule) != "") { ?>
		<h2 class="c-section_head"><?php echo h($ttlSchedule); ?></h2>
	<?php } ?>
	
	<?php if (isset($contentSchedule) && trim($contentSchedule) != "") { ?>
		<div class="c-noteArea">
		<div class="c-section_text"><?php echo $contentSchedule; ?></div>
		</div>
	<?php } ?>
	
	<?php if (!empty($loopSchedule_items)) { ?>
		<?php
		$sorted = array();
		foreach ($loopSchedule_items as $loopSchedule_item_key => $loopSchedule_item) {
			$m = date('m',$loopSchedule_item["dateSchedule"]);
			$loopSchedule_item['date'] = date('d',$loopSchedule_item["dateSchedule"]);
			$sorted[$m][] = $loopSchedule_item;
		}
		ksort($sorted);

		?>
		<div class="c-table">
			<div class="c-table_frame">
				 <table class="c-table_table">
					<tbody class="c-table_tbody">
						<?php
						$weekday = array( "日", "月", "火", "水", "木", "金", "土" );

						if($sorted){
							foreach($sorted as $sort){
								$rs = count($sort);
								$lc = 0;

								usort($sort, function($a,$b){return $a['date']-$b['date'];});
						?>
							<?php 
							foreach($sort as $st){
								$m = date('n',$st["dateSchedule"]);
								$j = date('j',$st["dateSchedule"]);
							?>
							<tr class="c-table_row">
								<?php if($lc == 0){?>
								<th rowspan="<?= $rs;?>" width="10%" class="c-table_head"><?= $m;?>月</th>
								<?php }?>
								<th width="20%" class="c-table_head"> 
									<?php if (isset($st["textDateSchedule"]) && trim($st["textDateSchedule"]) != "") { ?>
										<?php echo h($st["textDateSchedule"]); ?>
									<?php }else{ ?>
										<?= $j;?>日
									<?php } ?>
								</th>
								<td class="c-table_data">
									<?php if (isset($st["textDetailSchedule"]) && trim($st["textDetailSchedule"]) != "") { ?>
										<?php echo $st["textDetailSchedule"]; ?>
									<?php } ?>
								</td>
							</tr>
							<?php $lc++;}?>
							<?php } ?>
						<?php } ?>
					</tbody>
				  </table>
			</div><!--frame-->
		</div><!--table-->
	<?php } ?>
</div>