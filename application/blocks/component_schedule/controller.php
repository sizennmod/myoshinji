<?php namespace Application\Block\ComponentSchedule;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Concrete\Core\Editor\LinkAbstractor;
use Database;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('loopSchedule' => array());
    protected $btExportFileColumns = array();
    protected $btExportPageColumns = array();
    protected $btExportTables = array('btComponentSchedule', 'btComponentScheduleLoopScheduleEntries');
    protected $btTable = 'btComponentSchedule';
    protected $btInterfaceWidth = 800;
    protected $btInterfaceHeight = 600;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $btDefaultSet = 'schedule';
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("スケジュール");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->ttlSchedule;
        $content[] = $this->contentSchedule;
        $db = Database::connection();
        $loopSchedule_items = $db->fetchAll('SELECT * FROM btComponentScheduleLoopScheduleEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($loopSchedule_items as $loopSchedule_item_k => $loopSchedule_item_v) {
            if (isset($loopSchedule_item_v["textDateSchedule"]) && trim($loopSchedule_item_v["textDateSchedule"]) != "") {
                $content[] = $loopSchedule_item_v["textDateSchedule"];
            }
            if (isset($loopSchedule_item_v["textDetailSchedule"]) && trim($loopSchedule_item_v["textDetailSchedule"]) != "") {
                $content[] = $loopSchedule_item_v["textDetailSchedule"];
            }
        }
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $this->set('contentSchedule', LinkAbstractor::translateFrom($this->contentSchedule));
        $loopSchedule = array();
        $loopSchedule_items = $db->fetchAll('SELECT * FROM btComponentScheduleLoopScheduleEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($loopSchedule_items as $loopSchedule_item_k => &$loopSchedule_item_v) {
            $loopSchedule_item_v["textDetailSchedule"] = isset($loopSchedule_item_v["textDetailSchedule"]) ? LinkAbstractor::translateFrom($loopSchedule_item_v["textDetailSchedule"]) : null;
        }
        $this->set('loopSchedule_items', $loopSchedule_items);
        $this->set('loopSchedule', $loopSchedule);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btComponentScheduleLoopScheduleEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $loopSchedule_items = $db->fetchAll('SELECT * FROM btComponentScheduleLoopScheduleEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($loopSchedule_items as $loopSchedule_item) {
            unset($loopSchedule_item['id']);
            $loopSchedule_item['bID'] = $newBID;
            $db->insert('btComponentScheduleLoopScheduleEntries', $loopSchedule_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $loopSchedule = $this->get('loopSchedule');
        $this->set('loopSchedule_items', array());
        $this->set('loopSchedule', $loopSchedule);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        
        $this->set('contentSchedule', LinkAbstractor::translateFromEditMode($this->contentSchedule));
        $loopSchedule = $this->get('loopSchedule');
        $loopSchedule_items = $db->fetchAll('SELECT * FROM btComponentScheduleLoopScheduleEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        
        foreach ($loopSchedule_items as &$loopSchedule_item) {
            $loopSchedule_item['textDetailSchedule'] = isset($loopSchedule_item['textDetailSchedule']) ? LinkAbstractor::translateFromEditMode($loopSchedule_item['textDetailSchedule']) : null;
        }
        $this->set('loopSchedule', $loopSchedule);
        $this->set('loopSchedule_items', $loopSchedule_items);
    }

    protected function addEdit()
    {
        $loopSchedule = array();
        $this->set('loopSchedule', $loopSchedule);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/component_schedule/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/component_schedule/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/component_schedule/js_form/handlebars-helpers.js', array(), $this->pkg);
        $al->register('css', 'datetimepicker', 'blocks/component_schedule/css_form/bootstrap-datetimepicker.min.css', array(), $this->pkg);
        $al->register('css', 'bootstrap_fonts', 'blocks/component_schedule/css_form/bootstrap.fonts.css', array(), $this->pkg);
        $al->register('javascript', 'moment', 'blocks/component_schedule/js_form/moment.js', array(), $this->pkg);
        $al->register('javascript', 'bootstrap', 'blocks/component_schedule/js_form/bootstrap.min.js', array(), $this->pkg);
        $al->register('javascript', 'datetimepicker', 'blocks/component_schedule/js_form/bootstrap-datetimepicker.min.js', array(), $this->pkg);
        $this->requireAsset('redactor');
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->requireAsset('css', 'datetimepicker');
        $this->requireAsset('css', 'bootstrap_fonts');
        $this->requireAsset('javascript', 'moment');
        $this->requireAsset('javascript', 'bootstrap');
        $this->requireAsset('javascript', 'datetimepicker');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $db = Database::connection();
        $args['contentSchedule'] = LinkAbstractor::translateTo($args['contentSchedule']);
        $rows = $db->fetchAll('SELECT * FROM btComponentScheduleLoopScheduleEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $loopSchedule_items = isset($args['loopSchedule']) && is_array($args['loopSchedule']) ? $args['loopSchedule'] : array();
        $queries = array();
        if (!empty($loopSchedule_items)) {
            $i = 0;
            foreach ($loopSchedule_items as $loopSchedule_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($loopSchedule_item['dateSchedule']) && trim($loopSchedule_item['dateSchedule']) != '') {
                    $data['dateSchedule'] = strtotime(substr($loopSchedule_item['dateSchedule'], 0, 22));
                } else {
                    $data['dateSchedule'] = null;
                }
                if (isset($loopSchedule_item['textDateSchedule']) && trim($loopSchedule_item['textDateSchedule']) != '') {
                    $data['textDateSchedule'] = trim($loopSchedule_item['textDateSchedule']);
                } else {
                    $data['textDateSchedule'] = null;
                }
                $data['textDetailSchedule'] = isset($loopSchedule_item['textDetailSchedule']) ? LinkAbstractor::translateTo($loopSchedule_item['textDetailSchedule']) : null;
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btComponentScheduleLoopScheduleEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btComponentScheduleLoopScheduleEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btComponentScheduleLoopScheduleEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("ttlSchedule", $this->btFieldsRequired) && (trim($args["ttlSchedule"]) == "")) {
            $e->add(t("The %s field is required.", t("タイトル")));
        }
        if (in_array("contentSchedule", $this->btFieldsRequired) && (trim($args["contentSchedule"]) == "")) {
            $e->add(t("The %s field is required.", t("概要文")));
        }
        $loopScheduleEntriesMin = 0;
        $loopScheduleEntriesMax = 0;
        $loopScheduleEntriesErrors = 0;
        $loopSchedule = array();
        if (isset($args['loopSchedule']) && is_array($args['loopSchedule']) && !empty($args['loopSchedule'])) {
            if ($loopScheduleEntriesMin >= 1 && count($args['loopSchedule']) < $loopScheduleEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("スケジュール"), $loopScheduleEntriesMin, count($args['loopSchedule'])));
                $loopScheduleEntriesErrors++;
            }
            if ($loopScheduleEntriesMax >= 1 && count($args['loopSchedule']) > $loopScheduleEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("スケジュール"), $loopScheduleEntriesMax, count($args['loopSchedule'])));
                $loopScheduleEntriesErrors++;
            }
            if ($loopScheduleEntriesErrors == 0) {
                foreach ($args['loopSchedule'] as $loopSchedule_k => $loopSchedule_v) {
                    if (is_array($loopSchedule_v)) {
                        if (in_array("dateSchedule", $this->btFieldsRequired['loopSchedule']) && (!isset($loopSchedule_v['dateSchedule']) || trim($loopSchedule_v['dateSchedule']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("日付"), t("スケジュール"), $loopSchedule_k));
                        } elseif (isset($loopSchedule_v['dateSchedule']) && trim($loopSchedule_v['dateSchedule']) != "" && strtotime($loopSchedule_v['dateSchedule']) <= 0) {
                            $e->add(t("The %s field is not a valid date (%s, row #%s).", t("日付"), t("スケジュール"), $loopSchedule_k));
                        }
                        if (in_array("textDateSchedule", $this->btFieldsRequired['loopSchedule']) && (!isset($loopSchedule_v['textDateSchedule']) || trim($loopSchedule_v['textDateSchedule']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("表示用日付"), t("スケジュール"), $loopSchedule_k));
                        }
                        if (in_array("textDetailSchedule", $this->btFieldsRequired['loopSchedule']) && (!isset($loopSchedule_v['textDetailSchedule']) || trim($loopSchedule_v['textDetailSchedule']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("テキスト"), t("スケジュール"), $loopSchedule_k));
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('スケジュール'), $loopSchedule_k));
                    }
                }
            }
        } else {
            if ($loopScheduleEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("スケジュール"), $loopScheduleEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('css', 'datetimepicker-composer', 'blocks/component_schedule/css_form/bootstrap-datetimepicker-composer.css', array(), $this->pkg);
        $al->register('javascript', 'auto-js-component_schedule', 'blocks/component_schedule/auto.js', array(), $this->pkg);
        $this->requireAsset('css', 'datetimepicker-composer');
        $this->requireAsset('javascript', 'auto-js-component_schedule');
        $this->edit();
    }
}