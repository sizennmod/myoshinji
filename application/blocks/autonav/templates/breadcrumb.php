<?php defined('C5_EXECUTE') or die("Access Denied.");

$navItems = $controller->getNavItems(true); // Ignore exclude from nav
$c = Page::getCurrentPage();

if (count($navItems) > 0) {
    echo '<nav class="g-breadcrumb">';
    echo '<div class="g-breadcrumb_inner u-inner">';
    echo '<ul class="g-breadcrumb_items">';

    foreach ($navItems as $ni) {
        if(!$ni->cObj->getAttribute('exclude_nav')){
            if($ni->isHome){
               $ni->name = 'トップページ';
            }
            if ($ni->isCurrent) {
                echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" class="active g-breadcrumb_item"><span class="e-name">' . $ni->name . '</span></li>';
            } else {
                echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" class="g-breadcrumb_item"><a class="e-box" href="' . $ni->url . '" target="' . $ni->target . '"><span itemprop="title" class="e-name">' . $ni->name . '</span></a></li>';
            }
        }
    }

    echo '</ul>';
    echo '</div><!--inner-->';
    echo '</nav>';
} elseif (is_object($c) && $c->isEditMode()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Auto-Nav Block.')?></div>
<?php 
}
