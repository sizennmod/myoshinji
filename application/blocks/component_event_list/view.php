<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div class="c-section">
	<?php if (isset($cpEventListTitle) && trim($cpEventListTitle) != "") { ?>
    	<h2 class="c-section_head"><?php echo nl2br(h($cpEventListTitle)); ?></h2>
    <?php } ?>
	
	<?php if (isset($cpEventListDesc) && trim($cpEventListDesc) != "") { ?>
		<div class="c-noteArea">
			<div class="c-section_text"><?php echo $cpEventListDesc; ?></div>
		</div><!--noteArea-->
    <?php } ?>
	<?php if (!empty($cpEventListLoop_items)) { ?>
		<div class="c-linkList">
		<?php foreach ($cpEventListLoop_items as $cpEventListLoop_item_key => $cpEventListLoop_item) { ?>
			<?php if (trim($cpEventListLoop_item["cpEventListLoopLink_URL"]) != "") { ?>
				<?php
				$cpEventListLoop_itemcpEventListLoopLink_Attributes = array();
				$cpEventListLoop_itemcpEventListLoopLink_Attributes['href'] = $cpEventListLoop_item["cpEventListLoopLink_URL"];
				$cpEventListLoop_item["cpEventListLoopLink_AttributesHtml"] = join(' ', array_map(function ($key) use ($cpEventListLoop_itemcpEventListLoopLink_Attributes) {
				return $key . '="' . $cpEventListLoop_itemcpEventListLoopLink_Attributes[$key] . '"';
				}, array_keys($cpEventListLoop_itemcpEventListLoopLink_Attributes)));
				// echo sprintf('<a %s>%s</a>', $cpEventListLoop_item["cpEventListLoopLink_AttributesHtml"], $cpEventListLoop_item["cpEventListLoopLink_Title"]); ?>
			<?php } ?>
			<p class="c-linkList_item">
				<a <?= $cpEventListLoop_item["cpEventListLoopLink_AttributesHtml"];?> class="e-box">
					<?php if (isset($cpEventListLoop_item["cpEventListLoopTitle"]) && trim($cpEventListLoop_item["cpEventListLoopTitle"]) != "") { ?>
						<strong class="c-linkList_head"><?php echo nl2br(h($cpEventListLoop_item["cpEventListLoopTitle"])); ?></strong>
					<?php } ?>
					<?php if (isset($cpEventListLoop_item["cpEventListLoopDesc"]) && trim($cpEventListLoop_item["cpEventListLoopDesc"]) != "") { ?>
						<span class="c-linkList_text"><?php echo nl2br(h($cpEventListLoop_item["cpEventListLoopDesc"])); ?></span>
					<?php } ?>
				</a>
			</p>
		<?php } ?>
		</div>
	<?php } ?>
</div>