<?php namespace Application\Block\ComponentEventList;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Concrete\Core\Editor\LinkAbstractor;
use Database;
use Permissions;
use File;
use Page;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('cpEventListLoop' => array());
    protected $btExportFileColumns = array();
    protected $btExportPageColumns = array();
    protected $btExportTables = array('btComponentEventList', 'btComponentEventListCpEventListLoopEntries');
    protected $btTable = 'btComponentEventList';
    protected $btInterfaceWidth = 800;
    protected $btInterfaceHeight = 600;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("イベント・行事用ブロック");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->cpEventListTitle;
        $content[] = $this->cpEventListDesc;
        $db = Database::connection();
        $cpEventListLoop_items = $db->fetchAll('SELECT * FROM btComponentEventListCpEventListLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpEventListLoop_items as $cpEventListLoop_item_k => $cpEventListLoop_item_v) {
            if (isset($cpEventListLoop_item_v["cpEventListLoopTitle"]) && trim($cpEventListLoop_item_v["cpEventListLoopTitle"]) != "") {
                $content[] = $cpEventListLoop_item_v["cpEventListLoopTitle"];
            }
            if (isset($cpEventListLoop_item_v["cpEventListLoopDesc"]) && trim($cpEventListLoop_item_v["cpEventListLoopDesc"]) != "") {
                $content[] = $cpEventListLoop_item_v["cpEventListLoopDesc"];
            }
        }
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $this->set('cpEventListDesc', LinkAbstractor::translateFrom($this->cpEventListDesc));
        $cpEventListLoop = array();
        $cpEventListLoop_items = $db->fetchAll('SELECT * FROM btComponentEventListCpEventListLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpEventListLoop_items as $cpEventListLoop_item_k => &$cpEventListLoop_item_v) {
            $cpEventListLoop_item_v["cpEventListLoopLink_Object"] = null;
			$cpEventListLoop_item_v["cpEventListLoopLink_Title"] = trim($cpEventListLoop_item_v["cpEventListLoopLink_Title"]);
			if (isset($cpEventListLoop_item_v["cpEventListLoopLink"]) && trim($cpEventListLoop_item_v["cpEventListLoopLink"]) != '') {
				switch ($cpEventListLoop_item_v["cpEventListLoopLink"]) {
					case 'page':
						if ($cpEventListLoop_item_v["cpEventListLoopLink_Page"] > 0 && ($cpEventListLoop_item_v["cpEventListLoopLink_Page_c"] = Page::getByID($cpEventListLoop_item_v["cpEventListLoopLink_Page"])) && !$cpEventListLoop_item_v["cpEventListLoopLink_Page_c"]->error && !$cpEventListLoop_item_v["cpEventListLoopLink_Page_c"]->isInTrash()) {
							$cpEventListLoop_item_v["cpEventListLoopLink_Object"] = $cpEventListLoop_item_v["cpEventListLoopLink_Page_c"];
							$cpEventListLoop_item_v["cpEventListLoopLink_URL"] = $cpEventListLoop_item_v["cpEventListLoopLink_Page_c"]->getCollectionLink();
							if ($cpEventListLoop_item_v["cpEventListLoopLink_Title"] == '') {
								$cpEventListLoop_item_v["cpEventListLoopLink_Title"] = $cpEventListLoop_item_v["cpEventListLoopLink_Page_c"]->getCollectionName();
							}
						}
						break;
				    case 'file':
						$cpEventListLoop_item_v["cpEventListLoopLink_File_id"] = (int)$cpEventListLoop_item_v["cpEventListLoopLink_File"];
						if ($cpEventListLoop_item_v["cpEventListLoopLink_File_id"] > 0 && ($cpEventListLoop_item_v["cpEventListLoopLink_File_object"] = File::getByID($cpEventListLoop_item_v["cpEventListLoopLink_File_id"])) && is_object($cpEventListLoop_item_v["cpEventListLoopLink_File_object"])) {
							$fp = new Permissions($cpEventListLoop_item_v["cpEventListLoopLink_File_object"]);
							if ($fp->canViewFile()) {
								$cpEventListLoop_item_v["cpEventListLoopLink_Object"] = $cpEventListLoop_item_v["cpEventListLoopLink_File_object"];
								$cpEventListLoop_item_v["cpEventListLoopLink_URL"] = $cpEventListLoop_item_v["cpEventListLoopLink_File_object"]->getRelativePath();
								if ($cpEventListLoop_item_v["cpEventListLoopLink_Title"] == '') {
									$cpEventListLoop_item_v["cpEventListLoopLink_Title"] = $cpEventListLoop_item_v["cpEventListLoopLink_File_object"]->getTitle();
								}
							}
						}
						break;
				    case 'url':
						if ($cpEventListLoop_item_v["cpEventListLoopLink_Title"] == '') {
							$cpEventListLoop_item_v["cpEventListLoopLink_Title"] = $cpEventListLoop_item_v["cpEventListLoopLink_URL"];
						}
						break;
				    case 'relative_url':
						if ($cpEventListLoop_item_v["cpEventListLoopLink_Title"] == '') {
							$cpEventListLoop_item_v["cpEventListLoopLink_Title"] = $cpEventListLoop_item_v["cpEventListLoopLink_Relative_URL"];
						}
						$cpEventListLoop_item_v["cpEventListLoopLink_URL"] = $cpEventListLoop_item_v["cpEventListLoopLink_Relative_URL"];
						break;
				    case 'image':
						if ($cpEventListLoop_item_v["cpEventListLoopLink_Image"] > 0 && ($cpEventListLoop_item_v["cpEventListLoopLink_Image_object"] = File::getByID($cpEventListLoop_item_v["cpEventListLoopLink_Image"])) && is_object($cpEventListLoop_item_v["cpEventListLoopLink_Image_object"])) {
							$cpEventListLoop_item_v["cpEventListLoopLink_URL"] = $cpEventListLoop_item_v["cpEventListLoopLink_Image_object"]->getURL();
							$cpEventListLoop_item_v["cpEventListLoopLink_Object"] = $cpEventListLoop_item_v["cpEventListLoopLink_Image_object"];
							if ($cpEventListLoop_item_v["cpEventListLoopLink_Title"] == '') {
								$cpEventListLoop_item_v["cpEventListLoopLink_Title"] = $cpEventListLoop_item_v["cpEventListLoopLink_Image_object"]->getTitle();
							}
						}
						break;
				}
			}
        }
        $this->set('cpEventListLoop_items', $cpEventListLoop_items);
        $this->set('cpEventListLoop', $cpEventListLoop);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btComponentEventListCpEventListLoopEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $cpEventListLoop_items = $db->fetchAll('SELECT * FROM btComponentEventListCpEventListLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpEventListLoop_items as $cpEventListLoop_item) {
            unset($cpEventListLoop_item['id']);
            $cpEventListLoop_item['bID'] = $newBID;
            $db->insert('btComponentEventListCpEventListLoopEntries', $cpEventListLoop_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $cpEventListLoop = $this->get('cpEventListLoop');
        $this->set('cpEventListLoop_items', array());
        $this->set('cpEventListLoop', $cpEventListLoop);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        
        $this->set('cpEventListDesc', LinkAbstractor::translateFromEditMode($this->cpEventListDesc));
        $cpEventListLoop = $this->get('cpEventListLoop');
        $cpEventListLoop_items = $db->fetchAll('SELECT * FROM btComponentEventListCpEventListLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $this->set('cpEventListLoop', $cpEventListLoop);
        $this->set('cpEventListLoop_items', $cpEventListLoop_items);
    }

    protected function addEdit()
    {
        $cpEventListLoop = array();
        $this->set("cpEventListLoopLink_Options", $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
), true));
        $this->set('cpEventListLoop', $cpEventListLoop);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/component_event_list/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/component_event_list/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/component_event_list/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('redactor');
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $db = Database::connection();
        $args['cpEventListDesc'] = LinkAbstractor::translateTo($args['cpEventListDesc']);
        $rows = $db->fetchAll('SELECT * FROM btComponentEventListCpEventListLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $cpEventListLoop_items = isset($args['cpEventListLoop']) && is_array($args['cpEventListLoop']) ? $args['cpEventListLoop'] : array();
        $queries = array();
        if (!empty($cpEventListLoop_items)) {
            $i = 0;
            foreach ($cpEventListLoop_items as $cpEventListLoop_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($cpEventListLoop_item['cpEventListLoopTitle']) && trim($cpEventListLoop_item['cpEventListLoopTitle']) != '') {
                    $data['cpEventListLoopTitle'] = trim($cpEventListLoop_item['cpEventListLoopTitle']);
                } else {
                    $data['cpEventListLoopTitle'] = null;
                }
                if (isset($cpEventListLoop_item['cpEventListLoopDesc']) && trim($cpEventListLoop_item['cpEventListLoopDesc']) != '') {
                    $data['cpEventListLoopDesc'] = trim($cpEventListLoop_item['cpEventListLoopDesc']);
                } else {
                    $data['cpEventListLoopDesc'] = null;
                }
                if (isset($cpEventListLoop_item['cpEventListLoopLink']) && trim($cpEventListLoop_item['cpEventListLoopLink']) != '') {
					$data['cpEventListLoopLink_Title'] = $cpEventListLoop_item['cpEventListLoopLink_Title'];
					$data['cpEventListLoopLink'] = $cpEventListLoop_item['cpEventListLoopLink'];
					switch ($cpEventListLoop_item['cpEventListLoopLink']) {
						case 'page':
							$data['cpEventListLoopLink_Page'] = $cpEventListLoop_item['cpEventListLoopLink_Page'];
							$data['cpEventListLoopLink_File'] = '0';
							$data['cpEventListLoopLink_URL'] = '';
							$data['cpEventListLoopLink_Relative_URL'] = '';
							$data['cpEventListLoopLink_Image'] = '0';
							break;
                        case 'file':
							$data['cpEventListLoopLink_File'] = $cpEventListLoop_item['cpEventListLoopLink_File'];
							$data['cpEventListLoopLink_Page'] = '0';
							$data['cpEventListLoopLink_URL'] = '';
							$data['cpEventListLoopLink_Relative_URL'] = '';
							$data['cpEventListLoopLink_Image'] = '0';
							break;
                        case 'url':
							$data['cpEventListLoopLink_URL'] = $cpEventListLoop_item['cpEventListLoopLink_URL'];
							$data['cpEventListLoopLink_Page'] = '0';
							$data['cpEventListLoopLink_File'] = '0';
							$data['cpEventListLoopLink_Relative_URL'] = '';
							$data['cpEventListLoopLink_Image'] = '0';
							break;
                        case 'relative_url':
							$data['cpEventListLoopLink_Relative_URL'] = $cpEventListLoop_item['cpEventListLoopLink_Relative_URL'];
							$data['cpEventListLoopLink_Page'] = '0';
							$data['cpEventListLoopLink_File'] = '0';
							$data['cpEventListLoopLink_URL'] = '';
							$data['cpEventListLoopLink_Image'] = '0';
							break;
                        case 'image':
							$data['cpEventListLoopLink_Image'] = $cpEventListLoop_item['cpEventListLoopLink_Image'];
							$data['cpEventListLoopLink_Page'] = '0';
							$data['cpEventListLoopLink_File'] = '0';
							$data['cpEventListLoopLink_URL'] = '';
							$data['cpEventListLoopLink_Relative_URL'] = '';
							break;
                        default:
							$data['cpEventListLoopLink'] = '';
							$data['cpEventListLoopLink_Page'] = '0';
							$data['cpEventListLoopLink_File'] = '0';
							$data['cpEventListLoopLink_URL'] = '';
							$data['cpEventListLoopLink_Relative_URL'] = '';
							$data['cpEventListLoopLink_Image'] = '0';
							break;	
					}
				}
				else {
					$data['cpEventListLoopLink'] = '';
					$data['cpEventListLoopLink_Title'] = '';
					$data['cpEventListLoopLink_Page'] = '0';
					$data['cpEventListLoopLink_File'] = '0';
					$data['cpEventListLoopLink_URL'] = '';
					$data['cpEventListLoopLink_Relative_URL'] = '';
					$data['cpEventListLoopLink_Image'] = '0';
				}
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btComponentEventListCpEventListLoopEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btComponentEventListCpEventListLoopEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btComponentEventListCpEventListLoopEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("cpEventListTitle", $this->btFieldsRequired) && trim($args["cpEventListTitle"]) == "") {
            $e->add(t("The %s field is required.", t("タイトル")));
        }
        if (in_array("cpEventListDesc", $this->btFieldsRequired) && (trim($args["cpEventListDesc"]) == "")) {
            $e->add(t("The %s field is required.", t("概要文")));
        }
        $cpEventListLoopEntriesMin = 0;
        $cpEventListLoopEntriesMax = 0;
        $cpEventListLoopEntriesErrors = 0;
        $cpEventListLoop = array();
        if (isset($args['cpEventListLoop']) && is_array($args['cpEventListLoop']) && !empty($args['cpEventListLoop'])) {
            if ($cpEventListLoopEntriesMin >= 1 && count($args['cpEventListLoop']) < $cpEventListLoopEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("リスト"), $cpEventListLoopEntriesMin, count($args['cpEventListLoop'])));
                $cpEventListLoopEntriesErrors++;
            }
            if ($cpEventListLoopEntriesMax >= 1 && count($args['cpEventListLoop']) > $cpEventListLoopEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("リスト"), $cpEventListLoopEntriesMax, count($args['cpEventListLoop'])));
                $cpEventListLoopEntriesErrors++;
            }
            if ($cpEventListLoopEntriesErrors == 0) {
                foreach ($args['cpEventListLoop'] as $cpEventListLoop_k => $cpEventListLoop_v) {
                    if (is_array($cpEventListLoop_v)) {
                        if (in_array("cpEventListLoopTitle", $this->btFieldsRequired['cpEventListLoop']) && (!isset($cpEventListLoop_v['cpEventListLoopTitle']) || trim($cpEventListLoop_v['cpEventListLoopTitle']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("タイトル"), t("リスト"), $cpEventListLoop_k));
                        }
                        if (in_array("cpEventListLoopDesc", $this->btFieldsRequired['cpEventListLoop']) && (!isset($cpEventListLoop_v['cpEventListLoopDesc']) || trim($cpEventListLoop_v['cpEventListLoopDesc']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("概要文"), t("リスト"), $cpEventListLoop_k));
                        }
                        if ((in_array("cpEventListLoopLink", $this->btFieldsRequired['cpEventListLoop']) && (!isset($cpEventListLoop_v['cpEventListLoopLink']) || trim($cpEventListLoop_v['cpEventListLoopLink']) == "")) || (isset($cpEventListLoop_v['cpEventListLoopLink']) && trim($cpEventListLoop_v['cpEventListLoopLink']) != "" && !array_key_exists($cpEventListLoop_v['cpEventListLoopLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
))))) {
							$e->add(t("The %s field has an invalid value.", t("リンク")));
						} elseif (array_key_exists($cpEventListLoop_v['cpEventListLoopLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
)))) {
							switch ($cpEventListLoop_v['cpEventListLoopLink']) {
								case 'page':
									if (!isset($cpEventListLoop_v['cpEventListLoopLink_Page']) || trim($cpEventListLoop_v['cpEventListLoopLink_Page']) == "" || $cpEventListLoop_v['cpEventListLoopLink_Page'] == "0" || (($page = Page::getByID($cpEventListLoop_v['cpEventListLoopLink_Page'])) && $page->error !== false)) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Page"), t("リンク"), t("リスト"), $cpEventListLoop_k));
									}
									break;
				                case 'file':
									if (!isset($cpEventListLoop_v['cpEventListLoopLink_File']) || trim($cpEventListLoop_v['cpEventListLoopLink_File']) == "" || !is_object(File::getByID($cpEventListLoop_v['cpEventListLoopLink_File']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("File"), t("リンク"), t("リスト"), $cpEventListLoop_k));
									}
									break;
				                case 'url':
									if (!isset($cpEventListLoop_v['cpEventListLoopLink_URL']) || trim($cpEventListLoop_v['cpEventListLoopLink_URL']) == "" || !filter_var($cpEventListLoop_v['cpEventListLoopLink_URL'], FILTER_VALIDATE_URL)) {
										$e->add(t("The %s field for '%s' does not have a valid URL (%s, row #%s).", t("URL"), t("リンク"), t("リスト"), $cpEventListLoop_k));
									}
									break;
				                case 'relative_url':
									if (!isset($cpEventListLoop_v['cpEventListLoopLink_Relative_URL']) || trim($cpEventListLoop_v['cpEventListLoopLink_Relative_URL']) == "") {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Relative URL"), t("リンク"), t("リスト"), $cpEventListLoop_k));
									}
									break;
				                case 'image':
									if (!isset($cpEventListLoop_v['cpEventListLoopLink_Image']) || trim($cpEventListLoop_v['cpEventListLoopLink_Image']) == "" || !is_object(File::getByID($cpEventListLoop_v['cpEventListLoopLink_Image']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Image"), t("リンク"), t("リスト"), $cpEventListLoop_k));
									}
									break;	
							}
						}
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('リスト'), $cpEventListLoop_k));
                    }
                }
            }
        } else {
            if ($cpEventListLoopEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("リスト"), $cpEventListLoopEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-component_event_list', 'blocks/component_event_list/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-component_event_list');
        $this->edit();
    }

        protected function getSmartLinkTypeOptions($include = array(), $checkNone = false)
	{
		$options = array(
			''             => sprintf("-- %s--", t("None")),
			'page'         => t("Page"),
			'url'          => t("External URL"),
			'relative_url' => t("Relative URL"),
			'file'         => t("File"),
			'image'        => t("Image")
		);
		if ($checkNone) {
            $include = array_merge(array(''), $include);
        }
		$return = array();
		foreach($include as $v){
		    if(isset($options[$v])){
		        $return[$v] = $options[$v];
		    }
		}
		return $return;
	}
}