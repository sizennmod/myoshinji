<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php $tabs = array(
    array('form-basics-' . $identifier_getString, t('Basics'), true),
    array('form-cpEventListLoop_items-' . $identifier_getString, t('リスト'))
);
echo Core::make('helper/concrete/ui')->tabs($tabs); ?>

<div class="ccm-tab-content" id="ccm-tab-content-form-basics-<?php echo $identifier_getString; ?>">
    <div class="form-group">
    <?php echo $form->label('cpEventListTitle', t("タイトル")); ?>
    <?php echo isset($btFieldsRequired) && in_array('cpEventListTitle', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->textarea($view->field('cpEventListTitle'), $cpEventListTitle, array (
  'rows' => 5,
)); ?>
</div><div class="form-group">
    <?php echo $form->label('cpEventListDesc', t("概要文")); ?>
    <?php echo isset($btFieldsRequired) && in_array('cpEventListDesc', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo Core::make('editor')->outputBlockEditModeEditor($view->field('cpEventListDesc'), $cpEventListDesc); ?>
</div>
</div>

<div class="ccm-tab-content" id="ccm-tab-content-form-cpEventListLoop_items-<?php echo $identifier_getString; ?>">
    <script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
<?php $repeatable_container_id = 'btComponentEventList-cpEventListLoop-container-' . $identifier_getString; ?>
    <div id="<?php echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $cpEventListLoop_items,
                        'order' => array_keys($cpEventListLoop_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php echo t('リスト') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopTitle]" class="control-label"><?php echo t("タイトル"); ?></label>
    <?php echo isset($btFieldsRequired['cpEventListLoop']) && in_array('cpEventListLoopTitle', $btFieldsRequired['cpEventListLoop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopTitle]" id="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopTitle]" class="form-control" rows="5">{{ cpEventListLoopTitle }}</textarea>
</div>            <div class="form-group">
    <label for="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopDesc]" class="control-label"><?php echo t("概要文"); ?></label>
    <?php echo isset($btFieldsRequired['cpEventListLoop']) && in_array('cpEventListLoopDesc', $btFieldsRequired['cpEventListLoop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopDesc]" id="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopDesc]" class="form-control" rows="5">{{ cpEventListLoopDesc }}</textarea>
</div>            <?php $cpEventListLoopLink_ContainerID = 'btComponentEventList-cpEventListLoopLink-container-' . $identifier_getString; ?>
<div class="ft-smart-link" id="<?php echo $cpEventListLoopLink_ContainerID; ?>">
	<div class="form-group">
		<label for="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink]" class="control-label"><?php echo t("リンク"); ?></label>
	    <?php echo isset($btFieldsRequired['cpEventListLoop']) && in_array('cpEventListLoopLink', $btFieldsRequired['cpEventListLoop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
	    <?php $cpEventListLoopCpEventListLoopLink_options = $cpEventListLoopLink_Options; ?>
                    <select name="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink]" id="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink]" class="form-control ft-smart-link-type">{{#select cpEventListLoopLink}}<?php foreach ($cpEventListLoopCpEventListLoopLink_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
	</div>
	
	<div class="form-group">
		<div class="ft-smart-link-options hidden" style="padding-left: 10px;">
			<div class="form-group">
				<label for="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_Title]" class="control-label"><?php echo t("Title"); ?></label>
			    <input name="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_Title]" id="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_Title]" class="form-control" type="text" value="{{ cpEventListLoopLink_Title }}" />		
			</div>
			
			<div class="form-group hidden" data-link-type="page">
			<label for="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_Page]" class="control-label"><?php echo t("Page"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-page-selector="{{token}}" data-input-name="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_Page]" data-cID="{{cpEventListLoopLink_Page}}"></div>
		</div>

		<div class="form-group hidden" data-link-type="url">
			<label for="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_URL]" id="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_URL]" class="form-control" type="text" value="{{ cpEventListLoopLink_URL }}" />
		</div>

		<div class="form-group hidden" data-link-type="relative_url">
			<label for="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_Relative_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_Relative_URL]" id="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_Relative_URL]" class="form-control" type="text" value="{{ cpEventListLoopLink_Relative_URL }}" />
		</div>

		<div class="form-group hidden" data-link-type="file">
		    <label for="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_File]" class="control-label"><?php echo t("File"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_File]" class="ccm-file-selector" data-file-selector-f-id="{{ cpEventListLoopLink_File }}"></div>	
		</div>

		<div class="form-group hidden" data-link-type="image">
			<label for="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_Image]" class="control-label"><?php echo t("Image"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('cpEventListLoop'); ?>[{{id}}][cpEventListLoopLink_Image]" class="ccm-file-selector" data-file-selector-f-id="{{ cpEventListLoopLink_Image }}"></div>
		</div>
		</div>
	</div>
</div>
</div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btComponentEventList.cpEventListLoop.edit.open', {id: '<?php echo $repeatable_container_id; ?>'});
    $.each($('#<?php echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>
</div>