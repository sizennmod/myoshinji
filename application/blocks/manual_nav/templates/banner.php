<?php
defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage();
?>

<?php
if (count($rows) > 0) {
    $rows[0]['class'] = "nav-first";
    foreach ($rows as &$rowp) {
        if ($rowp['internalLinkCID'] === $c->getCollectionID()) {
            $rowp['class'] .= " nav-selected";
        }
    }
    ?>
    <?php foreach ($rows as $row) { ?>
        <?php
        // create title
        $title = null;
        if ($row['title'] != null) {
            $title = $row['title'];
        } elseif ($row['collectionName'] != null) {
            $title = $row['collectionName'];
        } else {
            $title = t('(Untitled)');
        }
        
        $tag = "";
        if ($displayImage != 0) {
            if (is_object($row['image'])) {
               $tag = '<img src="' . $row['image']->getURL() . '" class="e-img">';
            }
        }
        ?>

        <li class="<?php echo $row['class'] ?> c-subBnr_item">
            <a href="<?php echo $row['linkURL'] ?>" class="e-box" <?php echo $row['openInNewWindow']  ?  'target="_blank"' : '' ?>>
                <?php echo $tag ?>
            </a>
        </li>
    <?php } ?>
<?php } else { ?>
    <div class="ccm-manual-nav-placeholder">
        <p><?php echo t('No nav Entered.'); ?></p>
    </div>
<?php } ?>
