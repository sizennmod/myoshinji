<?php namespace Application\Block\ComponentTextTab;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Database;
use Concrete\Core\Editor\LinkAbstractor;
use File;
use Page;
use Permissions;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('cpTextColumns' => array());
    protected $btExportFileColumns = array('cpTextImg');
    protected $btExportPageColumns = array();
    protected $btExportTables = array('btComponentTextTab', 'btComponentTextTabCpTextColumnsEntries');
    protected $btTable = 'btComponentTextTab';
    protected $btInterfaceWidth = 800;
    protected $btInterfaceHeight = 600;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $btDefaultSet = 'common_component';
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("タブコンテンツ");
    }

    public function getSearchableContent()
    {
        $content = array();
        $db = Database::connection();
        $cpTextColumns_items = $db->fetchAll('SELECT * FROM btComponentTextTabCpTextColumnsEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpTextColumns_items as $cpTextColumns_item_k => $cpTextColumns_item_v) {
            if (isset($cpTextColumns_item_v["cpTextTabTitle"]) && trim($cpTextColumns_item_v["cpTextTabTitle"]) != "") {
                $content[] = $cpTextColumns_item_v["cpTextTabTitle"];
            }
            if (isset($cpTextColumns_item_v["cpTextTile"]) && trim($cpTextColumns_item_v["cpTextTile"]) != "") {
                $content[] = $cpTextColumns_item_v["cpTextTile"];
            }
            if (isset($cpTextColumns_item_v["cpTextWyg"]) && trim($cpTextColumns_item_v["cpTextWyg"]) != "") {
                $content[] = $cpTextColumns_item_v["cpTextWyg"];
            }
            if (isset($cpTextColumns_item_v["cpTextImagesAlt"]) && trim($cpTextColumns_item_v["cpTextImagesAlt"]) != "") {
                $content[] = $cpTextColumns_item_v["cpTextImagesAlt"];
            }
            if (isset($cpTextColumns_item_v["cpTextImgCapTitle"]) && trim($cpTextColumns_item_v["cpTextImgCapTitle"]) != "") {
                $content[] = $cpTextColumns_item_v["cpTextImgCapTitle"];
            }
            if (isset($cpTextColumns_item_v["cpTextImgCapText"]) && trim($cpTextColumns_item_v["cpTextImgCapText"]) != "") {
                $content[] = $cpTextColumns_item_v["cpTextImgCapText"];
            }
        }
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $cpTextColumns = array();
        $cpTextColumns_items = $db->fetchAll('SELECT * FROM btComponentTextTabCpTextColumnsEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpTextColumns_items as $cpTextColumns_item_k => &$cpTextColumns_item_v) {
            $cpTextColumns_item_v["cpTextWyg"] = isset($cpTextColumns_item_v["cpTextWyg"]) ? LinkAbstractor::translateFrom($cpTextColumns_item_v["cpTextWyg"]) : null;
            if (isset($cpTextColumns_item_v['cpTextImg']) && trim($cpTextColumns_item_v['cpTextImg']) != "" && ($f = File::getByID($cpTextColumns_item_v['cpTextImg'])) && is_object($f)) {
                $cpTextColumns_item_v['cpTextImg'] = $f;
            } else {
                $cpTextColumns_item_v['cpTextImg'] = false;
            }
            $cpTextColumns_item_v["cpTextImgLink_Object"] = null;
			$cpTextColumns_item_v["cpTextImgLink_Title"] = trim($cpTextColumns_item_v["cpTextImgLink_Title"]);
			if (isset($cpTextColumns_item_v["cpTextImgLink"]) && trim($cpTextColumns_item_v["cpTextImgLink"]) != '') {
				switch ($cpTextColumns_item_v["cpTextImgLink"]) {
					case 'page':
						if ($cpTextColumns_item_v["cpTextImgLink_Page"] > 0 && ($cpTextColumns_item_v["cpTextImgLink_Page_c"] = Page::getByID($cpTextColumns_item_v["cpTextImgLink_Page"])) && !$cpTextColumns_item_v["cpTextImgLink_Page_c"]->error && !$cpTextColumns_item_v["cpTextImgLink_Page_c"]->isInTrash()) {
							$cpTextColumns_item_v["cpTextImgLink_Object"] = $cpTextColumns_item_v["cpTextImgLink_Page_c"];
							$cpTextColumns_item_v["cpTextImgLink_URL"] = $cpTextColumns_item_v["cpTextImgLink_Page_c"]->getCollectionLink();
							if ($cpTextColumns_item_v["cpTextImgLink_Title"] == '') {
								$cpTextColumns_item_v["cpTextImgLink_Title"] = $cpTextColumns_item_v["cpTextImgLink_Page_c"]->getCollectionName();
							}
						}
						break;
				    case 'url':
						if ($cpTextColumns_item_v["cpTextImgLink_Title"] == '') {
							$cpTextColumns_item_v["cpTextImgLink_Title"] = $cpTextColumns_item_v["cpTextImgLink_URL"];
						}
						break;
				}
			}
            $cpTextColumns_item_v["cpTextImgLinkBtn_Object"] = null;
			$cpTextColumns_item_v["cpTextImgLinkBtn_Title"] = trim($cpTextColumns_item_v["cpTextImgLinkBtn_Title"]);
			if (isset($cpTextColumns_item_v["cpTextImgLinkBtn"]) && trim($cpTextColumns_item_v["cpTextImgLinkBtn"]) != '') {
				switch ($cpTextColumns_item_v["cpTextImgLinkBtn"]) {
					case 'page':
						if ($cpTextColumns_item_v["cpTextImgLinkBtn_Page"] > 0 && ($cpTextColumns_item_v["cpTextImgLinkBtn_Page_c"] = Page::getByID($cpTextColumns_item_v["cpTextImgLinkBtn_Page"])) && !$cpTextColumns_item_v["cpTextImgLinkBtn_Page_c"]->error && !$cpTextColumns_item_v["cpTextImgLinkBtn_Page_c"]->isInTrash()) {
							$cpTextColumns_item_v["cpTextImgLinkBtn_Object"] = $cpTextColumns_item_v["cpTextImgLinkBtn_Page_c"];
							$cpTextColumns_item_v["cpTextImgLinkBtn_URL"] = $cpTextColumns_item_v["cpTextImgLinkBtn_Page_c"]->getCollectionLink();
							if ($cpTextColumns_item_v["cpTextImgLinkBtn_Title"] == '') {
								$cpTextColumns_item_v["cpTextImgLinkBtn_Title"] = $cpTextColumns_item_v["cpTextImgLinkBtn_Page_c"]->getCollectionName();
							}
						}
						break;
				    case 'file':
						$cpTextColumns_item_v["cpTextImgLinkBtn_File_id"] = (int)$cpTextColumns_item_v["cpTextImgLinkBtn_File"];
						if ($cpTextColumns_item_v["cpTextImgLinkBtn_File_id"] > 0 && ($cpTextColumns_item_v["cpTextImgLinkBtn_File_object"] = File::getByID($cpTextColumns_item_v["cpTextImgLinkBtn_File_id"])) && is_object($cpTextColumns_item_v["cpTextImgLinkBtn_File_object"])) {
							$fp = new Permissions($cpTextColumns_item_v["cpTextImgLinkBtn_File_object"]);
							if ($fp->canViewFile()) {
								$cpTextColumns_item_v["cpTextImgLinkBtn_Object"] = $cpTextColumns_item_v["cpTextImgLinkBtn_File_object"];
								$cpTextColumns_item_v["cpTextImgLinkBtn_URL"] = $cpTextColumns_item_v["cpTextImgLinkBtn_File_object"]->getRelativePath();
								if ($cpTextColumns_item_v["cpTextImgLinkBtn_Title"] == '') {
									$cpTextColumns_item_v["cpTextImgLinkBtn_Title"] = $cpTextColumns_item_v["cpTextImgLinkBtn_File_object"]->getTitle();
								}
							}
						}
						break;
				    case 'url':
						if ($cpTextColumns_item_v["cpTextImgLinkBtn_Title"] == '') {
							$cpTextColumns_item_v["cpTextImgLinkBtn_Title"] = $cpTextColumns_item_v["cpTextImgLinkBtn_URL"];
						}
						break;
				    case 'relative_url':
						if ($cpTextColumns_item_v["cpTextImgLinkBtn_Title"] == '') {
							$cpTextColumns_item_v["cpTextImgLinkBtn_Title"] = $cpTextColumns_item_v["cpTextImgLinkBtn_Relative_URL"];
						}
						$cpTextColumns_item_v["cpTextImgLinkBtn_URL"] = $cpTextColumns_item_v["cpTextImgLinkBtn_Relative_URL"];
						break;
				    case 'image':
						if ($cpTextColumns_item_v["cpTextImgLinkBtn_Image"] > 0 && ($cpTextColumns_item_v["cpTextImgLinkBtn_Image_object"] = File::getByID($cpTextColumns_item_v["cpTextImgLinkBtn_Image"])) && is_object($cpTextColumns_item_v["cpTextImgLinkBtn_Image_object"])) {
							$cpTextColumns_item_v["cpTextImgLinkBtn_URL"] = $cpTextColumns_item_v["cpTextImgLinkBtn_Image_object"]->getURL();
							$cpTextColumns_item_v["cpTextImgLinkBtn_Object"] = $cpTextColumns_item_v["cpTextImgLinkBtn_Image_object"];
							if ($cpTextColumns_item_v["cpTextImgLinkBtn_Title"] == '') {
								$cpTextColumns_item_v["cpTextImgLinkBtn_Title"] = $cpTextColumns_item_v["cpTextImgLinkBtn_Image_object"]->getTitle();
							}
						}
						break;
				}
			}
        }
        $this->set('cpTextColumns_items', $cpTextColumns_items);
        $this->set('cpTextColumns', $cpTextColumns);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btComponentTextTabCpTextColumnsEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $cpTextColumns_items = $db->fetchAll('SELECT * FROM btComponentTextTabCpTextColumnsEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpTextColumns_items as $cpTextColumns_item) {
            unset($cpTextColumns_item['id']);
            $cpTextColumns_item['bID'] = $newBID;
            $db->insert('btComponentTextTabCpTextColumnsEntries', $cpTextColumns_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $cpTextColumns = $this->get('cpTextColumns');
        $this->set('cpTextColumns_items', array());
        $this->set('cpTextColumns', $cpTextColumns);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        $cpTextColumns = $this->get('cpTextColumns');
        $cpTextColumns_items = $db->fetchAll('SELECT * FROM btComponentTextTabCpTextColumnsEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        
        foreach ($cpTextColumns_items as &$cpTextColumns_item) {
            $cpTextColumns_item['cpTextWyg'] = isset($cpTextColumns_item['cpTextWyg']) ? LinkAbstractor::translateFromEditMode($cpTextColumns_item['cpTextWyg']) : null;
        }
        foreach ($cpTextColumns_items as &$cpTextColumns_item) {
            if (!File::getByID($cpTextColumns_item['cpTextImg'])) {
                unset($cpTextColumns_item['cpTextImg']);
            }
        }
        $this->set('cpTextColumns', $cpTextColumns);
        $this->set('cpTextColumns_items', $cpTextColumns_items);
    }

    protected function addEdit()
    {
        $cpTextColumns = array();
        $this->set("cpTextImgLink_Options", $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'url',
), true));
        $this->set("cpTextImgLinkBtn_Options", $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
), true));
        $this->set('cpTextColumns', $cpTextColumns);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/component_text_tab/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/component_text_tab/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/component_text_tab/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->requireAsset('redactor');
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $db = Database::connection();
        $rows = $db->fetchAll('SELECT * FROM btComponentTextTabCpTextColumnsEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $cpTextColumns_items = isset($args['cpTextColumns']) && is_array($args['cpTextColumns']) ? $args['cpTextColumns'] : array();
        $queries = array();
        if (!empty($cpTextColumns_items)) {
            $i = 0;
            foreach ($cpTextColumns_items as $cpTextColumns_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($cpTextColumns_item['cpTextTabTitle']) && trim($cpTextColumns_item['cpTextTabTitle']) != '') {
                    $data['cpTextTabTitle'] = trim($cpTextColumns_item['cpTextTabTitle']);
                } else {
                    $data['cpTextTabTitle'] = null;
                }
                if (isset($cpTextColumns_item['cpTextTile']) && trim($cpTextColumns_item['cpTextTile']) != '') {
                    $data['cpTextTile'] = trim($cpTextColumns_item['cpTextTile']);
                } else {
                    $data['cpTextTile'] = null;
                }
                $data['cpTextWyg'] = isset($cpTextColumns_item['cpTextWyg']) ? LinkAbstractor::translateTo($cpTextColumns_item['cpTextWyg']) : null;
                if (isset($cpTextColumns_item['cpTextImg']) && trim($cpTextColumns_item['cpTextImg']) != '') {
                    $data['cpTextImg'] = trim($cpTextColumns_item['cpTextImg']);
                } else {
                    $data['cpTextImg'] = null;
                }
                if (isset($cpTextColumns_item['cpTextImgLink']) && trim($cpTextColumns_item['cpTextImgLink']) != '') {
					$data['cpTextImgLink_Title'] = $cpTextColumns_item['cpTextImgLink_Title'];
					$data['cpTextImgLink'] = $cpTextColumns_item['cpTextImgLink'];
					switch ($cpTextColumns_item['cpTextImgLink']) {
						case 'page':
							$data['cpTextImgLink_Page'] = $cpTextColumns_item['cpTextImgLink_Page'];
							$data['cpTextImgLink_URL'] = '';
							break;
                        case 'url':
							$data['cpTextImgLink_URL'] = $cpTextColumns_item['cpTextImgLink_URL'];
							$data['cpTextImgLink_Page'] = '0';
							break;
                        default:
							$data['cpTextImgLink'] = '';
							$data['cpTextImgLink_Page'] = '0';
							$data['cpTextImgLink_URL'] = '';
							break;	
					}
				}
				else {
					$data['cpTextImgLink'] = '';
					$data['cpTextImgLink_Title'] = '';
					$data['cpTextImgLink_Page'] = '0';
					$data['cpTextImgLink_URL'] = '';
				}
                if (isset($cpTextColumns_item['cpTextImagesAlt']) && trim($cpTextColumns_item['cpTextImagesAlt']) != '') {
                    $data['cpTextImagesAlt'] = trim($cpTextColumns_item['cpTextImagesAlt']);
                } else {
                    $data['cpTextImagesAlt'] = null;
                }
                if (isset($cpTextColumns_item['cpTextImgCapTitle']) && trim($cpTextColumns_item['cpTextImgCapTitle']) != '') {
                    $data['cpTextImgCapTitle'] = trim($cpTextColumns_item['cpTextImgCapTitle']);
                } else {
                    $data['cpTextImgCapTitle'] = null;
                }
                if (isset($cpTextColumns_item['cpTextImgCapText']) && trim($cpTextColumns_item['cpTextImgCapText']) != '') {
                    $data['cpTextImgCapText'] = trim($cpTextColumns_item['cpTextImgCapText']);
                } else {
                    $data['cpTextImgCapText'] = null;
                }
                if (isset($cpTextColumns_item['cpTextImgLinkBtn']) && trim($cpTextColumns_item['cpTextImgLinkBtn']) != '') {
					$data['cpTextImgLinkBtn_Title'] = $cpTextColumns_item['cpTextImgLinkBtn_Title'];
					$data['cpTextImgLinkBtn'] = $cpTextColumns_item['cpTextImgLinkBtn'];
					switch ($cpTextColumns_item['cpTextImgLinkBtn']) {
						case 'page':
							$data['cpTextImgLinkBtn_Page'] = $cpTextColumns_item['cpTextImgLinkBtn_Page'];
							$data['cpTextImgLinkBtn_File'] = '0';
							$data['cpTextImgLinkBtn_URL'] = '';
							$data['cpTextImgLinkBtn_Relative_URL'] = '';
							$data['cpTextImgLinkBtn_Image'] = '0';
							break;
                        case 'file':
							$data['cpTextImgLinkBtn_File'] = $cpTextColumns_item['cpTextImgLinkBtn_File'];
							$data['cpTextImgLinkBtn_Page'] = '0';
							$data['cpTextImgLinkBtn_URL'] = '';
							$data['cpTextImgLinkBtn_Relative_URL'] = '';
							$data['cpTextImgLinkBtn_Image'] = '0';
							break;
                        case 'url':
							$data['cpTextImgLinkBtn_URL'] = $cpTextColumns_item['cpTextImgLinkBtn_URL'];
							$data['cpTextImgLinkBtn_Page'] = '0';
							$data['cpTextImgLinkBtn_File'] = '0';
							$data['cpTextImgLinkBtn_Relative_URL'] = '';
							$data['cpTextImgLinkBtn_Image'] = '0';
							break;
                        case 'relative_url':
							$data['cpTextImgLinkBtn_Relative_URL'] = $cpTextColumns_item['cpTextImgLinkBtn_Relative_URL'];
							$data['cpTextImgLinkBtn_Page'] = '0';
							$data['cpTextImgLinkBtn_File'] = '0';
							$data['cpTextImgLinkBtn_URL'] = '';
							$data['cpTextImgLinkBtn_Image'] = '0';
							break;
                        case 'image':
							$data['cpTextImgLinkBtn_Image'] = $cpTextColumns_item['cpTextImgLinkBtn_Image'];
							$data['cpTextImgLinkBtn_Page'] = '0';
							$data['cpTextImgLinkBtn_File'] = '0';
							$data['cpTextImgLinkBtn_URL'] = '';
							$data['cpTextImgLinkBtn_Relative_URL'] = '';
							break;
                        default:
							$data['cpTextImgLinkBtn'] = '';
							$data['cpTextImgLinkBtn_Page'] = '0';
							$data['cpTextImgLinkBtn_File'] = '0';
							$data['cpTextImgLinkBtn_URL'] = '';
							$data['cpTextImgLinkBtn_Relative_URL'] = '';
							$data['cpTextImgLinkBtn_Image'] = '0';
							break;	
					}
				}
				else {
					$data['cpTextImgLinkBtn'] = '';
					$data['cpTextImgLinkBtn_Title'] = '';
					$data['cpTextImgLinkBtn_Page'] = '0';
					$data['cpTextImgLinkBtn_File'] = '0';
					$data['cpTextImgLinkBtn_URL'] = '';
					$data['cpTextImgLinkBtn_Relative_URL'] = '';
					$data['cpTextImgLinkBtn_Image'] = '0';
				}
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btComponentTextTabCpTextColumnsEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btComponentTextTabCpTextColumnsEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btComponentTextTabCpTextColumnsEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        $cpTextColumnsEntriesMin = 0;
        $cpTextColumnsEntriesMax = 0;
        $cpTextColumnsEntriesErrors = 0;
        $cpTextColumns = array();
        if (isset($args['cpTextColumns']) && is_array($args['cpTextColumns']) && !empty($args['cpTextColumns'])) {
            if ($cpTextColumnsEntriesMin >= 1 && count($args['cpTextColumns']) < $cpTextColumnsEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("記事データ"), $cpTextColumnsEntriesMin, count($args['cpTextColumns'])));
                $cpTextColumnsEntriesErrors++;
            }
            if ($cpTextColumnsEntriesMax >= 1 && count($args['cpTextColumns']) > $cpTextColumnsEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("記事データ"), $cpTextColumnsEntriesMax, count($args['cpTextColumns'])));
                $cpTextColumnsEntriesErrors++;
            }
            if ($cpTextColumnsEntriesErrors == 0) {
                foreach ($args['cpTextColumns'] as $cpTextColumns_k => $cpTextColumns_v) {
                    if (is_array($cpTextColumns_v)) {
                        if (in_array("cpTextTabTitle", $this->btFieldsRequired['cpTextColumns']) && (!isset($cpTextColumns_v['cpTextTabTitle']) || trim($cpTextColumns_v['cpTextTabTitle']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("タブタイトル"), t("記事データ"), $cpTextColumns_k));
                        }
                        if (in_array("cpTextTile", $this->btFieldsRequired['cpTextColumns']) && (!isset($cpTextColumns_v['cpTextTile']) || trim($cpTextColumns_v['cpTextTile']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("タイトル"), t("記事データ"), $cpTextColumns_k));
                        }
                        if (in_array("cpTextWyg", $this->btFieldsRequired['cpTextColumns']) && (!isset($cpTextColumns_v['cpTextWyg']) || trim($cpTextColumns_v['cpTextWyg']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("本文"), t("記事データ"), $cpTextColumns_k));
                        }
                        if (in_array("cpTextImg", $this->btFieldsRequired['cpTextColumns']) && (!isset($cpTextColumns_v['cpTextImg']) || trim($cpTextColumns_v['cpTextImg']) == "" || !is_object(File::getByID($cpTextColumns_v['cpTextImg'])))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("画像"), t("記事データ"), $cpTextColumns_k));
                        }
                        if ((in_array("cpTextImgLink", $this->btFieldsRequired['cpTextColumns']) && (!isset($cpTextColumns_v['cpTextImgLink']) || trim($cpTextColumns_v['cpTextImgLink']) == "")) || (isset($cpTextColumns_v['cpTextImgLink']) && trim($cpTextColumns_v['cpTextImgLink']) != "" && !array_key_exists($cpTextColumns_v['cpTextImgLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'url',
))))) {
							$e->add(t("The %s field has an invalid value.", t("画像リンク")));
						} elseif (array_key_exists($cpTextColumns_v['cpTextImgLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'url',
)))) {
							switch ($cpTextColumns_v['cpTextImgLink']) {
								case 'page':
									if (!isset($cpTextColumns_v['cpTextImgLink_Page']) || trim($cpTextColumns_v['cpTextImgLink_Page']) == "" || $cpTextColumns_v['cpTextImgLink_Page'] == "0" || (($page = Page::getByID($cpTextColumns_v['cpTextImgLink_Page'])) && $page->error !== false)) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Page"), t("画像リンク"), t("記事データ"), $cpTextColumns_k));
									}
									break;
				                case 'url':
									if (!isset($cpTextColumns_v['cpTextImgLink_URL']) || trim($cpTextColumns_v['cpTextImgLink_URL']) == "" || !filter_var($cpTextColumns_v['cpTextImgLink_URL'], FILTER_VALIDATE_URL)) {
										$e->add(t("The %s field for '%s' does not have a valid URL (%s, row #%s).", t("URL"), t("画像リンク"), t("記事データ"), $cpTextColumns_k));
									}
									break;	
							}
						}
                        if (in_array("cpTextImagesAlt", $this->btFieldsRequired['cpTextColumns']) && (!isset($cpTextColumns_v['cpTextImagesAlt']) || trim($cpTextColumns_v['cpTextImagesAlt']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("画像alt"), t("記事データ"), $cpTextColumns_k));
                        }
                        if (in_array("cpTextImgCapTitle", $this->btFieldsRequired['cpTextColumns']) && (!isset($cpTextColumns_v['cpTextImgCapTitle']) || trim($cpTextColumns_v['cpTextImgCapTitle']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("キャプションタイトル"), t("記事データ"), $cpTextColumns_k));
                        }
                        if (in_array("cpTextImgCapText", $this->btFieldsRequired['cpTextColumns']) && (!isset($cpTextColumns_v['cpTextImgCapText']) || trim($cpTextColumns_v['cpTextImgCapText']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("キャプションテキスト"), t("記事データ"), $cpTextColumns_k));
                        }
                        if ((in_array("cpTextImgLinkBtn", $this->btFieldsRequired['cpTextColumns']) && (!isset($cpTextColumns_v['cpTextImgLinkBtn']) || trim($cpTextColumns_v['cpTextImgLinkBtn']) == "")) || (isset($cpTextColumns_v['cpTextImgLinkBtn']) && trim($cpTextColumns_v['cpTextImgLinkBtn']) != "" && !array_key_exists($cpTextColumns_v['cpTextImgLinkBtn'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
))))) {
							$e->add(t("The %s field has an invalid value.", t("リンクボタン")));
						} elseif (array_key_exists($cpTextColumns_v['cpTextImgLinkBtn'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
)))) {
							switch ($cpTextColumns_v['cpTextImgLinkBtn']) {
								case 'page':
									if (!isset($cpTextColumns_v['cpTextImgLinkBtn_Page']) || trim($cpTextColumns_v['cpTextImgLinkBtn_Page']) == "" || $cpTextColumns_v['cpTextImgLinkBtn_Page'] == "0" || (($page = Page::getByID($cpTextColumns_v['cpTextImgLinkBtn_Page'])) && $page->error !== false)) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Page"), t("リンクボタン"), t("記事データ"), $cpTextColumns_k));
									}
									break;
				                case 'file':
									if (!isset($cpTextColumns_v['cpTextImgLinkBtn_File']) || trim($cpTextColumns_v['cpTextImgLinkBtn_File']) == "" || !is_object(File::getByID($cpTextColumns_v['cpTextImgLinkBtn_File']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("File"), t("リンクボタン"), t("記事データ"), $cpTextColumns_k));
									}
									break;
				                case 'url':
									if (!isset($cpTextColumns_v['cpTextImgLinkBtn_URL']) || trim($cpTextColumns_v['cpTextImgLinkBtn_URL']) == "" || !filter_var($cpTextColumns_v['cpTextImgLinkBtn_URL'], FILTER_VALIDATE_URL)) {
										$e->add(t("The %s field for '%s' does not have a valid URL (%s, row #%s).", t("URL"), t("リンクボタン"), t("記事データ"), $cpTextColumns_k));
									}
									break;
				                case 'relative_url':
									if (!isset($cpTextColumns_v['cpTextImgLinkBtn_Relative_URL']) || trim($cpTextColumns_v['cpTextImgLinkBtn_Relative_URL']) == "") {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Relative URL"), t("リンクボタン"), t("記事データ"), $cpTextColumns_k));
									}
									break;
				                case 'image':
									if (!isset($cpTextColumns_v['cpTextImgLinkBtn_Image']) || trim($cpTextColumns_v['cpTextImgLinkBtn_Image']) == "" || !is_object(File::getByID($cpTextColumns_v['cpTextImgLinkBtn_Image']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Image"), t("リンクボタン"), t("記事データ"), $cpTextColumns_k));
									}
									break;	
							}
						}
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('記事データ'), $cpTextColumns_k));
                    }
                }
            }
        } else {
            if ($cpTextColumnsEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("記事データ"), $cpTextColumnsEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-component_text_tab', 'blocks/component_text_tab/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-component_text_tab');
        $this->edit();
    }

        protected function getSmartLinkTypeOptions($include = array(), $checkNone = false)
	{
		$options = array(
			''             => sprintf("-- %s--", t("None")),
			'page'         => t("Page"),
			'url'          => t("External URL"),
			'relative_url' => t("Relative URL"),
			'file'         => t("File"),
			'image'        => t("Image")
		);
		if ($checkNone) {
            $include = array_merge(array(''), $include);
        }
		$return = array();
		foreach($include as $v){
		    if(isset($options[$v])){
		        $return[$v] = $options[$v];
		    }
		}
		return $return;
	}
}