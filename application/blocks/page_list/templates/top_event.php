<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$c = Page::getCurrentPage();
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */
?>
<style>
.c-pager {
  text-align: center;
}
.c-pager li {
  display: inline-block;
}
.c-pager li a,
.c-pager li span {
  display: block;
  vertical-align: top;
  text-decoration: none;
  width: 35px;
  height: 35px;
  margin: 0 5px;
  padding: 9px 0 0;
  background: #fff;
  font-size: 14px;
  font-size: 1.4rem;
  letter-spacing: 0;
  border: solid 1px #aaa;
}
.c-pager .c-pager_button--type_prev a {
    display: block;
    position: relative;
    width: 70px;
    height: 35px;
    padding: 9px 0 0;
    background: #fff;
    font-size: 14px;
    font-size: 1.4rem;
    text-align: center;
    text-decoration: none;
    border: solid 1px #aaa;
  padding-left: 25px;
  text-align: left;
}
.c-pager .c-pager_button--type_prev a:after {
    content: '';
    display: block;
    position: absolute;
    top: 0;
    bottom: 0;
    width: 5px;
    background: no-repeat center center;
    background-size: 100% auto;
  left: 10px;
  background-image: url(/packages/suiton_base_util/themes/base_theme/assets/img/global/arrow_triangle_left_color.png);
}

.c-pager .c-pager_button--type_next a {
    display: block;
    position: relative;
    width: 70px;
    height: 35px;
    padding: 9px 0 0;
    background: #fff;
    font-size: 14px;
    font-size: 1.4rem;
    text-align: center;
    text-decoration: none;
    border: solid 1px #aaa;
  padding-right: 25px;
    text-align: right;
}
.c-pager .c-pager_button--type_next a:after {
    content: '';
    display: block;
    position: absolute;
    top: 0;
    bottom: 0;
    width: 5px;
    background: no-repeat center center;
    background-size: 100% auto;
  right: 10px;
  background-image: url(/packages/suiton_base_util/themes/base_theme/assets/img/global/arrow_triangle_right_color.png);
}

.c-pager .c-pager_button-acitive span{
    color: #fff;
    background-color: #dc4415;
    border-color: #dc4415;
}
.c-pager_button-disabled {
    display: none!important;
}
@media only screen and (max-width: 781px) {
  .c-pager li {
    display: none;
  }

  .c-pager .c-pager_button--type_next,
  .c-pager .c-pager_button--type_prev {
    display: block;
  }
}
</style>
<?php if ($c->isEditMode() && $controller->isBlockEmpty()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php
} else {
    ?>
<div class="c-newsListB">
    <ul class="c-newsListB_lists">
    <?php
    foreach ($pages as $page):

        // Prepare data for each page being listed...
        $buttonClasses = 'ccm-block-page-list-read-more';
        $entryClasses = 'ccm-block-page-list-page-entry';
        $title = $th->entities($page->getCollectionName());
        $url = ($page->getCollectionPointerExternalLink() != '') ? $page->getCollectionPointerExternalLink() : $nh->getLinkToCollection($page);
        $target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
        $target = empty($target) ? '_self' : $target;
        $description = $page->getCollectionDescription();
        $description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
        $description = $th->entities($description);
        $thumbnail = false;
        if ($displayThumbnail) {
        $thumbnail = $page->getAttribute('thumbnail');
        }
        if (is_object($thumbnail) && $includeEntryText) {
        $entryClasses = 'ccm-block-page-list-page-entry-horizontal';
        }

        //$date = $dh->formatDateTime($page->getCollectionDatePublic(), true);
        $date = date('Y/m/d',strtotime($page->getCollectionDatePublic()));
        //Other useful page data...

        //$last_edited_by = $page->getVersionObject()->getVersionAuthorUserName();

        //$original_author = Page::getByID($page->getCollectionID(), 1)->getVersionObject()->getVersionAuthorUserName();

        /* CUSTOM ATTRIBUTE EXAMPLES:
         * $example_value = $page->getAttribute('example_attribute_handle');
         *
         * HOW TO USE IMAGE ATTRIBUTES:
         * 1) Uncomment the "$ih = Loader::helper('image');" line up top.
         * 2) Put in some code here like the following 2 lines:
         *      $img = $page->getAttribute('example_image_attribute_handle');
         *      $thumb = $ih->getThumbnail($img, 64, 9999, false);
         *    (Replace "64" with max width, "9999" with max height. The "9999" effectively means "no maximum size" for that particular dimension.)
         *    (Change the last argument from false to true if you want thumbnails cropped.)
         * 3) Output the image tag below like this:
         *		<img src="<?php echo $thumb->src ?>" width="<?php echo $thumb->width ?>" height="<?php echo $thumb->height ?>" alt="" />
         *
         * ~OR~ IF YOU DO NOT WANT IMAGES TO BE RESIZED:
         * 1) Put in some code here like the following 2 lines:
         * 	    $img_src = $img->getRelativePath();
         *      $img_width = $img->getAttribute('width');
         *      $img_height = $img->getAttribute('height');
         * 2) Output the image tag below like this:
         * 	    <img src="<?php echo $img_src ?>" width="<?php echo $img_width ?>" height="<?php echo $img_height ?>" alt="" />
         */

        /* End data preparation. */

        /* The HTML from here through "endforeach" is repeated for every item in the list... */ 

       $parent = $page->getCollectionParentID();
       $parent = Page::getByID($parent);
       $cat_name = $parent->getCollectionName();
       //$p= pageobject
       if($parent->getAttribute('attr_cat_color')){
            $color = ' style="background-color:'.$parent->getAttribute('attr_cat_color').';"';
       }else{
            $color = '';
       }
       ?>
        <li class="c-newsListB_list">
            <a href="<?php echo $url ?>" class="e-box">
                <span class="e-date"><?= $date;?></span>
                <span class="e-category is-category_houwa">
                    <em class="e-category_icon" <?= $color;?>><?= $cat_name;?></em>
                </span>
                <strong class="e-name"><?php echo $title ?></strong>
            </a>
        </li>
	<?php endforeach;?>
        </ul>
    </div>

    <?php if (count($pages) == 0): ?>
        <div class="ccm-block-page-list-no-pages"><?php echo h($noResultsMessage)?></div>
    <?php endif;
    ?>


<?php if ($showPagination): ?>
    <?php 
    $pagination = $list->getPagination();
    if ($pagination->getTotalPages() > 1) {
        $options = array(
            'proximity'           => 2,
            'prev_message'        => '前へ',
            'next_message'        => '次へ',
            'dots_message'        => '...',
            'active_suffix'       => '',
            'css_container_class' => 'c-pager',
            'css_prev_class'      => 'c-pager_button c-pager_button--type_prev',
            'css_next_class'      => 'c-pager_button c-pager_button--type_next',
            'css_disabled_class'  => 'c-pager_button-disabled',
            'css_dots_class'      => 'c-pager_button-dots',
            'css_active_class'    => 'c-pager_button-acitive'
        );
        echo '<div class="c-section">'.$pagination->renderDefaultView($options).'</div>';
    }
    ?>
<?php endif;?>

<?php } ?>
