<?php namespace Application\Block\CreatePodcastXml;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Database;
use File;
use Page;
use Permissions;
use URL;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('cpxRepeat' => array('author', 'description_1', 'title', 'pubDate', 'enclosure'));
    protected $btExportFileColumns = array('enclosure');
    protected $btExportPageColumns = array();
    protected $btExportTables = array('btCreatePodcastXml', 'btCreatePodcastXmlCpxRepeatEntries');
    protected $btTable = 'btCreatePodcastXml';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("ポッドキャスト作成");
    }

    public function getSearchableContent()
    {
        $content = array();
        $db = Database::connection();
        $cpxRepeat_items = $db->fetchAll('SELECT * FROM btCreatePodcastXmlCpxRepeatEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpxRepeat_items as $cpxRepeat_item_k => $cpxRepeat_item_v) {
            if (isset($cpxRepeat_item_v["author"]) && trim($cpxRepeat_item_v["author"]) != "") {
                $content[] = $cpxRepeat_item_v["author"];
            }
            if (isset($cpxRepeat_item_v["description_1"]) && trim($cpxRepeat_item_v["description_1"]) != "") {
                $content[] = $cpxRepeat_item_v["description_1"];
            }
            if (isset($cpxRepeat_item_v["title"]) && trim($cpxRepeat_item_v["title"]) != "") {
                $content[] = $cpxRepeat_item_v["title"];
            }
        }
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $cpxRepeat = array();
        $cpxRepeat_items = $db->fetchAll('SELECT * FROM btCreatePodcastXmlCpxRepeatEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpxRepeat_items as $cpxRepeat_item_k => &$cpxRepeat_item_v) {
            $cpxRepeat_item_v["enclosure_id"] = isset($cpxRepeat_item_v["enclosure"]) && trim($cpxRepeat_item_v["enclosure"]) != "" ? (int)$cpxRepeat_item_v["enclosure"] : false;
            $cpxRepeat_item_v["enclosure"] = false;
            if ($cpxRepeat_item_v["enclosure_id"] > 0 && ($cpxRepeat_item_v["enclosure_file"] = File::getByID($cpxRepeat_item_v["enclosure_id"])) && is_object($cpxRepeat_item_v["enclosure_file"])) {
                $fp = new Permissions($cpxRepeat_item_v["enclosure_file"]);
                if ($fp->canViewFile()) {
                    $urls = array('relative' => $cpxRepeat_item_v["enclosure_file"]->getRelativePath());
                    if (($c = Page::getCurrentPage()) && $c instanceof Page) {
                        $urls['download'] = URL::to('/download_file', $cpxRepeat_item_v["enclosure_id"], $c->getCollectionID());
                    }
                    $cpxRepeat_item_v["enclosure_file"]->urls = $urls;
                    $cpxRepeat_item_v["enclosure"] = $cpxRepeat_item_v["enclosure_file"];
                }
            }
        }
        $this->set('cpxRepeat_items', $cpxRepeat_items);
        $this->set('cpxRepeat', $cpxRepeat);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btCreatePodcastXmlCpxRepeatEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $cpxRepeat_items = $db->fetchAll('SELECT * FROM btCreatePodcastXmlCpxRepeatEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpxRepeat_items as $cpxRepeat_item) {
            unset($cpxRepeat_item['id']);
            $cpxRepeat_item['bID'] = $newBID;
            $db->insert('btCreatePodcastXmlCpxRepeatEntries', $cpxRepeat_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $cpxRepeat = $this->get('cpxRepeat');
        $this->set('cpxRepeat_items', array());
        $this->set('cpxRepeat', $cpxRepeat);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        $cpxRepeat = $this->get('cpxRepeat');
        $cpxRepeat_items = $db->fetchAll('SELECT * FROM btCreatePodcastXmlCpxRepeatEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpxRepeat_items as &$cpxRepeat_item) {
            if (!File::getByID($cpxRepeat_item['enclosure'])) {
                unset($cpxRepeat_item['enclosure']);
            }
        }
        $this->set('cpxRepeat', $cpxRepeat);
        $this->set('cpxRepeat_items', $cpxRepeat_items);
    }

    protected function addEdit()
    {
        $cpxRepeat = array();
        $this->set('cpxRepeat', $cpxRepeat);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/create_podcast_xml/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/create_podcast_xml/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/create_podcast_xml/js_form/handlebars-helpers.js', array(), $this->pkg);
        $al->register('css', 'datetimepicker', 'blocks/create_podcast_xml/css_form/bootstrap-datetimepicker.min.css', array(), $this->pkg);
        $al->register('css', 'bootstrap_fonts', 'blocks/create_podcast_xml/css_form/bootstrap.fonts.css', array(), $this->pkg);
        $al->register('javascript', 'moment', 'blocks/create_podcast_xml/js_form/moment.js', array(), $this->pkg);
        $al->register('javascript', 'bootstrap', 'blocks/create_podcast_xml/js_form/bootstrap.min.js', array(), $this->pkg);
        $al->register('javascript', 'datetimepicker', 'blocks/create_podcast_xml/js_form/bootstrap-datetimepicker.min.js', array(), $this->pkg);
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->requireAsset('css', 'datetimepicker');
        $this->requireAsset('css', 'bootstrap_fonts');
        $this->requireAsset('javascript', 'moment');
        $this->requireAsset('javascript', 'bootstrap');
        $this->requireAsset('javascript', 'datetimepicker');
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $db = Database::connection();
        $rows = $db->fetchAll('SELECT * FROM btCreatePodcastXmlCpxRepeatEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $cpxRepeat_items = isset($args['cpxRepeat']) && is_array($args['cpxRepeat']) ? $args['cpxRepeat'] : array();
        $queries = array();
        if (!empty($cpxRepeat_items)) {
            $i = 0;
            foreach ($cpxRepeat_items as $cpxRepeat_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($cpxRepeat_item['author']) && trim($cpxRepeat_item['author']) != '') {
                    $data['author'] = trim($cpxRepeat_item['author']);
                } else {
                    $data['author'] = null;
                }
                if (isset($cpxRepeat_item['description_1']) && trim($cpxRepeat_item['description_1']) != '') {
                    $data['description_1'] = trim($cpxRepeat_item['description_1']);
                } else {
                    $data['description_1'] = null;
                }
                if (isset($cpxRepeat_item['title']) && trim($cpxRepeat_item['title']) != '') {
                    $data['title'] = trim($cpxRepeat_item['title']);
                } else {
                    $data['title'] = null;
                }
                if (isset($cpxRepeat_item['pubDate']) && trim($cpxRepeat_item['pubDate']) != '') {
                    $data['pubDate'] = strtotime(substr($cpxRepeat_item['pubDate'], 0, 22));
                } else {
                    $data['pubDate'] = null;
                }
                if (isset($cpxRepeat_item['enclosure']) && trim($cpxRepeat_item['enclosure']) != '') {
                    $data['enclosure'] = trim($cpxRepeat_item['enclosure']);
                } else {
                    $data['enclosure'] = null;
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btCreatePodcastXmlCpxRepeatEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btCreatePodcastXmlCpxRepeatEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btCreatePodcastXmlCpxRepeatEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        $cpxRepeatEntriesMin = 0;
        $cpxRepeatEntriesMax = 0;
        $cpxRepeatEntriesErrors = 0;
        $cpxRepeat = array();
        if (isset($args['cpxRepeat']) && is_array($args['cpxRepeat']) && !empty($args['cpxRepeat'])) {
            if ($cpxRepeatEntriesMin >= 1 && count($args['cpxRepeat']) < $cpxRepeatEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("データ"), $cpxRepeatEntriesMin, count($args['cpxRepeat'])));
                $cpxRepeatEntriesErrors++;
            }
            if ($cpxRepeatEntriesMax >= 1 && count($args['cpxRepeat']) > $cpxRepeatEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("データ"), $cpxRepeatEntriesMax, count($args['cpxRepeat'])));
                $cpxRepeatEntriesErrors++;
            }
            if ($cpxRepeatEntriesErrors == 0) {
                foreach ($args['cpxRepeat'] as $cpxRepeat_k => $cpxRepeat_v) {
                    if (is_array($cpxRepeat_v)) {
                        if (in_array("author", $this->btFieldsRequired['cpxRepeat']) && (!isset($cpxRepeat_v['author']) || trim($cpxRepeat_v['author']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("作者"), t("データ"), $cpxRepeat_k));
                        }
                        if (in_array("description_1", $this->btFieldsRequired['cpxRepeat']) && (!isset($cpxRepeat_v['description_1']) || trim($cpxRepeat_v['description_1']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("description"), t("データ"), $cpxRepeat_k));
                        }
                        if (in_array("title", $this->btFieldsRequired['cpxRepeat']) && (!isset($cpxRepeat_v['title']) || trim($cpxRepeat_v['title']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("タイトル"), t("データ"), $cpxRepeat_k));
                        }
                        if (in_array("pubDate", $this->btFieldsRequired['cpxRepeat']) && (!isset($cpxRepeat_v['pubDate']) || trim($cpxRepeat_v['pubDate']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("公開日"), t("データ"), $cpxRepeat_k));
                        } elseif (isset($cpxRepeat_v['pubDate']) && trim($cpxRepeat_v['pubDate']) != "" && strtotime($cpxRepeat_v['pubDate']) <= 0) {
                            $e->add(t("The %s field is not a valid date (%s, row #%s).", t("公開日"), t("データ"), $cpxRepeat_k));
                        }
                        if (in_array("enclosure", $this->btFieldsRequired['cpxRepeat']) && (!isset($cpxRepeat_v['enclosure']) || trim($cpxRepeat_v['enclosure']) == "" || !is_object(File::getByID($cpxRepeat_v['enclosure'])))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("ファイル"), t("データ"), $cpxRepeat_k));
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('データ'), $cpxRepeat_k));
                    }
                }
            }
        } else {
            if ($cpxRepeatEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("データ"), $cpxRepeatEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('css', 'datetimepicker-composer', 'blocks/create_podcast_xml/css_form/bootstrap-datetimepicker-composer.css', array(), $this->pkg);
        $al->register('javascript', 'auto-js-create_podcast_xml', 'blocks/create_podcast_xml/auto.js', array(), $this->pkg);
        $this->requireAsset('css', 'datetimepicker-composer');
        $this->requireAsset('javascript', 'auto-js-create_podcast_xml');
        $this->edit();
    }
}