<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
<?php $repeatable_container_id = 'btCreatePodcastXml-cpxRepeat-container-' . $identifier_getString; ?>
    <div id="<?php echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $cpxRepeat_items,
                        'order' => array_keys($cpxRepeat_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php echo t('データ') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][author]" class="control-label"><?php echo t("作者"); ?></label>
    <?php echo isset($btFieldsRequired['cpxRepeat']) && in_array('author', $btFieldsRequired['cpxRepeat']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][author]" id="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][author]" class="form-control title-me" type="text" value="{{ author }}" maxlength="255" />
</div>            <div class="form-group">
    <label for="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][description_1]" class="control-label"><?php echo t("description"); ?></label>
    <?php echo isset($btFieldsRequired['cpxRepeat']) && in_array('description_1', $btFieldsRequired['cpxRepeat']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][description_1]" id="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][description_1]" class="form-control" type="text" value="{{ description_1 }}" maxlength="255" />
</div>            <div class="form-group">
    <label for="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][title]" class="control-label"><?php echo t("タイトル"); ?></label>
    <?php echo isset($btFieldsRequired['cpxRepeat']) && in_array('title', $btFieldsRequired['cpxRepeat']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][title]" id="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][title]" class="form-control" type="text" value="{{ title }}" maxlength="255" />
</div>            <div class="form-group">
    <label for="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][pubDate]" class="control-label"><?php echo t("公開日"); ?></label>
    <?php echo isset($btFieldsRequired['cpxRepeat']) && in_array('pubDate', $btFieldsRequired['cpxRepeat']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][pubDate]" id="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][pubDate]" class="ft-datetime-cpxRepeat-pubDate" type="text" value="{{formatDate pubDate 'MM/DD/YYYY HH:mm:ss'}}" autocomplete="off" />
</div>            
<div class="form-group">
    <label for="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][enclosure]" class="control-label"><?php echo t("ファイル"); ?></label>
    <?php echo isset($btFieldsRequired['cpxRepeat']) && in_array('enclosure', $btFieldsRequired['cpxRepeat']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-file-selector-input-name="<?php echo $view->field('cpxRepeat'); ?>[{{id}}][enclosure]" class="ccm-file-selector ft-file-enclosure-file-selector" data-file-selector-f-id="{{ enclosure }}"></div>
</div></div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btCreatePodcastXml.cpxRepeat.edit.open', {id: '<?php echo $repeatable_container_id; ?>'});
    $.each($('#<?php echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>