<?php defined("C5_EXECUTE") or die("Access Denied."); ?><?php if (!empty($cpxRepeat_items)) { ?><?xml version="1.0" encoding="UTF-8" ?>
<rss xmlns:itunes="http://www.itunes.com/DTDs/Podcast-1.0.dtd" version="2.0">
<channel>

<link>http://www.myoshinji.or.jp/educate/podcast/</link>
<language>ja</language>
<description>本提唱録は第26代妙心寺派管長であり花園大学学長でもあった山田無文老大師が、昭和33～40年頃、祥福僧堂師家として専門道場や居士禅会などで提唱された講演録です。
当時、禅会に通われていた神戸市在住の安藤慧光居士が録音されたテープを、近年ご子息鷹彦氏が妙心寺派教化センターに寄贈されました。
妙心寺派では貴重な教化資料として保存すると共に、多くの皆様に禅の教えに触れていただくべくデジタルデータ化いたしました。
毎月2回程度配信（ポッドキャスト）いたしますので、お気軽にご利用ください。
なお本録音の中には、今日では差別的と思われる表現がありますが、録音された時代的背景や無文老師の提唱を音声で聞くことの意義を鑑み、そのままとしました。　何卒ご理解賜ります様、宜しくお願い申し上げます。
</description>
<title>妙心寺派教化センター</title>
<docs>http://blogs.law.harvard.edu/tech/rss</docs>
<pubDate>Fri, 10 Nov 2017 14:57:15 +0900</pubDate>
<itunes:author>妙心寺派教化センター</itunes:author>
<itunes:image href="http://www.myoshinji.or.jp/educate/podcast/img/thumb.jpg" />

<?php foreach ($cpxRepeat_items as $cpxRepeat_item_key => $cpxRepeat_item) { ?>
<item>
<?php if (isset($cpxRepeat_item["author"]) && trim($cpxRepeat_item["author"]) != "") { ?>
<author><?php echo h($cpxRepeat_item["author"]); ?></author>
<?php } ?>
<?php if (isset($cpxRepeat_item["description_1"]) && trim($cpxRepeat_item["description_1"]) != "") { ?>
<description><?php echo h($cpxRepeat_item["description_1"]); ?></description>
<?php } ?>
<?php if (isset($cpxRepeat_item["title"]) && trim($cpxRepeat_item["title"]) != "") { ?>
<title><?php echo h($cpxRepeat_item["title"]); ?></title>
<?php } ?>
<?php if (isset($cpxRepeat_item["pubDate"]) && $cpxRepeat_item["pubDate"] > 0) { ?>
<pubDate><?php echo strftime("DATE_RFC2822",$cpxRepeat_item["pubDate"]); ?></pubDate>
<?php } ?>
<?php if (isset($cpxRepeat_item["enclosure"]) && $cpxRepeat_item["enclosure"] !== false) { ?>
<enclosure url="<?= $cpxRepeat_item["enclosure_file"]->getURL();?>" length="" type="audio/mpeg" />
<?php } ?>
<itunes:author><?php echo h($cpxRepeat_item["author"]); ?></itunes:author>
<?php } ?>
</item>
</channel>
</rss>
<?php } ?>