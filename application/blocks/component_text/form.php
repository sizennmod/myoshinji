<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php $tabs = array(
    array('form-basics-' . $identifier_getString, t('Basics'), true),
    array('form-cpTextImages_items-' . $identifier_getString, t('画像データ')),
    array('form-cpLinkBtn_items-' . $identifier_getString, t('リンクボタン'))
);
echo Core::make('helper/concrete/ui')->tabs($tabs); ?>

<div class="ccm-tab-content" id="ccm-tab-content-form-basics-<?php echo $identifier_getString; ?>">
    <div class="form-group">
    <?php echo $form->label('cpTextTitleype', t("タイトルタイプ")); ?>
    <?php echo isset($btFieldsRequired) && in_array('cpTextTitleype', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->select($view->field('cpTextTitleype'), $cpTextTitleype_options, $cpTextTitleype, array()); ?>
    </div>
<div class="form-group">
    <?php echo $form->label('cpTextTitle', t("タイトル")); ?>
    <?php echo isset($btFieldsRequired) && in_array('cpTextTitle', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->textarea($view->field('cpTextTitle'), $cpTextTitle, array (
  'rows' => 5,
)); ?>
</div>
    <div class="form-group">
    <?php echo $form->label('cpTextAddEmph', t("エリアを強調表示にする")); ?>
    <?php echo isset($btFieldsRequired) && in_array('cpTextAddEmph', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->select($view->field('cpTextAddEmph'), (isset($btFieldsRequired) && in_array('cpTextAddEmph', $btFieldsRequired) ? array() : array("" => "--" . t("Select") . "--")) + array(0 => t("No"), 1 => t("Yes")), $cpTextAddEmph, array()); ?>
</div><div class="form-group">
    <?php echo $form->label('cpTextImgPosition', t("画像表示位置")); ?>
    <?php echo isset($btFieldsRequired) && in_array('cpTextImgPosition', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->select($view->field('cpTextImgPosition'), $cpTextImgPosition_options, $cpTextImgPosition, array()); ?>
</div><div class="form-group">
    <?php echo $form->label('cpTextWyg', t("本文")); ?>
    <?php echo isset($btFieldsRequired) && in_array('cpTextWyg', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo Core::make('editor')->outputBlockEditModeEditor($view->field('cpTextWyg'), $cpTextWyg); ?>
</div>
</div>

<div class="ccm-tab-content" id="ccm-tab-content-form-cpTextImages_items-<?php echo $identifier_getString; ?>">
    <script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
<?php $repeatable_container_id = 'btComponentText-cpTextImages-container-' . $identifier_getString; ?>
    <div id="<?php echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $cpTextImages_items,
                        'order' => array_keys($cpTextImages_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php echo t('画像データ') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImg]" class="control-label"><?php echo t("画像"); ?></label>
    <?php echo isset($btFieldsRequired['cpTextImages']) && in_array('cpTextImg', $btFieldsRequired['cpTextImages']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-file-selector-input-name="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImg]" class="ccm-file-selector ft-image-cpTextImg-file-selector" data-file-selector-f-id="{{ cpTextImg }}"></div>
</div>            <?php $cpTextImgLink_ContainerID = 'btComponentText-cpTextImgLink-container-' . $identifier_getString; ?>
<div class="ft-smart-link" id="<?php echo $cpTextImgLink_ContainerID; ?>">
	<div class="form-group">
		<label for="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgLink]" class="control-label"><?php echo t("画像リンク"); ?></label>
	    <?php echo isset($btFieldsRequired['cpTextImages']) && in_array('cpTextImgLink', $btFieldsRequired['cpTextImages']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
	    <?php $cpTextImagesCpTextImgLink_options = $cpTextImgLink_Options; ?>
                    <select name="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgLink]" id="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgLink]" class="form-control ft-smart-link-type">{{#select cpTextImgLink}}<?php foreach ($cpTextImagesCpTextImgLink_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
	</div>
	
	<div class="form-group">
		<div class="ft-smart-link-options hidden" style="padding-left: 10px;">
			<div class="form-group">
				<label for="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgLink_Title]" class="control-label"><?php echo t("Title"); ?></label>
			    <input name="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgLink_Title]" id="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgLink_Title]" class="form-control" type="text" value="{{ cpTextImgLink_Title }}" />		
			</div>
			
			<div class="form-group hidden" data-link-type="page">
			<label for="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgLink_Page]" class="control-label"><?php echo t("Page"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-page-selector="{{token}}" data-input-name="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgLink_Page]" data-cID="{{cpTextImgLink_Page}}"></div>
		</div>

		<div class="form-group hidden" data-link-type="url">
			<label for="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgLink_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgLink_URL]" id="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgLink_URL]" class="form-control" type="text" value="{{ cpTextImgLink_URL }}" />
		</div>
		</div>
	</div>
</div>
            <div class="form-group">
    <label for="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImagesAlt]" class="control-label"><?php echo t("画像alt"); ?></label>
    <?php echo isset($btFieldsRequired['cpTextImages']) && in_array('cpTextImagesAlt', $btFieldsRequired['cpTextImages']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImagesAlt]" id="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImagesAlt]" class="form-control" type="text" value="{{ cpTextImagesAlt }}" maxlength="255" />
</div>            <div class="form-group">
    <label for="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgCapTitle]" class="control-label"><?php echo t("キャプションタイトル"); ?></label>
    <?php echo isset($btFieldsRequired['cpTextImages']) && in_array('cpTextImgCapTitle', $btFieldsRequired['cpTextImages']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgCapTitle]" id="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgCapTitle]" class="form-control" rows="5">{{ cpTextImgCapTitle }}</textarea>
</div>            <div class="form-group">
    <label for="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgCapText]" class="control-label"><?php echo t("キャプションテキスト"); ?></label>
    <?php echo isset($btFieldsRequired['cpTextImages']) && in_array('cpTextImgCapText', $btFieldsRequired['cpTextImages']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgCapText]" id="<?php echo $view->field('cpTextImages'); ?>[{{id}}][cpTextImgCapText]" class="form-control" rows="5">{{ cpTextImgCapText }}</textarea>
</div></div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btComponentText.cpTextImages.edit.open', {id: '<?php echo $repeatable_container_id; ?>'});
    $.each($('#<?php echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>
</div>


<div class="ccm-tab-content" id="ccm-tab-content-form-cpLinkBtn_items-<?php echo $identifier_getString; ?>">
    <?php $repeatable_container_id = 'btComponentText-cpLinkBtn-container-' . $identifier_getString; ?>
    <div id="<?php echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $cpLinkBtn_items,
                        'order' => array_keys($cpLinkBtn_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php echo t('リンクボタン') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <!-- <div class="form-group">
    <label for="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnColor]" class="control-label"><?php echo t("色"); ?></label>
    <?php echo isset($btFieldsRequired['cpLinkBtn']) && in_array('cpLinkBtnColor', $btFieldsRequired['cpLinkBtn']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php $cpLinkBtnCpLinkBtnColor_options = $cpLinkBtn['cpLinkBtnColor_options']; ?>
                    <select name="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnColor]" id="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnColor]" class="form-control">{{#select cpLinkBtnColor}}<?php foreach ($cpLinkBtnCpLinkBtnColor_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
</div> -->            <?php $cpLinkBtnLink_ContainerID = 'btComponentText-cpLinkBtnLink-container-' . $identifier_getString; ?>
<div class="ft-smart-link" id="<?php echo $cpLinkBtnLink_ContainerID; ?>">
    <div class="form-group">
        <label for="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink]" class="control-label"><?php echo t("リンク"); ?></label>
        <?php echo isset($btFieldsRequired['cpLinkBtn']) && in_array('cpLinkBtnLink', $btFieldsRequired['cpLinkBtn']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
        <?php $cpLinkBtnCpLinkBtnLink_options = $cpLinkBtnLink_Options; ?>
                    <select name="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink]" id="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink]" class="form-control ft-smart-link-type">{{#select cpLinkBtnLink}}<?php foreach ($cpLinkBtnCpLinkBtnLink_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
    </div>
    
    <div class="form-group">
        <div class="ft-smart-link-options hidden" style="padding-left: 10px;">
            <div class="form-group">
                <label for="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_Title]" class="control-label"><?php echo t("Title"); ?></label>
                <input name="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_Title]" id="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_Title]" class="form-control" type="text" value="{{ cpLinkBtnLink_Title }}" />        
            </div>
            
            <div class="form-group hidden" data-link-type="page">
            <label for="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_Page]" class="control-label"><?php echo t("Page"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-page-selector="{{token}}" data-input-name="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_Page]" data-cID="{{cpLinkBtnLink_Page}}"></div>
        </div>

        <div class="form-group hidden" data-link-type="url">
            <label for="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_URL]" id="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_URL]" class="form-control" type="text" value="{{ cpLinkBtnLink_URL }}" />
        </div>

        <div class="form-group hidden" data-link-type="relative_url">
            <label for="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_Relative_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_Relative_URL]" id="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_Relative_URL]" class="form-control" type="text" value="{{ cpLinkBtnLink_Relative_URL }}" />
        </div>

        <div class="form-group hidden" data-link-type="file">
            <label for="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_File]" class="control-label"><?php echo t("File"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_File]" class="ccm-file-selector" data-file-selector-f-id="{{ cpLinkBtnLink_File }}"></div>   
        </div>

        <div class="form-group hidden" data-link-type="image">
            <label for="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_Image]" class="control-label"><?php echo t("Image"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('cpLinkBtn'); ?>[{{id}}][cpLinkBtnLink_Image]" class="ccm-file-selector" data-file-selector-f-id="{{ cpLinkBtnLink_Image }}"></div>
        </div>
        </div>
    </div>
</div>
</div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btComponentText.cpLinkBtn.edit.open', {id: '<?php echo $repeatable_container_id; ?>'});
    $.each($('#<?php echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>
</div>