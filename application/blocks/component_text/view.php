<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

	<?php
	$wrapper = 'textarea textarea-wraparound';
	switch($cpTextImgPosition) {
		case "1":
			$wrapper .= ' c-section_floFig c-section_floFig--left';
			// ENTER MARKUP HERE FOR FIELD "画像表示位置" : CHOICE "左"
			$center = false;
			break;
		case "2":
			$wrapper .= ' c-section_floFig c-section_floFig--center';
			$center = true;
			// ENTER MARKUP HERE FOR FIELD "画像表示位置" : CHOICE "中央"
			break;
		case "3":
			$wrapper .= ' c-section_floFig c-section_floFig--right';
			// ENTER MARKUP HERE FOR FIELD "画像表示位置" : CHOICE "右"
			$center = false;
			break;
		case "4":
			$wrapper .= ' c-section';
			// ENTER MARKUP HERE FOR FIELD "画像表示位置" : CHOICE "右"
			$center = true;
			break;
		default:
			$wrapper .= ' c-section';
			// ENTER MARKUP HERE FOR FIELD "画像表示位置" : CHOICE "右"
			$center = true;
			break;
	} 

	if (isset($cpTextAddEmph) && trim($cpTextAddEmph) != "") {
		if($cpTextAddEmph == 1) {
			$ext_wrapper = ' c-section--type_frame';
		}
	}
	?>
<?php if(!$center){ ?>
	<div class="c-section <?= $ext_wrapper;?>">
		<?php if (!empty($cpTextImages_items)) { ?>
			<div class="<?= $wrapper;?>">
			<?php foreach ($cpTextImages_items as $cpTextImages_item_key => $cpTextImages_item) { ?>
				<?php if ($cpTextImages_item["cpTextImg"]) { 
					$max_width = $cpTextImages_item["cpTextImg"]->getAttribute('width');
					if($max_width >= 640) {
						$sp_layout_full = true;
					}else{
						$sp_layout_full = false;
					}
					if(($cpTextImgPosition == 1 || $cpTextImgPosition == 3) && $max_width > 460){
						$max_width = 460;
					}
				?>
				<figure>
					<?php if (trim($cpTextImages_item["cpTextImgLink_URL"])) echo '<a href="'.$cpTextImages_item["cpTextImgLink_URL"].'">';?>
					<img src="<?php echo $cpTextImages_item["cpTextImg"]->getURL(); ?>" alt="<?php echo h($cpTextImages_item["cpTextImagesAlt"]); ?>"/>
					<?php if (trim($cpTextImages_item["cpTextImgLink_URL"])) echo '</a>';?>

					<?php if ((isset($cpTextImages_item["cpTextImgCapTitle"]) && trim($cpTextImages_item["cpTextImgCapTitle"]) != "") || (isset($cpTextImages_item["cpTextImgCapText"]) && trim($cpTextImages_item["cpTextImgCapText"]))) { ?>
						<figcaption class="e-caption">
							<?php if (isset($cpTextImages_item["cpTextImgCapTitle"]) && trim($cpTextImages_item["cpTextImgCapTitle"]) != "") { ?>
								<div class="caption-title">
									<p><?php echo h($cpTextImages_item["cpTextImgCapTitle"]); ?></p>
								</div>
							<?php } ?>
							<?php if (isset($cpTextImages_item["cpTextImgCapText"]) && trim($cpTextImages_item["cpTextImgCapText"]) != "") { ?>
								<div class="caption-text">
									<p><?php echo h($cpTextImages_item["cpTextImgCapText"]); ?></p>
								</div>
							<?php } ?>
						</figcaption>
					<?php } ?>
				</figure>
				<?php } ?>
			<?php } ?>
			</div>
		<?php } ?>
		
		
		<?php 
			switch($cpTextTitleype) {
			case "1":
			    if (isset($cpTextTitle) && trim($cpTextTitle) != "") echo '<h2 class="c-section_head">'.$cpTextTitle.'</h2>';
			    break;
			case "2":
			    if (isset($cpTextTitle) && trim($cpTextTitle) != "") echo '<h3 class="c-section_head2">'.$cpTextTitle.'</h3>';
			    break;
			default:
			    if (isset($cpTextTitle) && trim($cpTextTitle) != "") echo '<h2 class="c-section_head">'.$cpTextTitle.'</h2>';
			    break;
			} ?>
		
		<?php if (isset($cpTextWyg) && trim($cpTextWyg) != "") { ?>
			<div class="c-section_text">
			<?php echo $cpTextWyg; ?>
			</div>
		<?php } ?>
	
		<?php if (!empty($cpLinkBtn_items)) { ?>
		<div class="btn-group<?php if(count($cpLinkBtn_items) > 1) echo ' btn-group-2col';?>">
			<?php foreach ($cpLinkBtn_items as $cpLinkBtn_item_key => $cpLinkBtn_item) { ?><?php if (trim($cpLinkBtn_item["cpLinkBtnColor"]) != "") { ?>
			
			<?php 
			switch($cpLinkBtn_item["cpLinkBtnColor"]) {
			case "1":
				$class = "link_btn";
				break;
			case "2":
				$class = "link_btn link_btn-red";
				break;
			} ?>
			<?php } ?>

			<?php if (trim($cpLinkBtn_item["cpLinkBtnLink_URL"]) != "") { ?>
			<div class="c-linkBtn c-section_button">
				<p class="c-linkBtn_item">
				<?php
				$cpLinkBtn_itemcpLinkBtnLink_Attributes = array();
				$cpLinkBtn_itemcpLinkBtnLink_Attributes['href'] = $cpLinkBtn_item["cpLinkBtnLink_URL"];
				$cpLinkBtn_item["cpLinkBtnLink_AttributesHtml"] = join(' ', array_map(function ($key) use ($cpLinkBtn_itemcpLinkBtnLink_Attributes) {
					return $key . '="' . $cpLinkBtn_itemcpLinkBtnLink_Attributes[$key] . '"';
				}, array_keys($cpLinkBtn_itemcpLinkBtnLink_Attributes)));
				echo sprintf('<a class="%s e-box" %s><span class="e-box_in"><strong class="e-name">%s</strong></span></a>', $class,$cpLinkBtn_item["cpLinkBtnLink_AttributesHtml"], $cpLinkBtn_item["cpLinkBtnLink_Title"]); ?>
				<?php } ?>
				</p>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
<?php }else{ ?>
	<div class="c-section c-section_floFig--center">
		<div class="c-section_columns">
			<div class="c-section_column">
			<?php 
			switch($cpTextTitleype) {
			case "1":
			    if (isset($cpTextTitle) && trim($cpTextTitle) != "") echo '<h2 class="c-section_head">'.$cpTextTitle.'</h2>';
			    break;
			case "2":
			    if (isset($cpTextTitle) && trim($cpTextTitle) != "") echo '<h3 class="c-section_head2">'.$cpTextTitle.'</h3>';
			    break;
			default:
			    if (isset($cpTextTitle) && trim($cpTextTitle) != "") echo '<h2 class="c-section_head">'.$cpTextTitle.'</h2>';
			    break;
			} ?>

				<?php if (!empty($cpTextImages_items) && $cpTextImgPosition == 4) { ?>
					<div class="c-section_figure">
					<?php foreach ($cpTextImages_items as $cpTextImages_item_key => $cpTextImages_item) { ?>
						<?php if ($cpTextImages_item["cpTextImg"]) { 
							$max_width = $cpTextImages_item["cpTextImg"]->getAttribute('width');
							if($max_width >= 640) {
								$sp_layout_full = true;
							}else{
								$sp_layout_full = false;
							}
							if(($cpTextImgPosition == 1 || $cpTextImgPosition == 3) && $max_width > 460){
								$max_width = 460;
							}
						?>
							<figure>
								<?php if (trim($cpTextImages_item["cpTextImgLink_URL"])) echo '<a href="'.$cpTextImages_item["cpTextImgLink_URL"].'">';?>
								<img src="<?php echo $cpTextImages_item["cpTextImg"]->getURL(); ?>" alt="<?php echo h($cpTextImages_item["cpTextImagesAlt"]); ?>"/>
								<?php if (trim($cpTextImages_item["cpTextImgLink_URL"])) echo '</a>';?>

								<?php if ((isset($cpTextImages_item["cpTextImgCapTitle"]) && trim($cpTextImages_item["cpTextImgCapTitle"]) != "") || (isset($cpTextImages_item["cpTextImgCapText"]) && trim($cpTextImages_item["cpTextImgCapText"]))) { ?>
									<figcaption class="e-caption">
										<?php if (isset($cpTextImages_item["cpTextImgCapTitle"]) && trim($cpTextImages_item["cpTextImgCapTitle"]) != "") { ?>
											<div class="caption-title">
												<p><?php echo h($cpTextImages_item["cpTextImgCapTitle"]); ?></p>
											</div>
										<?php } ?>
										<?php if (isset($cpTextImages_item["cpTextImgCapText"]) && trim($cpTextImages_item["cpTextImgCapText"]) != "") { ?>
											<div class="caption-text">
												<p><?php echo h($cpTextImages_item["cpTextImgCapText"]); ?></p>
											</div>
										<?php } ?>
									</figcaption>
								<?php } ?>
							</figure>
						<?php } ?>
					<?php } ?>
					</div>
				<?php } ?>

				<?php if (isset($cpTextWyg) && trim($cpTextWyg) != "") { ?>
					<div class="c-section_text">
					<?php echo $cpTextWyg; ?>
					</div>
				<?php } ?>
				<!-- <p class="c-section_figure">
					<img src="../assets/img/template/dummy_img_02.png" alt="" class="e-img">
					<span class="e-caption">キャプションキャプションキャプション</span>
				</p> -->

				<?php if (!empty($cpTextImages_items) && $cpTextImgPosition == 2) { ?>
					<div class="c-section_figure">
					<?php foreach ($cpTextImages_items as $cpTextImages_item_key => $cpTextImages_item) { ?>
						<?php if ($cpTextImages_item["cpTextImg"]) { 
							$max_width = $cpTextImages_item["cpTextImg"]->getAttribute('width');
							if($max_width >= 640) {
								$sp_layout_full = true;
							}else{
								$sp_layout_full = false;
							}
							if(($cpTextImgPosition == 1 || $cpTextImgPosition == 3) && $max_width > 460){
								$max_width = 460;
							}
						?>
							<figure>
								<?php if (trim($cpTextImages_item["cpTextImgLink_URL"])) echo '<a href="'.$cpTextImages_item["cpTextImgLink_URL"].'">';?>
								<img src="<?php echo $cpTextImages_item["cpTextImg"]->getURL(); ?>" alt="<?php echo h($cpTextImages_item["cpTextImagesAlt"]); ?>"/>
								<?php if (trim($cpTextImages_item["cpTextImgLink_URL"])) echo '</a>';?>

								<?php if ((isset($cpTextImages_item["cpTextImgCapTitle"]) && trim($cpTextImages_item["cpTextImgCapTitle"]) != "") || (isset($cpTextImages_item["cpTextImgCapText"]) && trim($cpTextImages_item["cpTextImgCapText"]))) { ?>
									<figcaption class="e-caption">
										<?php if (isset($cpTextImages_item["cpTextImgCapTitle"]) && trim($cpTextImages_item["cpTextImgCapTitle"]) != "") { ?>
											<div class="caption-title">
												<p><?php echo h($cpTextImages_item["cpTextImgCapTitle"]); ?></p>
											</div>
										<?php } ?>
										<?php if (isset($cpTextImages_item["cpTextImgCapText"]) && trim($cpTextImages_item["cpTextImgCapText"]) != "") { ?>
											<div class="caption-text">
												<p><?php echo h($cpTextImages_item["cpTextImgCapText"]); ?></p>
											</div>
										<?php } ?>
									</figcaption>
								<?php } ?>
							</figure>
						<?php } ?>
					<?php } ?>
					</div>
				<?php } ?>

				<?php if (!empty($cpLinkBtn_items)) { ?>
				<div class="btn-group<?php if(count($cpLinkBtn_items) > 1) echo ' btn-group-2col';?>">
					<?php foreach ($cpLinkBtn_items as $cpLinkBtn_item_key => $cpLinkBtn_item) { ?><?php if (trim($cpLinkBtn_item["cpLinkBtnColor"]) != "") { ?>
					
					<?php 
					switch($cpLinkBtn_item["cpLinkBtnColor"]) {
					case "1":
						$class = "link_btn";
						break;
					case "2":
						$class = "link_btn link_btn-red";
						break;
					} ?>
					<?php } ?>

					<?php if (trim($cpLinkBtn_item["cpLinkBtnLink_URL"]) != "") { ?>
					<div class="c-linkBtn c-section_button">
						<p class="c-linkBtn_item">
						<?php
						$cpLinkBtn_itemcpLinkBtnLink_Attributes = array();
						$cpLinkBtn_itemcpLinkBtnLink_Attributes['href'] = $cpLinkBtn_item["cpLinkBtnLink_URL"];
						$cpLinkBtn_item["cpLinkBtnLink_AttributesHtml"] = join(' ', array_map(function ($key) use ($cpLinkBtn_itemcpLinkBtnLink_Attributes) {
							return $key . '="' . $cpLinkBtn_itemcpLinkBtnLink_Attributes[$key] . '"';
						}, array_keys($cpLinkBtn_itemcpLinkBtnLink_Attributes)));
						echo sprintf('<a class="%s e-box" %s><span class="e-box_in"><strong class="e-name">%s</strong></span></a>', $class,$cpLinkBtn_item["cpLinkBtnLink_AttributesHtml"], $cpLinkBtn_item["cpLinkBtnLink_Title"]); ?>
						<?php } ?>
						</p>
					</div>
					<?php } ?>
				</div>
				<?php } ?>
			</div><!--column-->
		</div><!--columns-->
	</div>

<?php }?>
