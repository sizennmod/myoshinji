<?php namespace Application\Block\ComponentText;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Concrete\Core\Editor\LinkAbstractor;
use Database;
use File;
use Page;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('cpTextImages' => array(), 'cpLinkBtn' => array());
    protected $btExportFileColumns = array('cpTextImg');
    protected $btExportPageColumns = array();
    protected $btExportTables = array('btComponentText', 'btComponentTextCpTextImagesEntries', 'btComponentTextCpLinkBtnEntries');
    protected $btTable = 'btComponentText';
    protected $btInterfaceWidth = 800;
    protected $btInterfaceHeight = 600;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $btDefaultSet = 'common_component';
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("記事+画像");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->cpTextWyg;
        $db = Database::connection();
        $cpTextImages_items = $db->fetchAll('SELECT * FROM btComponentTextCpTextImagesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpTextImages_items as $cpTextImages_item_k => $cpTextImages_item_v) {
            if (isset($cpTextImages_item_v["cpTextImagesAlt"]) && trim($cpTextImages_item_v["cpTextImagesAlt"]) != "") {
                $content[] = $cpTextImages_item_v["cpTextImagesAlt"];
            }
            if (isset($cpTextImages_item_v["cpTextImgCapTitle"]) && trim($cpTextImages_item_v["cpTextImgCapTitle"]) != "") {
                $content[] = $cpTextImages_item_v["cpTextImgCapTitle"];
            }
            if (isset($cpTextImages_item_v["cpTextImgCapText"]) && trim($cpTextImages_item_v["cpTextImgCapText"]) != "") {
                $content[] = $cpTextImages_item_v["cpTextImgCapText"];
            }
        }
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $cpTextTitleype_options = array(
            '1' => "赤",
            '2' => "黒"
        );
        $this->set("cpTextTitleype_options", $cpTextTitleype_options);
        $cpTextImgPosition_options = array(
            '' => "-- " . t("None") . " --",
            '1' => "左",
            '2' => "中央",
            '3' => "右",
            '4' => "中央（画像上）"
        );
        $this->set("cpTextImgPosition_options", $cpTextImgPosition_options);
        $this->set('cpTextWyg', LinkAbstractor::translateFrom($this->cpTextWyg));
        $cpTextImages = array();
        $cpTextImages_items = $db->fetchAll('SELECT * FROM btComponentTextCpTextImagesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpTextImages_items as $cpTextImages_item_k => &$cpTextImages_item_v) {
            if (isset($cpTextImages_item_v['cpTextImg']) && trim($cpTextImages_item_v['cpTextImg']) != "" && ($f = File::getByID($cpTextImages_item_v['cpTextImg'])) && is_object($f)) {
                $cpTextImages_item_v['cpTextImg'] = $f;
            } else {
                $cpTextImages_item_v['cpTextImg'] = false;
            }
            $cpTextImages_item_v["cpTextImgLink_Object"] = null;
			$cpTextImages_item_v["cpTextImgLink_Title"] = trim($cpTextImages_item_v["cpTextImgLink_Title"]);
			if (isset($cpTextImages_item_v["cpTextImgLink"]) && trim($cpTextImages_item_v["cpTextImgLink"]) != '') {
				switch ($cpTextImages_item_v["cpTextImgLink"]) {
					case 'page':
						if ($cpTextImages_item_v["cpTextImgLink_Page"] > 0 && ($cpTextImages_item_v["cpTextImgLink_Page_c"] = Page::getByID($cpTextImages_item_v["cpTextImgLink_Page"])) && !$cpTextImages_item_v["cpTextImgLink_Page_c"]->error && !$cpTextImages_item_v["cpTextImgLink_Page_c"]->isInTrash()) {
							$cpTextImages_item_v["cpTextImgLink_Object"] = $cpTextImages_item_v["cpTextImgLink_Page_c"];
							$cpTextImages_item_v["cpTextImgLink_URL"] = $cpTextImages_item_v["cpTextImgLink_Page_c"]->getCollectionLink();
							if ($cpTextImages_item_v["cpTextImgLink_Title"] == '') {
								$cpTextImages_item_v["cpTextImgLink_Title"] = $cpTextImages_item_v["cpTextImgLink_Page_c"]->getCollectionName();
							}
						}
						break;
				    case 'url':
						if ($cpTextImages_item_v["cpTextImgLink_Title"] == '') {
							$cpTextImages_item_v["cpTextImgLink_Title"] = $cpTextImages_item_v["cpTextImgLink_URL"];
						}
						break;
				}
			}
        }
        $this->set('cpTextImages_items', $cpTextImages_items);
        $this->set('cpTextImages', $cpTextImages);
        $cpLinkBtn = array();
        $cpLinkBtn["cpLinkBtnColor_options"] = array(
            '' => "-- " . t("None") . " --",
            '1' => "紺",
            '2' => "赤"
        );
        $cpLinkBtn_items = $db->fetchAll('SELECT * FROM btComponentTextCpLinkBtnEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpLinkBtn_items as $cpLinkBtn_item_k => &$cpLinkBtn_item_v) {
            $cpLinkBtn_item_v["cpLinkBtnLink_Object"] = null;
            $cpLinkBtn_item_v["cpLinkBtnLink_Title"] = trim($cpLinkBtn_item_v["cpLinkBtnLink_Title"]);
            if (isset($cpLinkBtn_item_v["cpLinkBtnLink"]) && trim($cpLinkBtn_item_v["cpLinkBtnLink"]) != '') {
                switch ($cpLinkBtn_item_v["cpLinkBtnLink"]) {
                    case 'page':
                        if ($cpLinkBtn_item_v["cpLinkBtnLink_Page"] > 0 && ($cpLinkBtn_item_v["cpLinkBtnLink_Page_c"] = Page::getByID($cpLinkBtn_item_v["cpLinkBtnLink_Page"])) && !$cpLinkBtn_item_v["cpLinkBtnLink_Page_c"]->error && !$cpLinkBtn_item_v["cpLinkBtnLink_Page_c"]->isInTrash()) {
                            $cpLinkBtn_item_v["cpLinkBtnLink_Object"] = $cpLinkBtn_item_v["cpLinkBtnLink_Page_c"];
                            $cpLinkBtn_item_v["cpLinkBtnLink_URL"] = $cpLinkBtn_item_v["cpLinkBtnLink_Page_c"]->getCollectionLink();
                            if ($cpLinkBtn_item_v["cpLinkBtnLink_Title"] == '') {
                                $cpLinkBtn_item_v["cpLinkBtnLink_Title"] = $cpLinkBtn_item_v["cpLinkBtnLink_Page_c"]->getCollectionName();
                            }
                        }
                        break;
                    case 'file':
                        $cpLinkBtn_item_v["cpLinkBtnLink_File_id"] = (int)$cpLinkBtn_item_v["cpLinkBtnLink_File"];
                        if ($cpLinkBtn_item_v["cpLinkBtnLink_File_id"] > 0 && ($cpLinkBtn_item_v["cpLinkBtnLink_File_object"] = File::getByID($cpLinkBtn_item_v["cpLinkBtnLink_File_id"])) && is_object($cpLinkBtn_item_v["cpLinkBtnLink_File_object"])) {
                            $fp = new \Permissions($cpLinkBtn_item_v["cpLinkBtnLink_File_object"]);
                            if ($fp->canViewFile()) {
                                $cpLinkBtn_item_v["cpLinkBtnLink_Object"] = $cpLinkBtn_item_v["cpLinkBtnLink_File_object"];
                                $cpLinkBtn_item_v["cpLinkBtnLink_URL"] = $cpLinkBtn_item_v["cpLinkBtnLink_File_object"]->getRelativePath();
                                if ($cpLinkBtn_item_v["cpLinkBtnLink_Title"] == '') {
                                    $cpLinkBtn_item_v["cpLinkBtnLink_Title"] = $cpLinkBtn_item_v["cpLinkBtnLink_File_object"]->getTitle();
                                }
                            }
                        }
                        break;
                    case 'url':
                        if ($cpLinkBtn_item_v["cpLinkBtnLink_Title"] == '') {
                            $cpLinkBtn_item_v["cpLinkBtnLink_Title"] = $cpLinkBtn_item_v["cpLinkBtnLink_URL"];
                        }
                        break;
                    case 'relative_url':
                        if ($cpLinkBtn_item_v["cpLinkBtnLink_Title"] == '') {
                            $cpLinkBtn_item_v["cpLinkBtnLink_Title"] = $cpLinkBtn_item_v["cpLinkBtnLink_Relative_URL"];
                        }
                        $cpLinkBtn_item_v["cpLinkBtnLink_URL"] = $cpLinkBtn_item_v["cpLinkBtnLink_Relative_URL"];
                        break;
                    case 'image':
                        if ($cpLinkBtn_item_v["cpLinkBtnLink_Image"] > 0 && ($cpLinkBtn_item_v["cpLinkBtnLink_Image_object"] = File::getByID($cpLinkBtn_item_v["cpLinkBtnLink_Image"])) && is_object($cpLinkBtn_item_v["cpLinkBtnLink_Image_object"])) {
                            $cpLinkBtn_item_v["cpLinkBtnLink_URL"] = $cpLinkBtn_item_v["cpLinkBtnLink_Image_object"]->getURL();
                            $cpLinkBtn_item_v["cpLinkBtnLink_Object"] = $cpLinkBtn_item_v["cpLinkBtnLink_Image_object"];
                            if ($cpLinkBtn_item_v["cpLinkBtnLink_Title"] == '') {
                                $cpLinkBtn_item_v["cpLinkBtnLink_Title"] = $cpLinkBtn_item_v["cpLinkBtnLink_Image_object"]->getTitle();
                            }
                        }
                        break;
                }
            }
        }
        $this->set('cpLinkBtn_items', $cpLinkBtn_items);
        $this->set('cpLinkBtn', $cpLinkBtn);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btComponentTextCpTextImagesEntries', array('bID' => $this->bID));
         $db->delete('btComponentTextCpLinkBtnEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $cpTextImages_items = $db->fetchAll('SELECT * FROM btComponentTextCpTextImagesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpTextImages_items as $cpTextImages_item) {
            unset($cpTextImages_item['id']);
            $cpTextImages_item['bID'] = $newBID;
            $db->insert('btComponentTextCpTextImagesEntries', $cpTextImages_item);
        }
        $cpLinkBtn_items = $db->fetchAll('SELECT * FROM btComponentTextCpLinkBtnEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpLinkBtn_items as $cpLinkBtn_item) {
            unset($cpLinkBtn_item['id']);
            $cpLinkBtn_item['bID'] = $newBID;
            $db->insert('btComponentTextCpLinkBtnEntries', $cpLinkBtn_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $cpTextImages = $this->get('cpTextImages');
        $this->set('cpTextImages_items', array());
        $this->set('cpTextImages', $cpTextImages);
        $cpLinkBtn = $this->get('cpLinkBtn');
        $this->set('cpLinkBtn_items', array());
        $this->set('cpLinkBtn', $cpLinkBtn);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        
        $this->set('cpTextWyg', LinkAbstractor::translateFromEditMode($this->cpTextWyg));
        $cpTextImages = $this->get('cpTextImages');
        $cpTextImages_items = $db->fetchAll('SELECT * FROM btComponentTextCpTextImagesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpTextImages_items as &$cpTextImages_item) {
            if (!File::getByID($cpTextImages_item['cpTextImg'])) {
                unset($cpTextImages_item['cpTextImg']);
            }
        }
        $this->set('cpTextImages', $cpTextImages);
        $this->set('cpTextImages_items', $cpTextImages_items);
        $cpLinkBtn = $this->get('cpLinkBtn');
        $cpLinkBtn_items = $db->fetchAll('SELECT * FROM btComponentTextCpLinkBtnEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $this->set('cpLinkBtn', $cpLinkBtn);
        $this->set('cpLinkBtn_items', $cpLinkBtn_items);
    }

    protected function addEdit()
    {
        $this->set("cpTextTitleype_options", array(
                '1' => "赤",
                '2' => "黒"
            )
        );
        $this->set("cpTextImgPosition_options", array(
                '' => "-- " . t("None") . " --",
                '1' => "左",
                '2' => "中央",
                '3' => "右",
                '4' => "中央（画像上）"
            )
        );
        $cpTextImages = array();
        $this->set("cpTextImgLink_Options", $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'url',
), true));
        $this->set('cpTextImages', $cpTextImages);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $cpLinkBtn = array();
        $cpLinkBtn['cpLinkBtnColor_options'] = array(
            '' => "-- " . t("None") . " --",
            '1' => "紺",
            '2' => "赤"
        );
        $this->set("cpLinkBtnLink_Options", $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
), true));
        $this->set('cpLinkBtn', $cpLinkBtn);
        $al = AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/component_text/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/component_text/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/component_text/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('redactor');
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $db = Database::connection();
        if (!isset($args["cpTextAddEmph"]) || trim($args["cpTextAddEmph"]) == "" || !in_array($args["cpTextAddEmph"], array(0, 1))) {
            $args["cpTextAddEmph"] = '';
        }
        $args['cpTextWyg'] = LinkAbstractor::translateTo($args['cpTextWyg']);
        $rows = $db->fetchAll('SELECT * FROM btComponentTextCpTextImagesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $cpTextImages_items = isset($args['cpTextImages']) && is_array($args['cpTextImages']) ? $args['cpTextImages'] : array();
        $queries = array();
        if (!empty($cpTextImages_items)) {
            $i = 0;
            foreach ($cpTextImages_items as $cpTextImages_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($cpTextImages_item['cpTextImg']) && trim($cpTextImages_item['cpTextImg']) != '') {
                    $data['cpTextImg'] = trim($cpTextImages_item['cpTextImg']);
                } else {
                    $data['cpTextImg'] = null;
                }
                if (isset($cpTextImages_item['cpTextImgLink']) && trim($cpTextImages_item['cpTextImgLink']) != '') {
					$data['cpTextImgLink_Title'] = $cpTextImages_item['cpTextImgLink_Title'];
					$data['cpTextImgLink'] = $cpTextImages_item['cpTextImgLink'];
					switch ($cpTextImages_item['cpTextImgLink']) {
						case 'page':
							$data['cpTextImgLink_Page'] = $cpTextImages_item['cpTextImgLink_Page'];
							$data['cpTextImgLink_URL'] = '';
							break;
                        case 'url':
							$data['cpTextImgLink_URL'] = $cpTextImages_item['cpTextImgLink_URL'];
							$data['cpTextImgLink_Page'] = '0';
							break;
                        default:
							$data['cpTextImgLink'] = '';
							$data['cpTextImgLink_Page'] = '0';
							$data['cpTextImgLink_URL'] = '';
							break;	
					}
				}
				else {
					$data['cpTextImgLink'] = '';
					$data['cpTextImgLink_Title'] = '';
					$data['cpTextImgLink_Page'] = '0';
					$data['cpTextImgLink_URL'] = '';
				}
                if (isset($cpTextImages_item['cpTextImagesAlt']) && trim($cpTextImages_item['cpTextImagesAlt']) != '') {
                    $data['cpTextImagesAlt'] = trim($cpTextImages_item['cpTextImagesAlt']);
                } else {
                    $data['cpTextImagesAlt'] = null;
                }
                if (isset($cpTextImages_item['cpTextImgCapTitle']) && trim($cpTextImages_item['cpTextImgCapTitle']) != '') {
                    $data['cpTextImgCapTitle'] = trim($cpTextImages_item['cpTextImgCapTitle']);
                } else {
                    $data['cpTextImgCapTitle'] = null;
                }
                if (isset($cpTextImages_item['cpTextImgCapText']) && trim($cpTextImages_item['cpTextImgCapText']) != '') {
                    $data['cpTextImgCapText'] = trim($cpTextImages_item['cpTextImgCapText']);
                } else {
                    $data['cpTextImgCapText'] = null;
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btComponentTextCpTextImagesEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btComponentTextCpTextImagesEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btComponentTextCpTextImagesEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        $rows = $db->fetchAll('SELECT * FROM btComponentTextCpLinkBtnEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $cpLinkBtn_items = isset($args['cpLinkBtn']) && is_array($args['cpLinkBtn']) ? $args['cpLinkBtn'] : array();
        $queries = array();
        if (!empty($cpLinkBtn_items)) {
            $i = 0;
            foreach ($cpLinkBtn_items as $cpLinkBtn_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($cpLinkBtn_item['cpLinkBtnColor']) && trim($cpLinkBtn_item['cpLinkBtnColor']) != '') {
                    $data['cpLinkBtnColor'] = trim($cpLinkBtn_item['cpLinkBtnColor']);
                } else {
                    $data['cpLinkBtnColor'] = null;
                }
                if (isset($cpLinkBtn_item['cpLinkBtnLink']) && trim($cpLinkBtn_item['cpLinkBtnLink']) != '') {
                    $data['cpLinkBtnLink_Title'] = $cpLinkBtn_item['cpLinkBtnLink_Title'];
                    $data['cpLinkBtnLink'] = $cpLinkBtn_item['cpLinkBtnLink'];
                    switch ($cpLinkBtn_item['cpLinkBtnLink']) {
                        case 'page':
                            $data['cpLinkBtnLink_Page'] = $cpLinkBtn_item['cpLinkBtnLink_Page'];
                            $data['cpLinkBtnLink_File'] = '0';
                            $data['cpLinkBtnLink_URL'] = '';
                            $data['cpLinkBtnLink_Relative_URL'] = '';
                            $data['cpLinkBtnLink_Image'] = '0';
                            break;
                        case 'file':
                            $data['cpLinkBtnLink_File'] = $cpLinkBtn_item['cpLinkBtnLink_File'];
                            $data['cpLinkBtnLink_Page'] = '0';
                            $data['cpLinkBtnLink_URL'] = '';
                            $data['cpLinkBtnLink_Relative_URL'] = '';
                            $data['cpLinkBtnLink_Image'] = '0';
                            break;
                        case 'url':
                            $data['cpLinkBtnLink_URL'] = $cpLinkBtn_item['cpLinkBtnLink_URL'];
                            $data['cpLinkBtnLink_Page'] = '0';
                            $data['cpLinkBtnLink_File'] = '0';
                            $data['cpLinkBtnLink_Relative_URL'] = '';
                            $data['cpLinkBtnLink_Image'] = '0';
                            break;
                        case 'relative_url':
                            $data['cpLinkBtnLink_Relative_URL'] = $cpLinkBtn_item['cpLinkBtnLink_Relative_URL'];
                            $data['cpLinkBtnLink_Page'] = '0';
                            $data['cpLinkBtnLink_File'] = '0';
                            $data['cpLinkBtnLink_URL'] = '';
                            $data['cpLinkBtnLink_Image'] = '0';
                            break;
                        case 'image':
                            $data['cpLinkBtnLink_Image'] = $cpLinkBtn_item['cpLinkBtnLink_Image'];
                            $data['cpLinkBtnLink_Page'] = '0';
                            $data['cpLinkBtnLink_File'] = '0';
                            $data['cpLinkBtnLink_URL'] = '';
                            $data['cpLinkBtnLink_Relative_URL'] = '';
                            break;
                        default:
                            $data['cpLinkBtnLink'] = '';
                            $data['cpLinkBtnLink_Page'] = '0';
                            $data['cpLinkBtnLink_File'] = '0';
                            $data['cpLinkBtnLink_URL'] = '';
                            $data['cpLinkBtnLink_Relative_URL'] = '';
                            $data['cpLinkBtnLink_Image'] = '0';
                            break;  
                    }
                }
                else {
                    $data['cpLinkBtnLink'] = '';
                    $data['cpLinkBtnLink_Title'] = '';
                    $data['cpLinkBtnLink_Page'] = '0';
                    $data['cpLinkBtnLink_File'] = '0';
                    $data['cpLinkBtnLink_URL'] = '';
                    $data['cpLinkBtnLink_Relative_URL'] = '';
                    $data['cpLinkBtnLink_Image'] = '0';
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btComponentTextCpLinkBtnEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btComponentTextCpLinkBtnEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btComponentTextCpLinkBtnEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if ((in_array("cpTextTitleype", $this->btFieldsRequired) && (!isset($args["cpTextTitleype"]) || trim($args["cpTextTitleype"]) == "")) || (isset($args["cpTextTitleype"]) && trim($args["cpTextTitleype"]) != "" && !in_array($args["cpTextTitleype"], array("1", "2", "3", "4")))) {
            $e->add(t("The %s field has an invalid value.", t("タイトルタイプ")));
        }
        if (in_array("cpTextTitle", $this->btFieldsRequired) && trim($args["cpTextTitle"]) == "") {
            $e->add(t("The %s field is required.", t("タイトル")));
        }
        if (in_array("cpTextAddEmph", $this->btFieldsRequired) && (trim($args["cpTextAddEmph"]) == "" || !in_array($args["cpTextAddEmph"], array(0, 1)))) {
            $e->add(t("The %s field is required.", t("エリアを強調表示にする")));
        }
        if ((in_array("cpTextImgPosition", $this->btFieldsRequired) && (!isset($args["cpTextImgPosition"]) || trim($args["cpTextImgPosition"]) == "")) || (isset($args["cpTextImgPosition"]) && trim($args["cpTextImgPosition"]) != "" && !in_array($args["cpTextImgPosition"], array("1", "2", "3" , "4")))) {
            $e->add(t("The %s field has an invalid value.", t("画像表示位置")));
        }
        if (in_array("cpTextWyg", $this->btFieldsRequired) && (trim($args["cpTextWyg"]) == "")) {
            $e->add(t("The %s field is required.", t("本文")));
        }
        $cpTextImagesEntriesMin = 0;
        $cpTextImagesEntriesMax = 0;
        $cpTextImagesEntriesErrors = 0;
        $cpTextImages = array();
        if (isset($args['cpTextImages']) && is_array($args['cpTextImages']) && !empty($args['cpTextImages'])) {
            if ($cpTextImagesEntriesMin >= 1 && count($args['cpTextImages']) < $cpTextImagesEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("画像データ"), $cpTextImagesEntriesMin, count($args['cpTextImages'])));
                $cpTextImagesEntriesErrors++;
            }
            if ($cpTextImagesEntriesMax >= 1 && count($args['cpTextImages']) > $cpTextImagesEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("画像データ"), $cpTextImagesEntriesMax, count($args['cpTextImages'])));
                $cpTextImagesEntriesErrors++;
            }
            if ($cpTextImagesEntriesErrors == 0) {
                foreach ($args['cpTextImages'] as $cpTextImages_k => $cpTextImages_v) {
                    if (is_array($cpTextImages_v)) {
                        if (in_array("cpTextImg", $this->btFieldsRequired['cpTextImages']) && (!isset($cpTextImages_v['cpTextImg']) || trim($cpTextImages_v['cpTextImg']) == "" || !is_object(File::getByID($cpTextImages_v['cpTextImg'])))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("画像"), t("画像データ"), $cpTextImages_k));
                        }
                        if ((in_array("cpTextImgLink", $this->btFieldsRequired['cpTextImages']) && (!isset($cpTextImages_v['cpTextImgLink']) || trim($cpTextImages_v['cpTextImgLink']) == "")) || (isset($cpTextImages_v['cpTextImgLink']) && trim($cpTextImages_v['cpTextImgLink']) != "" && !array_key_exists($cpTextImages_v['cpTextImgLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'url',
))))) {
							$e->add(t("The %s field has an invalid value.", t("画像リンク")));
						} elseif (array_key_exists($cpTextImages_v['cpTextImgLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'url',
)))) {
							switch ($cpTextImages_v['cpTextImgLink']) {
								case 'page':
									if (!isset($cpTextImages_v['cpTextImgLink_Page']) || trim($cpTextImages_v['cpTextImgLink_Page']) == "" || $cpTextImages_v['cpTextImgLink_Page'] == "0" || (($page = Page::getByID($cpTextImages_v['cpTextImgLink_Page'])) && $page->error !== false)) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Page"), t("画像リンク"), t("画像データ"), $cpTextImages_k));
									}
									break;
				                case 'url':
									if (!isset($cpTextImages_v['cpTextImgLink_URL']) || trim($cpTextImages_v['cpTextImgLink_URL']) == "" || !filter_var($cpTextImages_v['cpTextImgLink_URL'], FILTER_VALIDATE_URL)) {
										$e->add(t("The %s field for '%s' does not have a valid URL (%s, row #%s).", t("URL"), t("画像リンク"), t("画像データ"), $cpTextImages_k));
									}
									break;	
							}
						}
                        if (in_array("cpTextImagesAlt", $this->btFieldsRequired['cpTextImages']) && (!isset($cpTextImages_v['cpTextImagesAlt']) || trim($cpTextImages_v['cpTextImagesAlt']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("画像alt"), t("画像データ"), $cpTextImages_k));
                        }
                        if (in_array("cpTextImgCapTitle", $this->btFieldsRequired['cpTextImages']) && (!isset($cpTextImages_v['cpTextImgCapTitle']) || trim($cpTextImages_v['cpTextImgCapTitle']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("キャプションタイトル"), t("画像データ"), $cpTextImages_k));
                        }
                        if (in_array("cpTextImgCapText", $this->btFieldsRequired['cpTextImages']) && (!isset($cpTextImages_v['cpTextImgCapText']) || trim($cpTextImages_v['cpTextImgCapText']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("キャプションテキスト"), t("画像データ"), $cpTextImages_k));
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('画像データ'), $cpTextImages_k));
                    }
                }
            }
        } else {
            if ($cpTextImagesEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("画像データ"), $cpTextImagesEntriesMin));
            }
        }
        $cpLinkBtnEntriesMin = 0;
        $cpLinkBtnEntriesMax = 2;
        $cpLinkBtnEntriesErrors = 0;
        $cpLinkBtn = array();
        if (isset($args['cpLinkBtn']) && is_array($args['cpLinkBtn']) && !empty($args['cpLinkBtn'])) {
            if ($cpLinkBtnEntriesMin >= 1 && count($args['cpLinkBtn']) < $cpLinkBtnEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("リンクボタン"), $cpLinkBtnEntriesMin, count($args['cpLinkBtn'])));
                $cpLinkBtnEntriesErrors++;
            }
            if ($cpLinkBtnEntriesMax >= 1 && count($args['cpLinkBtn']) > $cpLinkBtnEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("リンクボタン"), $cpLinkBtnEntriesMax, count($args['cpLinkBtn'])));
                $cpLinkBtnEntriesErrors++;
            }
            if ($cpLinkBtnEntriesErrors == 0) {
                foreach ($args['cpLinkBtn'] as $cpLinkBtn_k => $cpLinkBtn_v) {
                    if (is_array($cpLinkBtn_v)) {
                        if ((in_array("cpLinkBtnColor", $this->btFieldsRequired['cpLinkBtn']) && (!isset($cpLinkBtn_v['cpLinkBtnColor']) || trim($cpLinkBtn_v['cpLinkBtnColor']) == "")) || (isset($cpLinkBtn_v['cpLinkBtnColor']) && trim($cpLinkBtn_v['cpLinkBtnColor']) != "" && !in_array($cpLinkBtn_v['cpLinkBtnColor'], array("1", "2")))) {
                            $e->add(t("The %s field has an invalid value (%s, row #%s).", t("色"), t("リンクボタン"), $cpLinkBtn_k));
                        }
                        if ((in_array("cpLinkBtnLink", $this->btFieldsRequired['cpLinkBtn']) && (!isset($cpLinkBtn_v['cpLinkBtnLink']) || trim($cpLinkBtn_v['cpLinkBtnLink']) == "")) || (isset($cpLinkBtn_v['cpLinkBtnLink']) && trim($cpLinkBtn_v['cpLinkBtnLink']) != "" && !array_key_exists($cpLinkBtn_v['cpLinkBtnLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
))))) {
                            $e->add(t("The %s field has an invalid value.", t("リンク")));
                        } elseif (array_key_exists($cpLinkBtn_v['cpLinkBtnLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
)))) {
                            switch ($cpLinkBtn_v['cpLinkBtnLink']) {
                                case 'page':
                                    if (!isset($cpLinkBtn_v['cpLinkBtnLink_Page']) || trim($cpLinkBtn_v['cpLinkBtnLink_Page']) == "" || $cpLinkBtn_v['cpLinkBtnLink_Page'] == "0" || (($page = Page::getByID($cpLinkBtn_v['cpLinkBtnLink_Page'])) && $page->error !== false)) {
                                        $e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Page"), t("リンク"), t("リンクボタン"), $cpLinkBtn_k));
                                    }
                                    break;
                                case 'file':
                                    if (!isset($cpLinkBtn_v['cpLinkBtnLink_File']) || trim($cpLinkBtn_v['cpLinkBtnLink_File']) == "" || !is_object(File::getByID($cpLinkBtn_v['cpLinkBtnLink_File']))) {
                                        $e->add(t("The %s field for '%s' is required (%s, row #%s).", t("File"), t("リンク"), t("リンクボタン"), $cpLinkBtn_k));
                                    }
                                    break;
                                case 'url':
                                    if (!isset($cpLinkBtn_v['cpLinkBtnLink_URL']) || trim($cpLinkBtn_v['cpLinkBtnLink_URL']) == "" || !filter_var($cpLinkBtn_v['cpLinkBtnLink_URL'], FILTER_VALIDATE_URL)) {
                                        $e->add(t("The %s field for '%s' does not have a valid URL (%s, row #%s).", t("URL"), t("リンク"), t("リンクボタン"), $cpLinkBtn_k));
                                    }
                                    break;
                                case 'relative_url':
                                    if (!isset($cpLinkBtn_v['cpLinkBtnLink_Relative_URL']) || trim($cpLinkBtn_v['cpLinkBtnLink_Relative_URL']) == "") {
                                        $e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Relative URL"), t("リンク"), t("リンクボタン"), $cpLinkBtn_k));
                                    }
                                    break;
                                case 'image':
                                    if (!isset($cpLinkBtn_v['cpLinkBtnLink_Image']) || trim($cpLinkBtn_v['cpLinkBtnLink_Image']) == "" || !is_object(File::getByID($cpLinkBtn_v['cpLinkBtnLink_Image']))) {
                                        $e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Image"), t("リンク"), t("リンクボタン"), $cpLinkBtn_k));
                                    }
                                    break;  
                            }
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('リンクボタン'), $cpLinkBtn_k));
                    }
                }
            }
        } else {
            if ($cpLinkBtnEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("リンクボタン"), $cpLinkBtnEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-component_text', 'blocks/component_text/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-component_text');
        $this->edit();
    }

        protected function getSmartLinkTypeOptions($include = array(), $checkNone = false)
	{
		$options = array(
			''             => sprintf("-- %s--", t("None")),
			'page'         => t("Page"),
			'url'          => t("External URL"),
			'relative_url' => t("Relative URL"),
			'file'         => t("File"),
			'image'        => t("Image")
		);
		if ($checkNone) {
            $include = array_merge(array(''), $include);
        }
		$return = array();
		foreach($include as $v){
		    if(isset($options[$v])){
		        $return[$v] = $options[$v];
		    }
		}
		return $return;
	}
}