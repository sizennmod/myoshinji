<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
<?php $repeatable_container_id = 'btComponentBnr-cpBnrLoop-container-' . $identifier_getString; ?>
    <div id="<?php echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php echo t('画像を追加'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $cpBnrLoop_items,
                        'order' => array_keys($cpBnrLoop_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php echo t('画像を追加'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php echo t('バナー') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrImg]" class="control-label"><?php echo t("画像"); ?></label>
    <?php echo isset($btFieldsRequired['cpBnrLoop']) && in_array('cpBnrImg', $btFieldsRequired['cpBnrLoop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-file-selector-input-name="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrImg]" class="ccm-file-selector ft-image-cpBnrImg-file-selector" data-file-selector-f-id="{{ cpBnrImg }}"></div>
</div>            <?php $cpBnrLink_ContainerID = 'btComponentBnr-cpBnrLink-container-' . $identifier_getString; ?>
<div class="ft-smart-link" id="<?php echo $cpBnrLink_ContainerID; ?>">
	<div class="form-group">
		<label for="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink]" class="control-label"><?php echo t("リンク"); ?></label>
	    <?php echo isset($btFieldsRequired['cpBnrLoop']) && in_array('cpBnrLink', $btFieldsRequired['cpBnrLoop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
	    <?php $cpBnrLoopCpBnrLink_options = $cpBnrLink_Options; ?>
                    <select name="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink]" id="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink]" class="form-control ft-smart-link-type">{{#select cpBnrLink}}<?php foreach ($cpBnrLoopCpBnrLink_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
	</div>
	
	<div class="form-group">
		<div class="ft-smart-link-options hidden" style="padding-left: 10px;">
			<div class="form-group">
				<label for="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_Title]" class="control-label"><?php echo t("Title"); ?></label>
			    <input name="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_Title]" id="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_Title]" class="form-control" type="text" value="{{ cpBnrLink_Title }}" />		
			</div>
			
			<div class="form-group hidden" data-link-type="page">
			<label for="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_Page]" class="control-label"><?php echo t("Page"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-page-selector="{{token}}" data-input-name="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_Page]" data-cID="{{cpBnrLink_Page}}"></div>
		</div>

		<div class="form-group hidden" data-link-type="url">
			<label for="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_URL]" id="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_URL]" class="form-control" type="text" value="{{ cpBnrLink_URL }}" />
		</div>

		<div class="form-group hidden" data-link-type="relative_url">
			<label for="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_Relative_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_Relative_URL]" id="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_Relative_URL]" class="form-control" type="text" value="{{ cpBnrLink_Relative_URL }}" />
		</div>

		<div class="form-group hidden" data-link-type="file">
		    <label for="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_File]" class="control-label"><?php echo t("File"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_File]" class="ccm-file-selector" data-file-selector-f-id="{{ cpBnrLink_File }}"></div>	
		</div>

		<div class="form-group hidden" data-link-type="image">
			<label for="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_Image]" class="control-label"><?php echo t("Image"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrLink_Image]" class="ccm-file-selector" data-file-selector-f-id="{{ cpBnrLink_Image }}"></div>
		</div>
		</div>
	</div>
</div>
            <div class="form-group">
    <label for="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrPopupCheck]" class="control-label"><?php echo t("画像をポップアップで表示"); ?></label>
    <?php echo isset($btFieldsRequired) && in_array('cpBnrPopupCheck', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php $cpBnrLoopCpBnrPopupCheck_options = array(0 => t("No"), 1 => t("Yes")); ?>
                    <select name="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrPopupCheck]" id="<?php echo $view->field('cpBnrLoop'); ?>[{{id}}][cpBnrPopupCheck]" class="form-control">{{#select cpBnrPopupCheck}}<?php foreach ($cpBnrLoopCpBnrPopupCheck_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
</div></div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btComponentBnr.cpBnrLoop.edit.open', {id: '<?php echo $repeatable_container_id; ?>'});
    $.each($('#<?php echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>