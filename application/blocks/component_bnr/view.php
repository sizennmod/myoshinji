<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if (!empty($cpBnrLoop_items)) { 
switch (count($cpBnrLoop_items)) {
	case '1':
		$class = 'c-bnrBlock_items c-bnrBlock_items--column_1';
		break;
	case '2':
		$class = 'c-bnrBlock_items c-bnrBlock_items--column_2';
		break;
	case '3':
		$class = 'c-bnrBlock_items c-bnrBlock_items--column_3';
		break;
	case '4':
		$class = 'c-bnrBlock_items c-bnrBlock_items--column_4';
		break;
	default:
		$class = 'c-bnrBlock_items c-bnrBlock_items--column_1';
		break;
}

$popup_wrap = '';
foreach ($cpBnrLoop_items as $cpBnrLoop_item_key => $cpBnrLoop_item) {
	if (isset($cpBnrLoop_item["cpBnrPopupCheck"]) && trim($cpBnrLoop_item["cpBnrPopupCheck"]) != "") { 
		if($cpBnrLoop_item["cpBnrPopupCheck"] == 1){
			$popup_wrap = ' js-popup';
			break;
		}
	}
}
$class .= $popup_wrap;
?>
<div class="c-bnrBlock c-section">
	<div class="<?= $class;?>">
	<?php foreach ($cpBnrLoop_items as $cpBnrLoop_item_key => $cpBnrLoop_item) { ?>
		<?php if ($cpBnrLoop_item["cpBnrImg"] && trim($cpBnrLoop_item["cpBnrLink_URL"]) != "") { ?>
			<?php 
			$popup = '';
			if (isset($cpBnrLoop_item["cpBnrPopupCheck"]) && trim($cpBnrLoop_item["cpBnrPopupCheck"]) != "" && $cpBnrLoop_item["cpBnrLink"] == 'image' && $cpBnrLoop_item["cpBnrPopupCheck"] == 1) { 
				$popup = ' js-popup_item';
			}
			
			$cpBnrLoop_itemcpBnrLink_Attributes = array();
			$cpBnrLoop_itemcpBnrLink_Attributes['href'] = $cpBnrLoop_item["cpBnrLink_URL"];
			$cpBnrLoop_item["cpBnrLink_AttributesHtml"] = join(' ', array_map(function ($key) use ($cpBnrLoop_itemcpBnrLink_Attributes) {
				return $key . '="' . $cpBnrLoop_itemcpBnrLink_Attributes[$key] . '"';
			}, array_keys($cpBnrLoop_itemcpBnrLink_Attributes)));
			?>
			<p class="c-bnrBlock_item<?= $popup;?>">
				<a <?= $cpBnrLoop_item["cpBnrLink_AttributesHtml"];?>><span class="e-figure"><img src="<?php echo $cpBnrLoop_item["cpBnrImg"]->getURL(); ?>" alt="<?= $cpBnrLoop_item["cpBnrLink_Title"];?>"/></span></a>
			</p>
		<?php } ?>
	<?php } ?>
	</div>
</div>
<?php } ?>