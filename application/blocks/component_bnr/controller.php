<?php namespace Application\Block\ComponentBnr;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Database;
use File;
use Page;
use Permissions;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('cpBnrLoop' => array());
    protected $btExportFileColumns = array('cpBnrImg');
    protected $btExportPageColumns = array();
    protected $btExportTables = array('btComponentBnr', 'btComponentBnrCpBnrLoopEntries');
    protected $btTable = 'btComponentBnr';
    protected $btInterfaceWidth = 800;
    protected $btInterfaceHeight = 600;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("複数バナー登録");
    }

    public function view()
    {
        $db = Database::connection();
        $cpBnrLoop = array();
        $cpBnrLoop_items = $db->fetchAll('SELECT * FROM btComponentBnrCpBnrLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpBnrLoop_items as $cpBnrLoop_item_k => &$cpBnrLoop_item_v) {
            if (isset($cpBnrLoop_item_v['cpBnrImg']) && trim($cpBnrLoop_item_v['cpBnrImg']) != "" && ($f = File::getByID($cpBnrLoop_item_v['cpBnrImg'])) && is_object($f)) {
                $cpBnrLoop_item_v['cpBnrImg'] = $f;
            } else {
                $cpBnrLoop_item_v['cpBnrImg'] = false;
            }
            $cpBnrLoop_item_v["cpBnrLink_Object"] = null;
			$cpBnrLoop_item_v["cpBnrLink_Title"] = trim($cpBnrLoop_item_v["cpBnrLink_Title"]);
			if (isset($cpBnrLoop_item_v["cpBnrLink"]) && trim($cpBnrLoop_item_v["cpBnrLink"]) != '') {
				switch ($cpBnrLoop_item_v["cpBnrLink"]) {
					case 'page':
						if ($cpBnrLoop_item_v["cpBnrLink_Page"] > 0 && ($cpBnrLoop_item_v["cpBnrLink_Page_c"] = Page::getByID($cpBnrLoop_item_v["cpBnrLink_Page"])) && !$cpBnrLoop_item_v["cpBnrLink_Page_c"]->error && !$cpBnrLoop_item_v["cpBnrLink_Page_c"]->isInTrash()) {
							$cpBnrLoop_item_v["cpBnrLink_Object"] = $cpBnrLoop_item_v["cpBnrLink_Page_c"];
							$cpBnrLoop_item_v["cpBnrLink_URL"] = $cpBnrLoop_item_v["cpBnrLink_Page_c"]->getCollectionLink();
							if ($cpBnrLoop_item_v["cpBnrLink_Title"] == '') {
								$cpBnrLoop_item_v["cpBnrLink_Title"] = $cpBnrLoop_item_v["cpBnrLink_Page_c"]->getCollectionName();
							}
						}
						break;
				    case 'file':
						$cpBnrLoop_item_v["cpBnrLink_File_id"] = (int)$cpBnrLoop_item_v["cpBnrLink_File"];
						if ($cpBnrLoop_item_v["cpBnrLink_File_id"] > 0 && ($cpBnrLoop_item_v["cpBnrLink_File_object"] = File::getByID($cpBnrLoop_item_v["cpBnrLink_File_id"])) && is_object($cpBnrLoop_item_v["cpBnrLink_File_object"])) {
							$fp = new Permissions($cpBnrLoop_item_v["cpBnrLink_File_object"]);
							if ($fp->canViewFile()) {
								$cpBnrLoop_item_v["cpBnrLink_Object"] = $cpBnrLoop_item_v["cpBnrLink_File_object"];
								$cpBnrLoop_item_v["cpBnrLink_URL"] = $cpBnrLoop_item_v["cpBnrLink_File_object"]->getRelativePath();
								if ($cpBnrLoop_item_v["cpBnrLink_Title"] == '') {
									$cpBnrLoop_item_v["cpBnrLink_Title"] = $cpBnrLoop_item_v["cpBnrLink_File_object"]->getTitle();
								}
							}
						}
						break;
				    case 'url':
						if ($cpBnrLoop_item_v["cpBnrLink_Title"] == '') {
							$cpBnrLoop_item_v["cpBnrLink_Title"] = $cpBnrLoop_item_v["cpBnrLink_URL"];
						}
						break;
				    case 'relative_url':
						if ($cpBnrLoop_item_v["cpBnrLink_Title"] == '') {
							$cpBnrLoop_item_v["cpBnrLink_Title"] = $cpBnrLoop_item_v["cpBnrLink_Relative_URL"];
						}
						$cpBnrLoop_item_v["cpBnrLink_URL"] = $cpBnrLoop_item_v["cpBnrLink_Relative_URL"];
						break;
				    case 'image':
						if ($cpBnrLoop_item_v["cpBnrLink_Image"] > 0 && ($cpBnrLoop_item_v["cpBnrLink_Image_object"] = File::getByID($cpBnrLoop_item_v["cpBnrLink_Image"])) && is_object($cpBnrLoop_item_v["cpBnrLink_Image_object"])) {
							$cpBnrLoop_item_v["cpBnrLink_URL"] = $cpBnrLoop_item_v["cpBnrLink_Image_object"]->getURL();
							$cpBnrLoop_item_v["cpBnrLink_Object"] = $cpBnrLoop_item_v["cpBnrLink_Image_object"];
							if ($cpBnrLoop_item_v["cpBnrLink_Title"] == '') {
								$cpBnrLoop_item_v["cpBnrLink_Title"] = $cpBnrLoop_item_v["cpBnrLink_Image_object"]->getTitle();
							}
						}
						break;
				}
			}
        }
        $this->set('cpBnrLoop_items', $cpBnrLoop_items);
        $this->set('cpBnrLoop', $cpBnrLoop);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btComponentBnrCpBnrLoopEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $cpBnrLoop_items = $db->fetchAll('SELECT * FROM btComponentBnrCpBnrLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpBnrLoop_items as $cpBnrLoop_item) {
            unset($cpBnrLoop_item['id']);
            $cpBnrLoop_item['bID'] = $newBID;
            $db->insert('btComponentBnrCpBnrLoopEntries', $cpBnrLoop_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $cpBnrLoop = $this->get('cpBnrLoop');
        $this->set('cpBnrLoop_items', array());
        
        $this->set('cpBnrLoop', $cpBnrLoop);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        $cpBnrLoop = $this->get('cpBnrLoop');
        $cpBnrLoop_items = $db->fetchAll('SELECT * FROM btComponentBnrCpBnrLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($cpBnrLoop_items as &$cpBnrLoop_item) {
            if (!File::getByID($cpBnrLoop_item['cpBnrImg'])) {
                unset($cpBnrLoop_item['cpBnrImg']);
            }
        }
        $this->set('cpBnrLoop', $cpBnrLoop);
        $this->set('cpBnrLoop_items', $cpBnrLoop_items);
    }

    protected function addEdit()
    {
        $cpBnrLoop = array();
        $this->set("cpBnrLink_Options", $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
), true));
        $this->set('cpBnrLoop', $cpBnrLoop);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/component_bnr/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/component_bnr/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/component_bnr/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $db = Database::connection();
        $rows = $db->fetchAll('SELECT * FROM btComponentBnrCpBnrLoopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $cpBnrLoop_items = isset($args['cpBnrLoop']) && is_array($args['cpBnrLoop']) ? $args['cpBnrLoop'] : array();
        $queries = array();
        if (!empty($cpBnrLoop_items)) {
            $i = 0;
            foreach ($cpBnrLoop_items as $cpBnrLoop_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($cpBnrLoop_item['cpBnrImg']) && trim($cpBnrLoop_item['cpBnrImg']) != '') {
                    $data['cpBnrImg'] = trim($cpBnrLoop_item['cpBnrImg']);
                } else {
                    $data['cpBnrImg'] = null;
                }
                if (isset($cpBnrLoop_item['cpBnrLink']) && trim($cpBnrLoop_item['cpBnrLink']) != '') {
					$data['cpBnrLink_Title'] = $cpBnrLoop_item['cpBnrLink_Title'];
					$data['cpBnrLink'] = $cpBnrLoop_item['cpBnrLink'];
					switch ($cpBnrLoop_item['cpBnrLink']) {
						case 'page':
							$data['cpBnrLink_Page'] = $cpBnrLoop_item['cpBnrLink_Page'];
							$data['cpBnrLink_File'] = '0';
							$data['cpBnrLink_URL'] = '';
							$data['cpBnrLink_Relative_URL'] = '';
							$data['cpBnrLink_Image'] = '0';
							break;
                        case 'file':
							$data['cpBnrLink_File'] = $cpBnrLoop_item['cpBnrLink_File'];
							$data['cpBnrLink_Page'] = '0';
							$data['cpBnrLink_URL'] = '';
							$data['cpBnrLink_Relative_URL'] = '';
							$data['cpBnrLink_Image'] = '0';
							break;
                        case 'url':
							$data['cpBnrLink_URL'] = $cpBnrLoop_item['cpBnrLink_URL'];
							$data['cpBnrLink_Page'] = '0';
							$data['cpBnrLink_File'] = '0';
							$data['cpBnrLink_Relative_URL'] = '';
							$data['cpBnrLink_Image'] = '0';
							break;
                        case 'relative_url':
							$data['cpBnrLink_Relative_URL'] = $cpBnrLoop_item['cpBnrLink_Relative_URL'];
							$data['cpBnrLink_Page'] = '0';
							$data['cpBnrLink_File'] = '0';
							$data['cpBnrLink_URL'] = '';
							$data['cpBnrLink_Image'] = '0';
							break;
                        case 'image':
							$data['cpBnrLink_Image'] = $cpBnrLoop_item['cpBnrLink_Image'];
							$data['cpBnrLink_Page'] = '0';
							$data['cpBnrLink_File'] = '0';
							$data['cpBnrLink_URL'] = '';
							$data['cpBnrLink_Relative_URL'] = '';
							break;
                        default:
							$data['cpBnrLink'] = '';
							$data['cpBnrLink_Page'] = '0';
							$data['cpBnrLink_File'] = '0';
							$data['cpBnrLink_URL'] = '';
							$data['cpBnrLink_Relative_URL'] = '';
							$data['cpBnrLink_Image'] = '0';
							break;	
					}
				}
				else {
					$data['cpBnrLink'] = '';
					$data['cpBnrLink_Title'] = '';
					$data['cpBnrLink_Page'] = '0';
					$data['cpBnrLink_File'] = '0';
					$data['cpBnrLink_URL'] = '';
					$data['cpBnrLink_Relative_URL'] = '';
					$data['cpBnrLink_Image'] = '0';
				}
                if (isset($cpBnrLoop_item['cpBnrPopupCheck']) && trim($cpBnrLoop_item['cpBnrPopupCheck']) != '' && in_array($cpBnrLoop_item['cpBnrPopupCheck'], array(0, 1))) {
                    $data['cpBnrPopupCheck'] = $cpBnrLoop_item['cpBnrPopupCheck'];
                } else {
                    $data['cpBnrPopupCheck'] = null;
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btComponentBnrCpBnrLoopEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btComponentBnrCpBnrLoopEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btComponentBnrCpBnrLoopEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        $cpBnrLoopEntriesMin = 1;
        $cpBnrLoopEntriesMax = 4;
        $cpBnrLoopEntriesErrors = 0;
        $cpBnrLoop = array();
        if (isset($args['cpBnrLoop']) && is_array($args['cpBnrLoop']) && !empty($args['cpBnrLoop'])) {
            if ($cpBnrLoopEntriesMin >= 1 && count($args['cpBnrLoop']) < $cpBnrLoopEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("バナー"), $cpBnrLoopEntriesMin, count($args['cpBnrLoop'])));
                $cpBnrLoopEntriesErrors++;
            }
            if ($cpBnrLoopEntriesMax >= 1 && count($args['cpBnrLoop']) > $cpBnrLoopEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("バナー"), $cpBnrLoopEntriesMax, count($args['cpBnrLoop'])));
                $cpBnrLoopEntriesErrors++;
            }
            if ($cpBnrLoopEntriesErrors == 0) {
                foreach ($args['cpBnrLoop'] as $cpBnrLoop_k => $cpBnrLoop_v) {
                    if (is_array($cpBnrLoop_v)) {
                        if (in_array("cpBnrImg", $this->btFieldsRequired['cpBnrLoop']) && (!isset($cpBnrLoop_v['cpBnrImg']) || trim($cpBnrLoop_v['cpBnrImg']) == "" || !is_object(File::getByID($cpBnrLoop_v['cpBnrImg'])))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("画像"), t("バナー"), $cpBnrLoop_k));
                        }
                        if ((in_array("cpBnrLink", $this->btFieldsRequired['cpBnrLoop']) && (!isset($cpBnrLoop_v['cpBnrLink']) || trim($cpBnrLoop_v['cpBnrLink']) == "")) || (isset($cpBnrLoop_v['cpBnrLink']) && trim($cpBnrLoop_v['cpBnrLink']) != "" && !array_key_exists($cpBnrLoop_v['cpBnrLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
))))) {
							$e->add(t("The %s field has an invalid value.", t("リンク")));
						} elseif (array_key_exists($cpBnrLoop_v['cpBnrLink'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
)))) {
							switch ($cpBnrLoop_v['cpBnrLink']) {
								case 'page':
									if (!isset($cpBnrLoop_v['cpBnrLink_Page']) || trim($cpBnrLoop_v['cpBnrLink_Page']) == "" || $cpBnrLoop_v['cpBnrLink_Page'] == "0" || (($page = Page::getByID($cpBnrLoop_v['cpBnrLink_Page'])) && $page->error !== false)) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Page"), t("リンク"), t("バナー"), $cpBnrLoop_k));
									}
									break;
				                case 'file':
									if (!isset($cpBnrLoop_v['cpBnrLink_File']) || trim($cpBnrLoop_v['cpBnrLink_File']) == "" || !is_object(File::getByID($cpBnrLoop_v['cpBnrLink_File']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("File"), t("リンク"), t("バナー"), $cpBnrLoop_k));
									}
									break;
				                case 'url':
									if (!isset($cpBnrLoop_v['cpBnrLink_URL']) || trim($cpBnrLoop_v['cpBnrLink_URL']) == "" || !filter_var($cpBnrLoop_v['cpBnrLink_URL'], FILTER_VALIDATE_URL)) {
										$e->add(t("The %s field for '%s' does not have a valid URL (%s, row #%s).", t("URL"), t("リンク"), t("バナー"), $cpBnrLoop_k));
									}
									break;
				                case 'relative_url':
									if (!isset($cpBnrLoop_v['cpBnrLink_Relative_URL']) || trim($cpBnrLoop_v['cpBnrLink_Relative_URL']) == "") {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Relative URL"), t("リンク"), t("バナー"), $cpBnrLoop_k));
									}
									break;
				                case 'image':
									if (!isset($cpBnrLoop_v['cpBnrLink_Image']) || trim($cpBnrLoop_v['cpBnrLink_Image']) == "" || !is_object(File::getByID($cpBnrLoop_v['cpBnrLink_Image']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Image"), t("リンク"), t("バナー"), $cpBnrLoop_k));
									}
									break;	
							}
						}
                        if (in_array("cpBnrPopupCheck", $this->btFieldsRequired['cpBnrLoop']) && (!isset($cpBnrLoop_v['cpBnrPopupCheck']) || trim($cpBnrLoop_v['cpBnrPopupCheck']) == "" || !in_array($cpBnrLoop_v['cpBnrPopupCheck'], array(0, 1)))) {
                            $e->add(t("The %s field is required.", t("画像をポップアップで表示"), t("バナー"), $cpBnrLoop_k));
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('バナー'), $cpBnrLoop_k));
                    }
                }
            }
        } else {
            if ($cpBnrLoopEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("バナー"), $cpBnrLoopEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-component_bnr', 'blocks/component_bnr/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-component_bnr');
        $this->edit();
    }

        protected function getSmartLinkTypeOptions($include = array(), $checkNone = false)
	{
		$options = array(
			''             => sprintf("-- %s--", t("None")),
			'page'         => t("Page"),
			'url'          => t("External URL"),
			'relative_url' => t("Relative URL"),
			'file'         => t("File"),
			'image'        => t("Image")
		);
		if ($checkNone) {
            $include = array_merge(array(''), $include);
        }
		$return = array();
		foreach($include as $v){
		    if(isset($options[$v])){
		        $return[$v] = $options[$v];
		    }
		}
		return $return;
	}
}