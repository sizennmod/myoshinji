<?php namespace Application\Block\TopMainNavi;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Permissions;
use File;
use Page;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array();
    protected $btExportFileColumns = array('imgtitle', 'imgmain');
    protected $btExportPageColumns = array();
    protected $btTable = 'btTopMainNavi';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("トップページメインナビゲーション");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->title;
        return implode(" ", $content);
    }

    public function view()
    {
        $link_URL = null;
		$link_Object = null;
		$link_Title = trim($this->link_Title);
		if (trim($this->link) != '') {
			switch ($this->link) {
				case 'page':
					if ($this->link_Page > 0 && ($link_Page_c = Page::getByID($this->link_Page)) && !$link_Page_c->error && !$link_Page_c->isInTrash()) {
						$link_Object = $link_Page_c;
						$link_URL = $link_Page_c->getCollectionLink();
						if ($link_Title == '') {
							$link_Title = $link_Page_c->getCollectionName();
						}
					}
					break;
				case 'file':
					$link_File_id = (int)$this->link_File;
					if ($link_File_id > 0 && ($link_File_object = File::getByID($link_File_id)) && is_object($link_File_object)) {
						$fp = new Permissions($link_File_object);
						if ($fp->canViewFile()) {
							$link_Object = $link_File_object;
							$link_URL = $link_File_object->getRelativePath();
							if ($link_Title == '') {
								$link_Title = $link_File_object->getTitle();
							}
						}
					}
					break;
				case 'url':
					$link_URL = $this->link_URL;
					if ($link_Title == '') {
						$link_Title = $link_URL;
					}
					break;
				case 'relative_url':
					$link_URL = $this->link_Relative_URL;
					if ($link_Title == '') {
						$link_Title = $this->link_Relative_URL;
					}
					break;
				case 'image':
					if ($this->link_Image && ($link_Image_object = File::getByID($this->link_Image)) && is_object($link_Image_object)) {
						$link_URL = $link_Image_object->getURL();
						$link_Object = $link_Image_object;
						if ($link_Title == '') {
							$link_Title = $link_Image_object->getTitle();
						}
					}
					break;
			}
		}
		$this->set("link_URL", $link_URL);
		$this->set("link_Object", $link_Object);
		$this->set("link_Title", $link_Title);
        
        if ($this->imgtitle && ($f = File::getByID($this->imgtitle)) && is_object($f)) {
            $this->set("imgtitle", $f);
        } else {
            $this->set("imgtitle", false);
        }
        
        if ($this->imgmain && ($f = File::getByID($this->imgmain)) && is_object($f)) {
            $this->set("imgmain", $f);
        } else {
            $this->set("imgmain", false);
        }
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
    }

    protected function addEdit()
    {
        $this->set("link_Options", $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
), true));
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        if (isset($args["link"]) && trim($args["link"]) != '') {
			switch ($args["link"]) {
				case 'page':
					$args["link_File"] = '0';
					$args["link_URL"] = '';
					$args["link_Relative_URL"] = '';
					$args["link_Image"] = '0';
					break;
				case 'file':
					$args["link_Page"] = '0';
					$args["link_URL"] = '';
					$args["link_Relative_URL"] = '';
					$args["link_Image"] = '0';
					break;
				case 'url':
					$args["link_Page"] = '0';
					$args["link_Relative_URL"] = '';
					$args["link_File"] = '0';
					$args["link_Image"] = '0';
					break;
				case 'relative_url':
					$args["link_Page"] = '0';
					$args["link_URL"] = '';
					$args["link_File"] = '0';
					$args["link_Image"] = '0';
					break;
				case 'image':
					$args["link_Page"] = '0';
					$args["link_File"] = '0';
					$args["link_URL"] = '';
					$args["link_Relative_URL"] = '';
					break;
				default:
					$args["link_Title"] = '';
					$args["link_Page"] = '0';
					$args["link_File"] = '0';
					$args["link_URL"] = '';
					$args["link_Relative_URL"] = '';
					$args["link_Image"] = '0';
					break;	
			}
		}
		else {
			$args["link_Title"] = '';
			$args["link_Page"] = '0';
			$args["link_File"] = '0';
			$args["link_URL"] = '';
			$args["link_Relative_URL"] = '';
			$args["link_Image"] = '0';
		}
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("title", $this->btFieldsRequired) && (trim($args["title"]) == "")) {
            $e->add(t("The %s field is required.", t("タイトル")));
        }
        if ((in_array("link", $this->btFieldsRequired) && (!isset($args["link"]) || trim($args["link"]) == "")) || (isset($args["link"]) && trim($args["link"]) != "" && !array_key_exists($args["link"], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
))))) {
			$e->add(t("The %s field has an invalid value.", t("リンク先")));
		} elseif (array_key_exists($args["link"], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
)))) {
			switch ($args["link"]) {
				case 'page':
					if (!isset($args["link_Page"]) || trim($args["link_Page"]) == "" || $args["link_Page"] == "0" || (($page = Page::getByID($args["link_Page"])) && $page->error !== false)) {
						$e->add(t("The %s field for '%s' is required.", t("Page"), t("リンク先")));
					}
					break;
				case 'file':
					if (!isset($args["link_File"]) || trim($args["link_File"]) == "" || !is_object(File::getByID($args["link_File"]))) {
						$e->add(t("The %s field for '%s' is required.", t("File"), t("リンク先")));
					}
					break;
				case 'url':
					if (!isset($args["link_URL"]) || trim($args["link_URL"]) == "" || !filter_var($args["link_URL"], FILTER_VALIDATE_URL)) {
						$e->add(t("The %s field for '%s' does not have a valid URL.", t("URL"), t("リンク先")));
					}
					break;
				case 'relative_url':
					if (!isset($args["link_Relative_URL"]) || trim($args["link_Relative_URL"]) == "") {
						$e->add(t("The %s field for '%s' is required.", t("Relative URL"), t("リンク先")));
					}
					break;
				case 'image':
					if (!isset($args["link_Image"]) || trim($args["link_Image"]) == "" || !is_object(File::getByID($args["link_Image"]))) {
						$e->add(t("The %s field for '%s' is required.", t("Image"), t("リンク先")));
					}
					break;	
			}
		}
        if (in_array("imgtitle", $this->btFieldsRequired) && (trim($args["imgtitle"]) == "" || !is_object(File::getByID($args["imgtitle"])))) {
            $e->add(t("The %s field is required.", t("タイトル画像")));
        }
        if (in_array("imgmain", $this->btFieldsRequired) && (trim($args["imgmain"]) == "" || !is_object(File::getByID($args["imgmain"])))) {
            $e->add(t("The %s field is required.", t("メイン画像")));
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-top_main_navi', 'blocks/top_main_navi/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-top_main_navi');
        $this->edit();
    }

    protected function getSmartLinkTypeOptions($include = array(), $checkNone = false)
	{
		$options = array(
			''             => sprintf("-- %s--", t("None")),
			'page'         => t("Page"),
			'url'          => t("External URL"),
			'relative_url' => t("Relative URL"),
			'file'         => t("File"),
			'image'        => t("Image")
		);
		if ($checkNone) {
            $include = array_merge(array(''), $include);
        }
		$return = array();
		foreach($include as $v){
		    if(isset($options[$v])){
		        $return[$v] = $options[$v];
		    }
		}
		return $return;
	}
}