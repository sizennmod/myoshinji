<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<figure class="c-kv_item">
	<?php echo sprintf('<a href="%s" class="e-box">', $link_URL); ?>
		<span class="e-thumb" style="background:url(<?php echo $imgmain->getURL(); ?>) no-repeat center center;">
			<img src="<?php echo $imgmain->getURL(); ?>" alt="<?php echo $imgtitle->getTitle(); ?>" class="e-thumb_img">
		</span>
		<figcaption class="e-caption" style="background: url(<?php echo $imgtitle->getURL(); ?>) no-repeat center center;background-size:100% auto;"><?php echo $title;?></figcaption>
	</a>
</figure>