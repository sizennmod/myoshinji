<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php $this->inc('elements/header.php');?>
<?php 
$page_main_img = $c->getAttribute('page_main_img');
$page_main_text_img = $c->getAttribute('page_main_text_img');

if(is_object($page_main_img)){
	$bg = ' style="background:url('.$page_main_img->getUrl().')"';
}

if(is_object($page_main_text_img)){
	$catch = Core::make('html/image', array($page_main_text_img))->getTag();
	$catch->width = null;
	$catch->height = null;
	$catch->alt = $c->getCollectionName();
}
?>
<div class="c-mv"<?= $bg?>>
	<div class="c-mv_inner u-inner">
		<h1 class="c-mv_name">
			<?php if($catch) echo $catch;?>
		</h1>
	</div><!--inner-->
</div>

<?php $this->inc('elements/breadcrumb.php') ?>

<main class="g-main u-inner">
	<div class="g-primary">
		<?php
			$a = new Area('メインコンテンツ');
			$a->display($c);
		?>
	</div>

	<aside class="g-side">
		<?php
			$a = new GlobalArea('サイドナビ');
			$a->display();
		?>
		<?php 
		$a = new Area('サイドナビ ページ個別');
		if ($c->isEditMode() || $a->getTotalBlocksInArea($c) > 0) {
		?>
		<div class="g-sideLink">
			<h3 class="g-sideLink_head">
				<span class="e-name">リンク</span>
			</h3>
			<ul class="g-sideLink_items">
				<?php
				$a->setCustomTemplate('manual_nav', 'sidenav_3.php');
				$a->display();
				?>
			</ul>
		</div>
		<?php } ?>
	</aside>
</main>

<?php $this->inc('elements/footer.php') ?>