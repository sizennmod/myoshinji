http_path = "/"
css_dir = "assets/css"
sass_dir = "assets/scss"
images_dir = "assets/img"
javascripts_dir = "assets/js"

#environment = :production

output_style = (environment == :production) ? :compressed : :expanded
relative_assets = (environment == :production) ? false : true
line_comments = (environment == :production) ? false : true
asset_cache_buster = :none
