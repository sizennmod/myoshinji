<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php $this->inc('elements/header.php');?>
<?php 
$nh = Core::make('helper/navigation');
$data_show_self_title = $c->getAttribute('data_show_self_title');
if($data_show_self_title !== true) {
	$trail = $nh->getTrailToCollection($c);
	$ancestors = array_reverse($trail);
	if(count($ancestors) > 1){
		$ancestor = array_slice($ancestors, 1,1);
		$ancestor = $ancestor[0];
	}else{
		$ancestor = $c;
	}
	
	$mv_title = $ancestor->getCollectionName();

	$data_header_img = $ancestor->getAttribute('data_header_img');
	$data_page_title_en = $ancestor->getAttribute('data_page_title_en');
}else{
	$mv_title = $c->getCollectionName();
	$data_header_img = $c->getAttribute('data_header_img');
	$data_page_title_en = $c->getAttribute('data_page_title_en');
}

if(is_object($data_header_img)){
	$bg = ' style="background:url('.$data_header_img->getUrl().') no-repeat center center;background-size:cover;"';
}

$data_contents_title = $c->getAttribute('data_contents_title');
$data_contents_subtitle = $c->getAttribute('data_contents_subtitle');
$data_introduction = $c->getAttribute('data_introduction');

if(!$data_contents_title){
	$data_contents_title = $c->getCollectionName();
}

?>
<div class="c-mv"<?= $bg;?>>
	<div class="c-mv_inner u-inner">
		<h1 class="c-mv_name">
			<strong class="e-name"><?= h($mv_title);?></strong>
			<span class="e-en"><?= $data_page_title_en;?></span>
		</h1>
	</div><!--inner-->
</div>

<?php $this->inc('elements/breadcrumb.php') ?>

<main class="g-main u-inner" id="page_content_top">
	<div class="g-primary">
		<section class="c-pageTitle">
			<h1 class="c-pageTitle_name"><?= $data_contents_title;?></h1>
			<?php if($data_contents_subtitle){?>
			<h2 class="c-pageTitle_sub"><?= $data_contents_subtitle;?></h2>
			<?php } ?>
			<?php if($data_introduction){?>
			<div class="c-pageTitle_desc"><?= $data_introduction;?></div>
			<?php } ?>
		</section>
		<?php
			$a = new Area('メインコンテンツ');
			$a->display($c);
		?>
	</div>
	<?php 
	$a3 = new GlobalArea('サイドナビ');
	$a4 = new GlobalArea('サイドナビ2');
	?>
	<aside class="g-side">
		<?php
			$a3->display();
			$a4->setCustomTemplate('manual_nav', 'sidenav_2.php');
			$a4->display();
		?>

		<?php 
		$a1 = new GlobalArea('サイドナビ3');
		$a2 = new Area('サイドナビ ページ個別');
		if ($c->isEditMode() || $a1->getTotalBlocksInArea($c) > 0  || $a2->getTotalBlocksInArea($c) > 0) {
		?>
		<div class="g-sideLink">
			<h3 class="g-sideLink_head">
				<span class="e-name">リンク</span>
			</h3>
			<ul class="g-sideLink_items">
				<?php
				$a1->setCustomTemplate('manual_nav', 'sidenav_3.php');
				$a1->display();
				?>
				<?php
				$a2->setCustomTemplate('manual_nav', 'sidenav_3.php');
				$a2->display();
				?>
			</ul>
		</div>
		<?php } ?>
	</aside>
</main>

<?php $this->inc('elements/footer.php') ?>