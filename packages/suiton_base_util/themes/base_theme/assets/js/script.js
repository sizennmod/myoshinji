/* ---------------------------------------------------
 *
 * script.js
 *
 * ---------------------------------------------------*/




/* ---------------------------------------------------
 * CONSOLELOG IE BUGFIX
 * ---------------------------------------------------*/
(function(){
	if(typeof window.console === 'undefined') window.console = {};
	if(typeof window.console.log !== 'function') window.console.log = function (){};
})();




/* ---------------------------------------------------
 * INDEXOF IE BUGFIX
 * ---------------------------------------------------*/
if(!Array.prototype.indexOf){
	Array.prototype.indexOf = function(target,index){
		if(isNaN(index)) index = 0;
		for(var i = index; i < target.length; i++){
			if(this[i] === target) return i;
		}
		return -1;
	};
}




/* ---------------------------------------------------
 * OS & BROWSER & USERAGENT
 * ------------------------------------------------ */
var
	uaStr = window.navigator.userAgent.toLowerCase(),
	appVer = window.navigator.appVersion.toLowerCase(),
	isDevice = false,
	isIE = false,
	isIEOld = false,
	isEdge = false,
	ieVer = false,
	isWebkit = false,
	isFirefox = false,
	isTouch = false,
	isOpacity = true,
	isTransform = true,
	isTransition = true
;

var ua = (function(u){
	return {
		Tablet: (u.indexOf('windows') != -1 && u.indexOf('touch') != -1) || u.indexOf('ipad') != -1 || (u.indexOf('android') != -1 && u.indexOf('mobile') == -1) || (u.indexOf('firefox') != -1 && u.indexOf('tablet') != -1) || u.indexOf('kindle') != -1 || u.indexOf('silk') != -1 || u.indexOf('playbook') != -1,
		Mobile: (u.indexOf('windows') != -1 && u.indexOf('phone') != -1) || u.indexOf('iphone') != -1 || u.indexOf('ipod') != -1 || (u.indexOf('android') != -1 && u.indexOf('mobile') != -1) || (u.indexOf('firefox') != -1 && u.indexOf('mobile') != -1) || u.indexOf('blackberry') != -1,
		IE: u.indexOf('trident') != -1 || u.indexOf('msie') != -1,
		Edge: u.indexOf('edge') != -1,
		Webkit: u.indexOf('chrome') != -1 || u.indexOf('safari') != -1,
		Firefox: u.indexOf('firefox') != -1
	};
})(uaStr);


/* SMARTPHONE
 * ------------------------------------------------ */
if(ua.Tablet || ua.Mobile) isDevice = true;
if(isDevice){
	$('html').addClass('device');
}else{
	$('html').addClass('no-device');
}


/* IE
 * ------------------------------------------------ */
if(ua.IE){
	isIE = true;
	var ieAry = /(msie|rv:?)\s?([\d\.]+)/.exec(uaStr);
	ieVer = (ieAry) ? ieAry[2] : '';
	ieVer = Math.floor(ieVer);
	$('html').addClass('ie');
	$('html').addClass('ie' + ieVer);
	if(ieVer <= 8){
		isIEOld = true;
		$('html').addClass('ieOld');
	}
}


/* EDGE
 * ------------------------------------------------ */
if(ua.Edge){
	isEdge = true;
	$('html').addClass('edge');
}


/* WEBKIT
 * ------------------------------------------------ */
if(ua.Webkit){
	isWebkit = true;
}




/* ---------------------------------------------------
 * GET ROOTPATH
 * --------------------------------------------------- */
var
	pageURL = '',
	pagePath = ''
;
(function(){
	var
		file = 'assets/js/script.js',
		reg = new RegExp('(^|.*\/)' + file + '$'),
		script = document.getElementsByTagName('script'),
		i = script.length
	;
	while(i--){
		var src = script[i].src.replace(/\?.*?$/g, '');
		var match = src.match(reg);
		if(match){
			pageURL = match[1];
			break;
		}
	}
	pagePath = pageURL.slice(pageURL.indexOf(location.hostname) + location.hostname.length);
//console.log('pagePath:' + pagePath);
}());




/* ---------------------------------------------------
 * SMOOTH SCROLL
 * ------------------------------------------------ */
$(function(){
	if(!CCM_EDIT_MODE){
		/* CLICK */
		$('.ccm-page a[href^="#"]').on('click', function(){
			if(!$(this).hasClass('js-noScroll')){
				var href = $(this).attr('href');
				smoothScroll({
					href: href,
					speed: 500,
					easing: 'easeOutExpo',
					fixed: '' //EX.'#header'
				});
				return false;
			}
		});
	}

	/* LOAD */
	var hash = location.hash;
	if(hash){
		//$(window).scrollTop(0, 0);
		setTimeout(function(){
			smoothScroll({
				href: hash,
				speed: 1,
				easing: 'easeOutExpo'
			});
		}, 750);
	}
});
function smoothScroll(e){
	var
		speed = e.speed ? e.speed : 750,
		easing = e.easing ? e.easing : 'easeOutExpo',
		target = $(e.href === '#' || e.href === '' ? '#PAGETOP' : e.href),
		position = (target.length > 0) ? target.offset().top : 0,
		posMinus = (e.href === '#PAGETOP') ? 0 : (e.fixed ? $(e.fixed).outerHeight() : 0),
		zoom = ($('html').css('zoom') != 1 && !(isNaN($('html').css('zoom')))) ? Number($('html').css('zoom')) : 1
	;
	$('html, body').animate({scrollTop: (position - posMinus) * zoom}, speed, easing);
}




/* ---------------------------------------------------
 * HEIGHT GROUP
 * ------------------------------------------------ */
function heightGFunc(option){
	var
		elm = option.element,
		column = option.column ? option.column : 9999,
		minWidth = option.minWidth ? option.minWidth : 0,
		parent = option.parent,
		timer = false
	;

	var elmAry = [];
	if(elm.indexOf(',') <= -1){
		elmAry[0] = elm;
	}else{
		elmAry = elm.split(',');
	}

	$(window).on('load resize', function(){
		if(timer !== false) clearTimeout(timer);
		timer = setTimeout(function(){
			func(elm, column, minWidth);
		}, 50);
	});
	setTimeout(function(){
		func(elm, column, minWidth);
	}, 500);

	function func(elm, column, minWidth){
		elmW = $(elm).width();
		winW = $(window).width();

		if(winW < minWidth){
			group = Math.floor(winW / elmW);
		}else{
			if(!column){
				group = 9999;
			}else{
				group = column;
			}
		}

		for(var i = 0; i < elmAry.length; i++){
			var
				list = $(elmAry[i]),
				grH = [],
				grHNew = 0,
				count = 0,
				num = 0,
				parentInd = 0,
				parentIndThis = 0
			;
			list.each(function(i){
				$(this).css({height: 'auto'});
				grHNew = $(this).height();

				if(count == group){
					++num;
					count = 0;
					grH[num] = 0;
				}
				if(count === 0){
					grH[num] = 0;
				}
				if(grH[num] < grHNew){
					grH[num] = grHNew;
				}
				++count;
			});

			count = 0;
			num = 0;
			parentInd = 0;
			parentIndThis = 0;

			list.each(function(){
				if(count == group){
					++num;
					count = 0;
				}
				$(this).height(grH[num]);
				++count;
			});
		}
	}
}


/* RUN
 * ------------------------------------------------ */
/*
$(function(){
	heightGFunc({
		element: '.js-xxxxx',
		column: 3
	});
}); //END jQuery
*/




/* ---------------------------------------------------
 * LINK FORCUS
 * ------------------------------------------------ */
$(function(){
	var
		path = location.pathname.replace(/^\//g, '').replace(/\/$/g, ''),
		pathArray = path.split('/'),
		domain = document.domain,
		forcusClass = 'is-focus_active'
	;
	path = path + '/';


	/* Global Header
	 * ------------------------------------------------ */
	forcusFnc({
		element: $('.js-linkFocus a'),
		lower: false //For Contents top link
	});
	forcusFnc({
		element: $('.js-linkFocus_top a'),
		lower: true //For Contents top link
	});


	/* Active
	 * ------------------------------------------------ */
	function forcusFnc(obj){
		element = obj.element;
		lower = obj.lower ? true : false;

		element.each(function(){
			var
				dataFocus = $(this).attr('data-focus'),
				url = $(this).get(0).href,
				href = dataFocus ? dataFocus : url.slice(url.indexOf(domain) + domain.length).replace(/^\//g, '').replace(/\/$/g, ''),
				array = href.split('/'),
				match = 0
			;
			href = href + '/';
			for(var i = 0; i < array.length; i++){
				if(path.indexOf(array[i]) >= 0){
					match++;
				}
			}

			if(lower){
				if(url.indexOf('/') >= 0 && pathArray.length == 1 && array.length === match && array.length === pathArray.length){
					classFnc($(this));
				}else if(url.indexOf('/') >= 0 && array.length > 1 && path.indexOf(href) >= 0){
					classFnc($(this));
				}
			}else{
				if(url.indexOf('/') >= 0 && (array[0] === '' && pathArray[0] === '') || (path.indexOf(href) >= 0 && array[0] !== '')){
					classFnc($(this));
				}
			}

			function classFnc(select){
				select.parent().addClass(forcusClass);
			} //END classFnc
		});
	} //END forcusFnc
}); //END jQuery




/* ---------------------------------------------------
 * HOVER CLASS
 * ------------------------------------------------ */
/*
$(function(){
	var
		select = '.js-hoverClass',
		onClass = 'is-hover_on'
	;
	$(select).on({
		'touchstart': function() {
			return $(this).unbind('mouseover mouseout');
		},
		'touchstart mouseover': function() {
			return $(this).addClass(onClass);
		},
		'mouseout': function() {
		//'touchend mouseout': function() {
			return $(this).removeClass(onClass);
		}
	});
	$(document).on('touchstart', function(){
		$(select).removeClass(onClass);
	});
	$(select).on('touchstart', function(){
		event.stopPropagation();
	});
}); //END jQuery
*/



/* ---------------------------------------------------
 * HEADER MENU
 * ------------------------------------------------ */
$(function(){
	var
		menu = '.js-headMenu',
		trigger = '.js-headMenu_trigger',
		slide = '.js-headMenu_slide',
		onClass = 'is-menu_open',
		readyClass = 'is-menu_ready',
		speed = 350
	;

	$(menu).removeClass(onClass).addClass(readyClass);
	$(slide).slideUp(0);
	$(trigger).removeClass(onClass).addClass(readyClass);

	$(trigger).on('click', function(){
		$(menu).toggleClass(onClass);
		$(slide).slideToggle(speed);
		$(trigger).toggleClass(onClass);
		return false;
	});
	if(!CCM_EDIT_MODE){
		$(document).on('click', function(){
			$(menu).removeClass(onClass);
			$(slide).slideUp(speed);
			$(trigger).removeClass(onClass);
		});
	}
	$(menu).on('click', function(){
		event.stopPropagation();
	});
	$(trigger).on('click', function(){
		event.stopPropagation();
	});
}); //END jQuery




/* ---------------------------------------------------
 * ACCORDION
 * ------------------------------------------------ */
$(function(){
	var
		obj = '.js-accordion',
		trigger = '.js-accordion_trigger',
		slide = '.js-accordion_slide',
		openClass = 'is-accordion_open',
		speed = 250
	;

	$(obj).each(function(index){
		if(!$(this).hasClass(openClass)){
			$(this).find(slide).slideUp(0);
		}
	});

	$(trigger).on('click', function(){
		$(this).closest(obj).find(slide).stop().slideToggle(speed);
		$(this).closest(obj).toggleClass(openClass);
	});
}); //END jQuery




/* ---------------------------------------------------
 * SLIDER
 * ------------------------------------------------ */
$(function(){
	$('.js-slider').slick({
		variableWidth: true,
		autoplay: true,
		autoplaySpeed: 3000,
		pauseOnHover: true,
		pauseOnDotsHover: true,
		dots: false,
		infinite: true,
		speed: 750,
		slidesToShow: 3,
		centerMode: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					//slidesToScroll: 1,
					//variableWidth: false,
					//centerMode: false,
					slidesToShow: 1,
					centerPadding: '0px'
				}
			}
		]
	});
}); //END jQuery




/* ---------------------------------------------------
 * POPUP
 * ------------------------------------------------ */
$(function(){
	$('.js-popup').magnificPopup({
		delegate: '.js-popup_item a',
		type: 'image',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1]
		},
		removalDelay: 500,
		callbacks: {
			beforeOpen: function(){
				this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
				this.st.mainClass = this.st.el.attr('data-effect');
			}
		},
		closeOnContentClick: true,
		midClick: true
	});
}); //END jQuery




/* ---------------------------------------------------
 * TAB CHANGE
 * ------------------------------------------------ */
$(function(){
	var
		select = '.js-tabChanage',
		trigger = '.js-tabChanage_trigger',
		content = '.js-tabChanage_content',
		activeClass = 'is-state_active',
		index = 0
	;

	$(content).hide().eq(index).show();
	$(trigger).removeClass(activeClass).eq(index).addClass(activeClass);

	$(trigger).on('click', function(){
		if(!$(this).hasClass(activeClass)){
			index = $(this).index();
			$(content).hide().eq(index).fadeIn();
			$(trigger).removeClass(activeClass).eq(index).addClass(activeClass);
		}

		return false;
	});

}); //END jQuery




/* ---------------------------------------------------
 * OPENING
 * ------------------------------------------------ */
$(function(){
	var
		select = '.js-opening',
		band = '.js-opening_band',
		slides = '.js-opening_slides',
		slide = '.js-opening_slide',
		activeClass = 'is-slide_active',
		nextClass = 'is-slide_next',
		fade = 1500,
		delay = 3000,
		max = $(slide).length,
		index = 0,
		next = index + 1
	;

	/* SLIDE */
	$(slide).hide();
	$(slide).eq(index).addClass(activeClass);
	$(slide).eq(next).addClass(nextClass);


	/* COOKIE */
	try{
		if($(select).length <= 0 || $.cookie('opening') == 'true'){
			$('html').css({'display': 'inline'});
			$('html').css({'overflow': 'auto'});
			$('#blank').css({'display': 'none'});
			$(select).hide();
		}else{
			$('html').css({'display': 'inline'});
			$('html').css({'overflow': 'hidden'});
			$('.c-opening').css({'display': 'inline'});
			$('.g-wrapper').outerHeight($(window).height()).css({'overflow': 'hidden'});
			$('#blank').css({'display': 'none'});
		}
//console.log('cookie:' + $.cookie('opening'));
	}catch(e){}

	/* LOAD */
	$(window).on('load', function(){
		$(slide).eq(index).fadeIn(fade);

		setInterval(function(){
			$(slide).eq(next).show();
			$(slide).eq(index).fadeOut(fade, function(){
				$(this).removeClass(activeClass);

				index = next;
				next = (next == (max - 1)) ? 0 : next + 1;
				$(slide).eq(index).removeClass(nextClass).addClass(activeClass);
				$(slide).eq(next).addClass(nextClass);
				$(slide).eq(index).fadeIn(fade);
			});
		}, delay + fade);
	});

	/* CLICK */
	$(select).on('click', function(){
		$('html').css({'overflow': 'auto'});
		$('.g-wrapper').css({'overflow': 'auto', 'height': 'auto'});
		$(window).scrollTop(0);
		$(band).fadeOut(1250, function(){
			$(select).fadeOut(1250);

			$.cookie('opening', 'true', {expires: 7, path: '/'});
		});
	});
}); //END jQuery



/* ---------------------------------------------------
 * FACEBOOK PAGE
 * ------------------------------------------------ */
$(function(){
	var select = '.js-fbpage';
	if($(select).size() > 0){
		var winW = $(window).width();
		var html = $(select).html();
		var timer = null;
		$(window).on('resize',function(){
			var resizedW = $(window).width();
			if(winW != resizedW && resizedW < 769){
				clearTimeout(timer);
				timer = setTimeout(function(){
					$(select).html(html);
					window.FB.XFBML.parse();
					var winW = $(window).width();
				}, 100);
			}
		});
	}
});

(function($) {
$(function(){
	$('[href^=http]:not([href*="'+location.hostname+'"],[href$=pdf],[href$=xls],[href$=xlsx],[href$=xlsm],[href$=doc],[href$=docx],[href$=pptx],[href$=pptm],[href$=ppt])').attr({target:"_blank"});
});
})(jQuery);


