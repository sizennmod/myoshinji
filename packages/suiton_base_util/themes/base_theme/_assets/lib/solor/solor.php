<?php
/**
 * 太陽光発電量を表示する
 *
 * @package    PackageName
 * @author     2013/06/25 製作者1 <kawata@selva-i.co.jp>
 */

// 初期値
$result = '';
$csv  = array();
$file = './assets/lib/solor/solorlog.csv';

// csvファイルオープン処理
$fp   = fopen($file, "r");
while (($data = fgetcsv($fp, 0, "/")) !== FALSE) {
  $csv[] = $data;
}
fclose($fp);

// データは15分前の物か？
if (strtotime(date('Y-m-d H:i:s', strtotime('-15 minute ' . $today))) >= strtotime($csv[0][0] . " " . $csv[0][1])) {

    //-------------------------現在---------------------------//
    $result = getSolor('instant', $result);
    //-------------------------本日---------------------------//
    $result = getSolor('daily', $result);
    //-------------------------今月---------------------------//
    $result = getSolor('monthly', $result);
    //-------------------------これまで---------------------------//
    $result = getSolor('total', $result);


    echo $result;
    $result  = date('Y-m-d H:i') . "/" . $result;
    $result  = str_replace("/", "\r\n", $result);

    $fp = fopen($file, "w");
    fwrite($fp, $result);
    fclose($fp);

} else {
// データが15分以内に存在する

    $result  = $csv[1][0];
    $result .= "/";
    $result .= $csv[2][0];
    $result .= "/";
    $result .= $csv[3][0];
    $result .= "/";
    $result .= $csv[4][0];
    $result .= "/";

    echo $result;
}

/*
 * ソーラーAPIより格項目を取得する。
 * @param string $item 項目
 * @param string $title 題名
 * return string $result 結果
 */

function getSolor($item, $result) {

    // 認証されるユーザー名
    $username = 'XMZ0130541';

    // 認証パスワード
    $password = 'KdPgRbWk4yWFXFv';

    // ターゲットurl
    $url = 'https://services18.energymntr.com/megasolar/XMZ0130541/services/api/generating/' . $item . '.php?unit=total&groupid=1&time=now';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);

    if( !$data ) {
    // エラーの場合そのまま返却
       return $result;
    } else {
        $solorTestObject = simplexml_load_string($data);
        $solorTestArray =  xml2array($solorTestObject);
        $solorTestArray = ($solorTestArray['apiResponse'][0]['apiResult'][0][$item][0]);

        $result .= number_format($solorTestArray['acEnergy'][0]['_value']) . "/";

        // 接続を閉じる
        curl_close($ch);

       return $result;
    }
}

/*
 * simplexml形式を連想配列にする。
 * @param Object $sxml = xml内容
 * retun  Array $r 連想配列
 */

function xml2array(&$sxml, $isRoot = true){
  if ($isRoot)
    return array($sxml->getName() => array(xml2array($sxml, false)));
  $r = array();
  foreach($sxml->children() as $cld){
    $a = &$r[(string)$cld->getName()];
    $a = &$a[count($a)];
    if (count($cld->children()) == 0)
      $a['_value'] = (string)$cld;
    else
      $a = xml2array($cld, false);
    foreach($cld->attributes() as $at)
      $a['_attr'][(string)$at->getName()] = (string)$at;
  }
  return $r;
}
