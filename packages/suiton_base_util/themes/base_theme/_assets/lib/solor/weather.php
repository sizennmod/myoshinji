<?php
// 天気プロパティ
$condition = array(395 => '大雪所|雷',
                   392 => '小雪|雷',
                   389 => '大雨|雷',
                   386 => '雨|雷',
                   377 => '大雨|凍雨',
                   374 => '雨|凍雨',
                   371 => '雨|豪雨',
                   368 => '雨|大雨',
                   365 => 'みぞれ|雨',
                   362 => 'みぞれ|雨',
                   359 => '豪雨',
                   356 => '雨|大雨',
                   353 => '小雨',
                   350 => '凍雨',
                   338 => '大雪',
                   335 => '晴れ|大雪',
                   332 => '雪',
                   329 => '晴れ|雪',
                   326 => '小雪',
                   323 => '晴れ|小雪',
                   320 => 'みぞれ',
                   317 => 'みぞれ',
                   314 => '激しい雹',
                   311 => '雹',
                   308 => '大雨',
                   305 => '晴れ|大雨',
                   302 => 'にわか雨',
                   299 => '晴れ|にわか雨',
                   296 => '小雨',
                   293 => '晴れ|小雨',
                   284 => '晴れ|あられ',
                   281 => '晴れ|雹',
                   266 => '小雨',
                   263 => '晴れ|小雨',
                   260 => '霧',
                   248 => '霧',
                   230 => '吹雪',
                   227 => '雪',
                   200 => '雷',
                   185 => '晴れ|雹',
                   182 => '晴れ|みぞれ',
                   179 => '晴れ|雪',
                   176 => '晴れ|雨',
                   143 => '霧',
                   122 => '曇り',
                   119 => '曇り',
                   116 => 'やや曇り',
                   113 => '晴れ',
                   );

$file = './assets/lib/solor/weatherlog.csv';

// csvファイルオープン処理
$data = file_get_contents($file);

$data = split(',', $data);

// データは15分前の物か？
if (strtotime(date('Y-m-d H:i:s', strtotime('-15 minute ' . $today))) >= strtotime($data[0])) {

    // apiリクエスト
    $weatherObject = simplexml_load_file('http://api.worldweatheronline.com/free/v1/weather.ashx?q=Kyoto&format=xml&num_of_days=1&key=7cyyvmmpj2duvfcvbkyq8duj');
    if($weatherObject) {
        $weatherArray =  xml2array($weatherObject);
        $result  = $weatherArray['data'][0]['weather'][0][tempMaxC][0][_value] . ',';
        $result .= $weatherArray['data'][0]['weather'][0][tempMinC][0][_value] . ',';

        // プロパティより天気導出
        $result .= $condition[$weatherArray['data'][0]['weather'][0][weatherCode][0][_value]] . ',';
        echo $result;
    }

    $result = date('Y-m-d H:i') . "," . $result;
    $fp = fopen($file, "w");
    fwrite($fp, $result);
    fclose($fp);

} else {
// データが15分以内に存在する

    $result  = $data[1];
    $result .= ",";
    $result .= $data[2];
    $result .= ",";
    $result .= $data[3];
    $result .= ",";

    echo $result;
}

/*
 * simplexml形式を連想配列にする。
 * @param Object $sxml = xml内容
 * retun  Array $r 連想配列
 */

function xml2array(&$sxml, $isRoot = true){
  if ($isRoot)
    return array($sxml->getName() => array(xml2array($sxml, false)));
  $r = array();
  foreach($sxml->children() as $cld){
    $a = &$r[(string)$cld->getName()];
    $a = &$a[count($a)];
    if (count($cld->children()) == 0)
      $a['_value'] = (string)$cld;
    else
      $a = xml2array($cld, false);
    foreach($cld->attributes() as $at)
      $a['_attr'][(string)$at->getName()] = (string)$at;
  }
  return $r;
}
