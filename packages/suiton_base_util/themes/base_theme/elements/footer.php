<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php if($is_home){?>
</main><!--main-->
<?php }?>
</div><!--content-->


<div class="g-pagetop">
	<a href="#PAGETOP" class="e-box">
		<span class="e-text">ページの先頭へ</span>
	</a>
</div><!--pagetop-->

<footer class="g-footer">
	<div class="g-footer_inner u-inner">
		<?php
			$a = new GlobalArea('フッターSNS');
			$a->display();
		?>
		<div class="g-footer_grid">
			<?php
				$a = new GlobalArea('フッターロゴ');
				$a->display();
			?>
			<div class="g-footLink g-footer_cell">
				<div class="g-footLink_wrap">
					<ul class="g-footLink_items">
						<?php
							$a = new GlobalArea('フッターリンク_1');
							$a->display();
						?>
					</ul>
					<ul class="g-footLink_items">
						<?php
							$a = new GlobalArea('フッターリンク_2');
							$a->display();
						?>
					</ul>
					<ul class="g-footLink_items">
						<?php
							$a = new GlobalArea('フッターリンク_3');
							$a->display();
						?>
					</ul>
					<ul class="g-footLink_items is-media_show">
						<?php
							$a = new GlobalArea('フッターリンク_4');
							$a->display();
						?>
					</ul>
				</div><!--wrap-->
			</div><!--link-->
		</div><!--top-->
		<small class="g-footer_copy">
		<?php
			$a = new GlobalArea('コピーライト');
			$a->display();
		?>
		</small>
	</div><!--inner-->
</footer><!--footer-->


<div id="responsive_flg"></div>
</div><!--/getPageWrapperClass-->
<?php Loader::element('footer_required'); ?>
</body></html>
