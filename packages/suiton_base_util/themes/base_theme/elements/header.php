<?php
defined('C5_EXECUTE') or die(_("Access Denied."));
$u = new User();
?>
<!DOCTYPE html><!-- [if IEMobile 7 ]>  <html lang="ja" class="no-js iem7"><![endif] -->
<!-- [if lt IE 7]><html lang="ja" class="no-js lt-ie9 lt-ie8 lt-ie7 ie6"><![endif] -->
<!-- [if IE 7]><html lang="ja" class="no-js lt-ie9 lt-ie8 ie7"><![endif] -->
<!-- [if IE 8]><html lang="ja" class="no-js lt-ie9 ie8"><![endif] -->

<!-- [if (gte IE 8)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html lang="ja" class="no-js">
<!-- <![endif]-->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="UTF-8">
	<meta id="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=5.0,user-scalable=1" name="viewport">
	<?php Loader::element('header_required');?>
	<script>var CCM_THEME_PATH = '<?php echo $this->getThemePath()?>'</script>
	<?php
	//for total pagenation num
	//for ajax pagenation,for infinityscroll etc...
	?>
	<script>var cm_this_pagenation_total = 0;</script>
<?php if($c->getAttribute('attr_header_none') > 0){?>
<style>
	.g-header,.g-nav,.g-breadcrumb ,.g-menuBtn ,.g-footSns,.g-footer_grid{
		display: none;
	}
</style>
<?php } ?>
<?php if($c->isEditMode()):?>
<style>
	.ccm-area-drag-area {
		letter-spacing: 0;
	}
	.ccm-ui {
		letter-spacing: 0!important;
	}
	body .ccm-page , .ccm-page select, .ccm-page input, .ccm-page button, .ccm-page textarea, .ccm-page :before, .ccm-page :after {
		font-family: inherit;
	}
	.ui-widget-overlay {
		background: rgba(240,240,240,0.9);
	}
	.g-menu.is-menu_ready,.g-menu_wrap,.g-footSns {
		display: block!important;
		position: static;
	}
</style>
<?php endif;?>

<?php if($u->isLoggedIn()):?>
	<script>
		var IS_LOGGED_IN = true;
	</script>
	<style>
		.c-opening {
			position: static;
		}
		.c-opening_bg {
			display: block;
			position: static;
			width: 200px;
			height: 200px;
			background: no-repeat center center;
			background-size: cover;
			z-index: 1;
		}
		.c-opening_band {
			position: static;
			width: 140px;
			height: 450px;
		}
	</style>
<?php else:?>
	<script>
		var IS_LOGGED_IN = false;
	</script>
	<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" media="all">
<?php endif;?>
</head>

<body id="" class="g-body">

<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.0";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<div id="PAGETOP" class="<?php echo $c->getPageWrapperClass();?> g-wrapper ccm-page">
	 <?php
	 // $au = Core::make('helper/aUtil');
	 // echo $au->test();
	 ?>
	<header class="g-header">
		<div class="g-header_wrap">
			<?php
				$a = new GlobalArea('グローバルナビヘッダー');
				$a->display();
			?>
		</div><!--wrap-->
	</header><!--header-->
	
	<?php if(!$is_home){?>
	<nav class="g-nav">
		<div class="g-navMain">
			<?php
				$a = new GlobalArea('グローバルナビPC_1');
				$a->setCustomTemplate('manual_nav', 'gnav_pc_1.php');
				$a->display();
			?>
			
		</div><!--main-->
		<!--<div class="g-navSub">
			<?php
				$a = new GlobalArea('グローバルナビPC_2');
				$a->setCustomTemplate('manual_nav', 'gnav_pc_2.php');
				$a->display();
			?>
		</div>-->
	</nav>
	<?php }?>

	<div class="g-menuBtn js-headMenu_trigger">
		<span class="g-menuBtn_line is-line_top"></span>
		<span class="g-menuBtn_line is-line_middle"></span>
		<span class="g-menuBtn_line is-line_bottom"></span>
		<span class="g-menuBtn_text"></span>
	</div><!--menuBtn-->

	<header class="g-menu js-headMenu">
		<div class="g-menu_wrap js-headMenu_slide">
			<div class="g-menu_inner u-inner">
				<nav class="g-menuNav">
					<?php
						$a = new GlobalArea('グローバルナビSP_1');
						$a->setCustomTemplate('manual_nav', 'gnav_sp_1.php');
						$a->display();
					?>
					
					<?php
						$a = new GlobalArea('グローバルナビSP_2');
						$a->setCustomTemplate('manual_nav', 'gnav_sp_1.php');
						$a->display();
					?>
					
				</nav>
				<div class="g-menu_bottom">
					<?php
						$a = new GlobalArea('グローバルナビSP_3');
						$a->display();
					?>
				</div><!--bottom-->
			</div><!--inner-->
		</div><!--wrap-->
	</header><!--menu-->

	<div class="g-content">
	<?php if($is_home){?>
	<main class="g-main">
	<?php }?>
