<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php
$u = new User();
$this->inc('elements/header.php',array('is_home' => true));
?>
<article class="c-opening js-opening">
<?php if(!$u->isLoggedIn()):?>
	<?php
		$a = new Area('イントロタイトル');
		$a->display($c)
	?>
	<div class="c-opening_bgs js-opening_slides">
		<?php
			$a = new Area('イントロ画像スライド');
			$a->display($c)
		?>
	</div>
<?php else:?>
	<?php
		$a = new Area('イントロタイトル');
		$a->display($c)
	?>
	<?php
		$a = new Area('イントロ画像スライド');
		$a->display($c)
	?>
<?php endif;?>
</article>

<nav class="c-kv">
	<?php
		$a = new Area('トップページメインナビゲーション');
		$a->display($c)
	?>
</nav><!--kv-->

<nav class="c-navBtn">
	<?php
		$a = new Area('トップページサブナビゲーション');
		$a->display($c)
	?>
</nav><!--navBtn-->

<section class="c-gallery">
	<div class="c-gallery_area">
		<?php
			$a = new Area('トップページスライドショー');
			$a->display($c)
		?>
	</div><!--area-->
	<div class="c-gallery_link c-linkBtn">
		<div class="u-inner">
			<div class="c-gallery_linkItem c-linkBtn_item">
				<a href="/photogallery" class="e-box">
					<span class="e-box_in">
						<strong class="e-name">フォトギャラリーを見る</strong>
					</span>
				</a>
			</div>
		</div><!--inner-->
	</div><!--linkBtn-->
</section><!--gallery-->

<section class="c-newsArea">
	<div class="u-inner">
	
		<h2 class="c-newsArea_head">
			<a href="/hp" class="e-link_box"><strong class="e-name">お知らせ</strong></a>
		</h2>
	
		<div class="c-newsListA">
			<div class="c-newsArea_subhead">
				<h3 class="e-name">お知らせ</h3>
				<p class="e-link">
					<a href="/hp" class="e-link_box">一覧を見る</a>
				</p>
			</div>
			<?php
				$a = new Area('ニュース');
				$a->display($c)
			?>
			
		</div><!--newsListA-->
		<?php
			$a = new Area('法話・行事');
			$a->display($c)
		?>
		<!-- <div class="c-newsListB">
			<ul class="c-newsListB_lists">
				<li class="c-newsListB_list">
					<a href="#" class="e-box">
						<span class="e-date">2016/11/01</span>
						<span class="e-category is-category_houwa">
							<em class="e-category_icon">今月の法話</em>
						</span>
						<strong class="e-name">「食い物の恨み」 と 「下化衆生」</strong>
					</a>
				</li>
				<li class="c-newsListB_list">
					<a href="#" class="e-box">
						<span class="e-date">2016/10/17</span>
						<span class="e-category is-category_event">
							<em class="e-category_icon">行事案内</em>
						</span>
						<strong class="e-name">法話の集い11月7日(月)花園会館にて開催(参加費無料)</strong>
					</a>
				</li>
				<li class="c-newsListB_list">
					<a href="#" class="e-box">
						<span class="e-date">2016/10/05</span>
						<span class="e-category is-category_houwa">
							<em class="e-category_icon">今月の法話</em>
						</span>
						<strong class="e-name">月に手を伸ばせ!?</strong>
					</a>
				</li>
			</ul>
		</div><!--newsListB-->
	</div><!--inner-->
</section><!--newsArea-->

<section class="c-pageFront">
	<?php
		$a = new Area('おかげさま運動');
		$a->display($c)
	?>
</section><!--pageFront-->

<div class="c-bannerArea">
	<div class="u-inner">
		<div class="c-gridBnr">
			<div class="c-gridBnr_grid">
				<div class="c-gridBnr_cell">
					<ul class="c-gridBnr_items">
						<?php
							$a = new Area('トップページリンクエリア1');
							$a->display($c)
						?>
					</ul>
				</div>
			</div>
			<ul class="c-gridBnr_items">
				<li class="c-gridBnr_item is-item_solor">
					<?php
						$a = new Area('太陽光');
						$a->display($c)
					?>
					<script src="<?php echo $this->getThemePath();?>/assets/lib/solor/script.js"></script>
				</li>
				<?php
					$a = new Area('トップページリンクエリア2');
					$a->display($c)
				?>
			</ul>
			<?php
				$a = new Area('facebook');
				$a->display($c)
			?>
		</div><!--gridBnr-->

		<div class="c-subBnr">
			<ul class="c-subBnr_items">
				<?php
					$a = new Area('トップページバナー');
					$a->setCustomTemplate('manual_nav', 'banner.php');
					$a->display($c)
				?>
			</ul>
		</div><!--subBnr-->
	</div><!--inner-->
</div><!--bannerArea-->

<?php
	$a = new Area('メインコンテンツ');
	$a->display($c);
?>

<?php $this->inc('elements/footer.php',array('is_home' => true)) ?>
