<?php
namespace Concrete\Package\SuitonBaseUtil;
defined('C5_EXECUTE') or die("Access Denied.");

use Package;
use Concrete\Package\SuitonBaseUtil\Src\AdditionalUtil\AdditionalUtilServiceProvider;
use Core;
use Config;
use Concrete\Core\Page\Theme\Theme;
use Environment;
use Concrete\Core\Support\Facade\Events;
use Concrete\Core\Block\View\BlockView;

class Controller extends Package
{
	protected $pkgDescription = "Base utilies and snipetts";
	protected $pkgName = "Suiton Base Util";
	protected $pkgHandle = 'suiton_base_util';
	protected $appVersionRequired = '5.7.3.1';
	protected $pkgVersion = '1.1.0';

	 public function on_start()
	{
		/* ===============================================
		register assets
		=============================================== */
		$al = \Concrete\Core\Asset\AssetList::getInstance();
		/* css
		----------------------- */
		$css = array(
			'style-css' =>  $this->pkgThemePath .'themes/base_theme/assets/css/style.css'
		);
		foreach($css as $h => $n){
			$al->register('css',$h,$n,array(),$this->pkgHandle);
		}
		/* external css
		----------------------- */
		$css_ext = array(
			'notosansjapanese' => 'https://fonts.googleapis.com/earlyaccess/notosansjapanese.css',
			'sawarabimincho' => 'https://fonts.googleapis.com/earlyaccess/sawarabimincho.css',
			'Roboto' => 'https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i',
			'Lora' => 'https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i',
			'smoothness' => 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css',
			'slick' => 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css',
			'magnific' => 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css'
		);
		foreach($css_ext as $h => $n){
			$al->register('css',$h,$n,array('local' => false),$this->pkgHandle);
		}
		/* js
		----------------------- */
		$js = array(
			'script-js' => $this->pkgThemePath . 'themes/base_theme/assets/js/script.js'
		);
		foreach($js as $h => $n){
			$al->register(
				'javascript',$h,$n,
				array(
					'version'  => '1.0.0',
					'position' => \Concrete\Core\Asset\Asset::ASSET_POSITION_FOOTER,
					'minify' => false,
					'combine' => true
				),
				$this->pkgHandle
			);
		}
		/* external JS
		----------------------- */
		$js_ext = array(
			'cookie-js' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js',
			'modernizr-js' => 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js',
			'slick-js' => 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js',
			'magnific-js' => 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js',
		);
		foreach($js_ext as $h => $n){
			$al->register(
				'javascript',$h,$n,
				array(
					'version'  => '1.0.0',
					'position' => \Concrete\Core\Asset\Asset::ASSET_POSITION_FOOTER,
					'minify' => false,
					'combine' => true,
					'local' => false
				),
				$this->pkgHandle
			);
		}
		/* ===============================================
		Save my config
		=============================================== */
		$config_param = array(
			'seo.title_format' => '%2$s | %1$s',
			'seo.title_segment_separator' => ' | ',
			'external.news_overlay' => false,
		);
		$this->setMyConfig($config_param);
		/* ===============================================
		Reading the general-purpose functional class as a helper
			Thanks!! http://www.concrete5.org/community/forums/5-7-discussion/helpers/

		If you want to add functions ,
		add functions to 'this_package/src/AdditionalUtil/Service/AdditionalUtil.php'
		=============================================== */
		$app = Core::getFacadeApplication();
		$sp = new AdditionalUtilServiceProvider($app);
		$sp->register();
		/* ===============================================
		sitemap extend
		未承認バージョンの有無・権限オーバーライド状況の表示
		=============================================== */
		Core::bind('helper/concrete/dashboard/sitemap', 'Concrete\Package\SuitonBaseUtil\Src\Application\Service\Dashboard\ExtendSitemap');
		/* ===============================================
		override core components
		=============================================== */
		$env = Environment::get();
		$env->overrideCoreByPackage('elements/header_required.php', $this);

		$this->updateXML();
	}

	public function install(){
		// Run default install process
		$pkg = parent::install();

		//theme install
		Theme::add('base_theme', $pkg);
	}

	/**
	* Set congfig
	* Thanks! https://gist.github.com/hissy/d11551ad58e5f3d0fcd4
	*/
	private function setMyConfig($param = null){
		if($param){
			foreach($param as $key => $val)
			Config::set('concrete.'.$key, $val);
		}
	}

	private function updateXML(){
		Events::addListener('on_page_update', function($event) {
			$page = $event->getPageObject();
			$blocks = $page->getBlocks('メインコンテンツ');
			if(is_array($blocks)){
				foreach($blocks as $block){
					if($block->btHandle == 'create_podcast_xml'){//target_block_typeは取得したいブロックタイプを指定
						$bv = new BlockView($block);
						ob_start();
						$bv->render('view');
						$out = ob_get_contents();
						ob_end_clean();
					}
				}
			}

			$filename = dirname(dirname( dirname( __FILE__ ) )).'/podcast.xml';
			$fp = fopen($filename, 'w');
			fwrite($fp, trim($out));
			fclose($fp);
		});
	}
}
